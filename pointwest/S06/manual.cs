// ================================================
// C# Pillars of OOP (Inheritance and Polymorphism)
// ================================================

/*
Other References:
    C# Inheritance
        https://www.tutorialspoint.com/csharp/csharp_inheritance.htm
    C# Inheritance in Constructors
        https://www.geeksforgeeks.org/c-sharp-inheritance-in-constructors/
    C# Polymorphism
        https://www.tutorialspoint.com/csharp/csharp_polymorphism.htm

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "C# Console App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Create an "Animal" Class to demonstrate the use of inheritance.
    Application > discussion > Animal.cs
*/

    	// ...

        namespace discussion
        {
            internal class Animal
            {
                private string classification;
                private string dietType;

                public Animal() { }
                public Animal(string classification, string dietType)
                {
                    this.classification = classification;
                    this.dietType = dietType;
                }

                public string Classification { get => classification; set => classification = value; }
                public string DietType { get => dietType; set => dietType = value; }

                public void Sleep()
                {
                    Console.WriteLine("This animal is sleeping.");
                }
            }
        }

/*
3. Create a "Reptile" Class to demonstrate the use of inheritance.
    Application > discussion > Reptile.cs
*/

        // ...

        namespace discussion
        {
            internal class Reptile: Animal
            {
                private string habitat;
                private bool hasScales;

                public Reptile() { }
                public Reptile(string habitat, bool hasScales)
                {
                    this.habitat = habitat;
                    this.hasScales = hasScales;
                }

                public string Habitat { get => habitat; set => habitat = value; }
                public bool HasScales { get => hasScales; set => hasScales = value; }

                public void Eat()
                {
                    Console.WriteLine("This animal is eating.");
                }
            }
        }

/*
4. Create a "Crocodile" Class to demonstrate the use of inheritance.
    Application > discussion > Crocodile.cs
*/

        // ...

        namespace discussion
        {
            internal class Crocodile: Reptile
            {
                private string name;
                private int age;

                public Crocodile() { }
                // We can utilize the getters and setters inherited from the base/parent classes to include fields to be instantiated when an object is instantiated
                public Crocodile(string name, int age, string classification, string dietType, string habitat, bool hasScales)
                {
                    this.name = name;
                    this.age = age;
                    Classification = classification;
                    DietType = dietType;
                    Habitat = habitat;
                    HasScales = hasScales;
                }

                public string Name { get => name; set => name = value; }
                public int Age { get => age; set => age = value; }

                public void DescribePet()
                {
                    Console.WriteLine($"Name: {name}");
                    Console.WriteLine($"Age: {age}");
                    // Since the crocodile class inherits from the Animal and Reptile classes, we have access to the getter methods denoted by the Capital letters for printing out values
                    Console.WriteLine($"Classification: {Classification}");
                    Console.WriteLine($"Diet Type: {DietType}");
                    Console.WriteLine($"Habitat: {Habitat}");
                    Console.WriteLine($"Has Scales: {HasScales}");
                }

                public void Swim()
                {
                    Console.WriteLine("This crocodile is swimming.");
                }
            }
        }

/*
5. Instantiate a "Crocodile" class and invoke it's method.
    Application > discussion > Program.cs
*/

        // ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // [Section] Inheritance
                    // Inheritance allows us to inherit or pass fields and methods from one class to another.
                    // This allows reusability of code functionality adhering to the DRY principle.
                    // The "Crocodile" class has inherited all the properties and methods from the "Animal" and "Reptile" classes
                    Crocodile myPet = new Crocodile();
                    myPet.Classification = "Reptile";
                    myPet.DietType = "Carnivore";
                    myPet.Habitat = "Fresh Water";
                    myPet.HasScales = true;
                    myPet.Name = "Drogon";
                    myPet.Age = 7;

                    Console.WriteLine("Result of Inheritance:");
                    myPet.DescribePet();
                    myPet.Swim();
                    myPet.Sleep();
                    myPet.Eat();

                    Crocodile myOtherPet = new Crocodile("Godzilla", 1000, "Reptile", "Carnivore", "Underground", true);

                    myOtherPet.DescribePet();
                    myOtherPet.Swim();
                    myOtherPet.Sleep();
                    myOtherPet.Eat();

                }

            }
        }

/*
6. Create a "Driver" Class to demonstrate the use of inheritance vs composition.
    Application > discussion > Driver.cs
*/

        // ...

        namespace discussion
        {
            internal class Driver
            {
                private string name;
                private int age;

                public Driver() { }
                public Driver(string name, int age)
                {
                    this.name = name;
                    this.age = age;
                }

                public string Name { get => name; set => name = value; }
                public int Age { get => age; set => age = value; }
            }
        }

/*
7. Create a "Car" Class to demonstrate the use of inheritance vs composition.
    Application > discussion > Car.cs
*/

        // ...

        namespace discussion
        {
            internal class Car
            {
                private string make;
                private string model;
                private Driver driver;

                public Car() { }
                public Car(string make, string model, Driver driver)
                {
                    this.make = make;
                    this.model = model;
                    this.driver = driver;
                }

                public string Make { get => make; set => make = value; }
                public string Model { get => model; set => model = value; }
                internal Driver Driver { get => driver; set => driver = value; }

                public void StartEngine()
                {
                    Console.WriteLine("The engine is now running.");
                }
            }
        }

/*
8. Instantiate a "Driver" and a "Car" class and invoke it's method.
    Application > discussion > Program.cs
*/

        // ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    myOtherPet.Eat();

                    // [Section] Inheritance vs Composition
                    // Inheritance creates an is-a relationship (e.g. A crocodile is a reptile and is an animal)
                    // Composition creates a has-a relationship (e.g. A driver has a car)
                    // Inheritance creates a tight coupling of classes meaning when an implementation of the parent/base classes changes, so will the child classes inheriting from them.
                    // Composition creates a loose coupling such that when on of the classes change, it may or may not directly affect the other classes.
                    Driver myDriver = new Driver("Michael Schumacher", 52);
                    Car myCar = new Car("Ferrari", "F430", myDriver);
                    // The Name field is not available because the "Car" class did not inherit the "Driver" class but is composed of it
                    // Console.WriteLine(myCar.Name);
                    Console.WriteLine(myCar.Driver.Name);

                }

            }
        }

/*
9. Create a "Turtle" Class to demonstrate the use of polymorphism.
    Application > discussion > Turtle.cs
*/

        // ...

        namespace discussion
        {
         
            // [Section] Polymorphism
            // Polymorphism is derived from the greek words "poly" meaning many and "morph" means forms.
            // It is the ability of an object to take many forms.
            internal class Turtle: Reptile
            {
                private string name;
                private int age;

                public Turtle() { }
                public Turtle(string name, int age, string classification, string dietType, string habitat, bool hasScales)
                {
                    this.name = name;
                    this.age = age;
                    Classification = classification;
                    DietType = dietType;
                    Habitat = habitat;
                    HasScales = hasScales;
                }

                public string Name { get => name; set => name = value; }
                public int Age { get => age; set => age = value; }

                // Even if the Crocodile class has a similar method, we can achieve polymorphism by having a different implementation using the same method name.
                public void Swim()
                {
                    Console.WriteLine("This turtle is swimming using it's limbs and webbed feet");
                }
            }
        }

/*
10. Instantiate a "Turtle" class and invoke it's method.
    Application > discussion > Program.cs
*/

        // ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    Console.WriteLine(myCar.Driver.Name);

                    Console.WriteLine("Result of Polymorphism:");
                    Turtle myFavoritePet = new Turtle("Rhaegal", 7, "Reptile", "Herbivore", "Salt Water", false);
                    myFavoritePet.Swim();
                    myPet.Swim();

                }

            }
        }

/*
11. Update the "Turtle" Class to demonstrate the use of function overloading.
    Application > discussion > Turtle.cs
*/

        // ...

        namespace discussion
        {
         
            internal class Turtle: Reptile
            {
                // ...

                public void Swim()
                {
                    // ...
                }

                // [Section] Static Polymorphism and Function Overloading
                // The mechanism of linking a function with an object during compile time is called static/early binding.
                // The definition of the function must differ from each other by the types and/or the number of arguments in the argument list.
                public void Swim(string location)
                {
                    Console.WriteLine($"This turtle is swimming in the {location}");
                } 
            }
        }



/*
========
Activity
========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

        // 1a. On the menu bar, choose File > New > Project.
        // 1b. Choose "C# Console App" from the templates.
        // 1c. Add the project name as "Discussion" and click on "Next".
        // 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Create a "User" Class.
    Application > discussion > User.cs
*/

        // ...

        namespace activity
        {
            internal class User
            {
                private string name;
                private string joinDate;
                private bool isAdmin;

                public User() { }
                public User(string name, string joinDate, bool isAdmin)
                {
                    this.name = name;
                    this.joinDate = joinDate;
                    this.isAdmin = isAdmin;
                }

                public string Name { get => name; set => name = value; }
                public string JoinDate { get => joinDate; set => joinDate = value; }
                public bool IsAdmin { get => isAdmin; set => isAdmin = value; }
            }
        }

/*
3. Create an "Admin" Class that inherits from the user class.
    Application > discussion > Admin.cs
*/

        // ...

        namespace activity
        {
            internal class Admin : User
            {
                private string role;

                public Admin() { }
                public Admin(string name, string joinDate, bool isAdmin, string role)
                {
                    Name = name;
                    JoinDate = joinDate;
                    IsAdmin = isAdmin;
                    this.role = role;
                }

                public string Role { get => role; set => role = value; }

                public void AddProduct()
                {
                    Console.WriteLine($"{Name} just added another product");
                }
            }
        }

/*
4. Instantiate an "Admin" class and invoke it's method.
    Application > discussion > Program.cs
*/

        // ...

        namespace activity
        {
            class Activity
            {

                static void Main(string[] args)
                {

                    Admin adminAccount = new Admin("Jane Smith", "05-01-83", true, "Lead Developer");
                    adminAccount.AddProduct();

                }

            }
        }

/*
5. Create a "Premium" Class that inherits from the user class.
    Application > discussion > Admin.cs
*/

        // ...

        namespace activity
        {
            internal class Premium : User
            {
                public Premium() { }
                public Premium(string name, string joinDate, bool isAdmin)
                {
                    Name = name;
                    JoinDate = joinDate;
                    IsAdmin = isAdmin;
                }

                public void Login()
                {
                    Console.WriteLine("View products online with free shipping!");
                }

                public void Checkout()
                {
                    Console.WriteLine("Payment received. Thank you for your purchase!");
                }

            }
        }

/*
6. Create a "Regular" Class that inherits from the user class.
    Application > discussion > Admin.cs
*/

        // ...

        namespace activity
        {
            internal class Regular : User
            {
                public Regular() { }
                public Regular(string name, string joinDate, bool isAdmin)
                {
                    Name = name;
                    JoinDate = joinDate;
                    IsAdmin = isAdmin;
                }

                public void Login()
                {
                    Console.WriteLine("Purchase today and get a 25% discount on your shipping fee!");
                }

                public void Checkout()
                {
                    Console.WriteLine("Please login before you can process your payment.");
                }
            }
        }

/*
7. Instantiate a "Premium" and a "Regular" class and invoke their method.
    Application > discussion > Program.cs
*/

        // ...

        namespace activity
        {
            class Activity
            {

                static void Main(string[] args)
                {

                    // ...
                    adminAccount.AddProduct();

                    Premium myAccount = new Premium("John Smith", "10-24-21", false);
                    Console.WriteLine("Result from a Premium user:");
                    myAccount.Login();
                    myAccount.Checkout();

                    Regular unknownUser = new Regular("Joe Garcia", "02-24-90", false);
                    Console.WriteLine("Result from a Regular user:");
                    unknownUser.Login();
                    unknownUser.Checkout();

                }

            }
        }