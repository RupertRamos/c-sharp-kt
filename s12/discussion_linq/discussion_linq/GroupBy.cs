﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion_linq
{
    public class GroupBy
    {
        public void GroupByOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            var data = courses.GroupBy(c => c.Author);

            foreach ( var item in data)
            {
                Console.WriteLine(item.Key);
            }
        }

        public void NewListGroupByOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            var data = courses.GroupBy(c => c.Author).Select(g => new
            {
                Author = g.Key,
                CourseCount = g.Count()
            });

            foreach ( var item in data)
            {
                Console.WriteLine($"{item.Author} - {item.CourseCount}");
            }
            Console.ReadLine();
        }


        public void MoreGroupBySample()
        {
            var courses = CourseDatabase.GetCoursesData();

            var data = courses.Where(c => c.Price >= 15000).GroupBy(c => c.Author).Select(g => new
            {
                Author = g.Key,
                Prices = g.Select(price => price.Price),
                Courses = g.Select(course => course.Name)
            }).OrderByDescending(a => a.Author);

            foreach (var item in data)
            {
                Console.WriteLine(item.Author);

                foreach (var course in item.Courses)
                {
                    foreach (var price in item.Prices)
                    {
                        Console.WriteLine($"    {course} - {price}");
                        break;
                    }
                }
            }
        }

    }
}
