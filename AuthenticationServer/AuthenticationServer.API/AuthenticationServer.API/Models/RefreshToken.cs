﻿namespace AuthenticationServer.API.Models
{
    public class RefreshToken
    {
        // As a uniques identifier
        public Guid Id { get; set; }

        // These two is what we need from our new refreshed token.
        public string Token { get; set; } = null!;
        public Guid UserId { get; set; }


    }
}
