﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
 
    // [Section] Polymorphism
    // Polymorphism is derived from the greek words "poly" meaning many and "morph" means forms.
    // It is the ability of an object to take many forms.
    internal class Turtle: Reptile
    {
        private string name;
        private int age;

        public Turtle() { }
        public Turtle(string name, int age, string classification, string dietType, string habitat, bool hasScales)
        {
            this.name = name;
            this.age = age;
            Classification = classification;
            DietType = dietType;
            Habitat = habitat;
            HasScales = hasScales;
        }

        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }

        // Even if the Crocodile class has a similar method, we can achieve polymorphism by having a different implementation using the same method name.
        public void Swim()
        {
            Console.WriteLine("This turtle is swimming using it's limbs and webbed feet");
        }

        // [Section] Static Polymorphism and Function Overloading
        // The mechanism of linking a function with an object during compile time is called static/early binding.
        // The definition of the function must differ from each other by the types and/or the number of arguments in the argument list.
        public void Swim(string location)
        {
            Console.WriteLine($"This turtle is swimming in the {location}");
        }
    }
}
