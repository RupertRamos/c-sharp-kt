﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    internal class Reptile : Animal
    {
        private string habitat;
        private bool hasScales;

        public Reptile() { }
        public Reptile(string habitat, bool hasScales, string classification, string dietType)
        {
            this.habitat = habitat;
            this.hasScales = hasScales;
            Classification = classification;
            DietType = dietType;
        }

        public string Habitat { get => habitat; set => habitat = value; }
        public bool HasScales { get => hasScales; set => hasScales = value; }

        public void Eat()
        {
            Console.WriteLine("This animal is eating.");
        }

    }
}
