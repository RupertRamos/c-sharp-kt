// ==============================
// Controller and CRUD Operations
// ==============================

/*
Other References:
    An Introduction to NuGet
        https://learn.microsoft.com/en-us/nuget/what-is-nuget
    Usage of the @ symbol in ASP.NET
        https://www.mikesdotnetting.com/article/258/usage-of-the-at-sign-in-asp-net
    Razor syntax reference for ASP.NET Core
        https://learn.microsoft.com/en-us/aspnet/core/mvc/views/razor?view=aspnetcore-7.0
    Views in ASP.NET Core MVC
        https://learn.microsoft.com/en-us/aspnet/core/mvc/views/overview?view=aspnetcore-7.0

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "ASP.NET Core Web App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

        /*
        Important Note:
        	- The creation of a new project here will be important because the approach to creating the files in the previous session will be enhanced using scaffolding tools of Visual Studio to generate all the necessary controller and view files.
            - Upon creating the project, several files and folders will be created that will serve different functionalities as follows:
                - The "Dependencies" folder will contain dependency files for making the framework run properly.
                - The "Properties" folder will contain a "launchSettings.json" file which describes how a project can be launched, the command to run, whether the browser should be opened, which environment variables should be set and other useful information.
                - The "wwwroot folder" will contain static files such as CSS, JS, Bootstrap and jquery libraries. Bootstrap is already included in the default application and can be readily used after creating the project.
                - The "Controller", "Models" and Views folders are support the MVC pattern for building MVC web applications.
                - The "asspettings.json" is used to store information such as connection strings or application specific settings in JSON format.
                - The "Program.cs" is the main entry point of the application is the entry point for the application. This also contains the default route for the MVC application under the "app.MapControllerRoute" method.
                - The "HomeController.cs" returns the view "Index.cshtml" as the initial page.
        */

/*
2. Modify the "site.css" file to include CSS reset and change the font for the whole application.
    discussion > wwwroot > css > site.css
*/

        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        html {
            font-size: 14px;
            position: relative;
            min-height: 100%;
        }

        @media (min-width: 768px) {
            // ...
        }

        body {
            margin-bottom: 60px;
            font-family: Arial, Helvetica, sans-serif;
        }

        /*
        Important Note:
            - The site.css file can be used to define global CSS rules.
        */

/*
3. Modify the "Home" page.
    discussion > Views > Home > Index.cshtml
*/

    	@{
            ViewData["Title"] = "Home Page";
        }

        <div class="text-center my-5">
            <h1 class="display-4">Welcome to LaZuitt</h1>
            <br />

        </div>

        <div class="text-center">
            <button class="btn btn-primary">
                <a class="nav-link text-light" asp-controller="Courses" asp-action="Index">
                    Manage Courses
                </a>
            </button>
        </div>

    	/*
        Important Note:
            - Refer to "references" section of this file to find the documentations for Usage of the @ symbol in ASP.NET, Razor syntax reference for ASP.NET Core and Views in ASP.NET Core MVC
        */

/*
3. Modify the "_Layout.cshtml" file to change the design and layout and to redirect to the correct controller methods.
    discussion > Shared > _Layout.cshtml
*/

        // ...
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            @*This allows us to use the ViewData values provided in the individual pages*@
            <title>@ViewData["Title"]</title>
            <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.min.css" />
            // ...
        </head>
        <body>
            <header>
                @*Change the navbar color to "bg-dark"*@
                <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-dark border-bottom box-shadow mb-3">
                    <div class="container-fluid">
                        @*Change the branding text to "LaZuitt" and the text color to "text-light"*@
                        <a class="navbar-brand text-light" asp-area="" asp-controller="Home" asp-action="Index">LaZuitt</a>
                        // ...
                        <div class="navbar-collapse collapse d-sm-inline-flex justify-content-between">
                            <ul class="navbar-nav flex-grow-1">
                                <li class="nav-item">
                                    @*Change the text color to "text-light" and the asp-controller/asp-action attributes*@
                                    <a class="nav-link text-light" asp-area="" asp-controller="Courses" asp-action="Index">Courses</a>
                                </li>
                                <li class="nav-item">
                                    @*Change the text color to "text-light" and the asp-controller/asp-action attributes*@
                                    <a class="nav-link text-light" asp-area="" asp-controller="Settings" asp-action="Index">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div class="container">
                // ...
            </div>

            <footer class="border-top footer text-muted">
                @*Remove the "container" class and add the following classes "bg-dark text-light text-center" and change the branding text to "LaZuitt"*@
                <div class="bg-dark text-light text-center">
                    &copy; 2023 - LaZuitt - <a asp-area="" asp-controller="Home" asp-action="Privacy">Privacy</a>
                </div>
            </footer>
            // ...
        </body>
        // ...

/*
4. Create a "Course.cs" Model file.
    discussion > Models > Course.cs
*/

    	using System.Collections;

    	namespace discussion.Models
    	{
    	    public class Course
    	    {
    	        public int Id { get; set; }
    	        public string Title { get; set; } = null!;
    	        public Stack Stack { get; set; }
    	        public string Description { get; set; } = null!;
    	        public double Price { get; set; }
    	        public string StackUrl { get; set; } = null!;
    	        public string Logo { get; set; } = null!;

    	    }
    	}

/*
5. Create a "Stack.cs" Model file.
    discussion > Models > Stack.cs
*/

    	using System.ComponentModel.DataAnnotations;
    	using System.Xml.Linq;

    	namespace discussion.Models
    	{
    	    public enum Stack
    	    {
    	        MEAN,
    	        MERN,
    	        Django,
    	        [Display(Name = "Ruby on Rails")] Rails,
    	        LAMP
    	    }

    	}

/*
6. Create a "User.cs" Model file.
    discussion > Models > Stack.cs
*/

    	using System.ComponentModel.DataAnnotations;
    	using System.Xml.Linq;

    	namespace discussion.Models
    	{
    	    public class User
    	    {
    	        [Key] // Denotes Primary Key
    	        public int Id { get; set; }

    	        [Required]
    	        [Display(Name = "First Name")]
    	        public string FirstName { get; set; }

    	        [Required]
    	        [Display(Name = "Last Name")]
    	        public string LastName { get; set; }

    	        [Required]
    	        public string Email { get; set; }

    	        [Required]
    	        [Display(Name = "Registration Date")]
    	        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
    	        [DataType(DataType.Date)]
    	        public DateTime RegistrationDate { get; set; }

    	        [Required]
    	        [Display(Name = "Is Admin")]
    	        public bool IsAdmin { get; set; }

    	    }
    	}


    	/*
        Important Note:
            - This will be used in the future discussions regarding LINQ for manipulating our data.
            - Have the students/trainees copy the output and the items here will be discussed in the succeeding steps/sessions.
        */

/*
7. Create the Course Controller and View files.
    Visual Studio
*/

    	/*
        Important Note:
            - To create the controller and view files in visual studio, do the following:
                - Right click on "Controllers" in the "solution explorer".
                - Select "add" and then select "New Scaffolded Item".
                - In the pop up window for "Add New Scaffolded Item" select "MVC Controller with views, using Entity Framework".
                - Choose "Course" as the "Model class".
                - Click on the "+" symbol next to the Data context class and input the following "discussion.Data.CoursesContext"
                - Use "CourseController" as the "Controller Name"
            - Doing the above steps will create both the controller file with the following premade methods:
            	- Index
            	- Details
            	- Create
            	- Edit
            	- Delete
            - It will also create all the necessary view files for each method under the "Courses" folder:
            	- Create.cshtml
            	- Delete.csthml
            	- Details.cshtml
            	- Edit.cshtml
            	- Index.cshtml
			- This will also create the "CoursesContext" file that defines which entities are included in the data model.
            - Lastly, a database file will be created locally and can be found under "C:\Users\<Username>"
            - This approach is a very powerful tool for easily templating View files from models, all the vew files have been templated to include all fields from the "Course" model and all controllers have been templated as well to add the methods to perform basic CRUD operations.
        */

/*
8. Create the database using EF Core Migrations feature.
    Visual Studio > Tools > NuGet Package Manager > Package Manager Console
*/

    	/*
    	8a. Use the migration command to generate the code to setup the database.
    	*/
	    	// Creates the initial migration
	    	Add-Migration InitialCreate

	    /*
	    8b. Update database command to create the database and the "courses" table.
	    */

        	// Updates the database
        	Update-Database

    	/*
        Important Note:
            - NuGet is used as the package manager for Visual Studio and works similarly as Maven with IntelliJ for Java applications.
            - Migrations lets us create a database that matches our data model and update the database schema when our data model changes.
            - When prompted to stop debugging, select "yes" as the option and allow Visual Studio to create our database.
            - This command will generate a "Migrations" folder that generates code to create the initial database schema which is based on the model specified in the CoursesContext class. The Initial argument is the migration name and any name can be used.
            - The NuGet Package Manager Console will need to be run twice executing the commands provided above one by one.
            - This will install the following packages to be able to connect to a database:
                - Microsoft.EntityFrameworkCore.Sqlite (6.0.20)
                - Microsoft.EntityFrameworkCore.SqlServer (6.0.20)
                - Microsoft.EntityFrameworkCore.Tools (6.0.20)
                - Microsoft.VisualStudio.Web.CodeGeneration.Design (6.0.15)
            - If any of the above packages are missing, this can be manually installed by clicking on "Tools", then select "Nuget Package Manager" and lastly "Manage NuGet Packages for Solution".
                - Under the "Installed" tab, check if all dependencies were properly installed.
                - Click on "Tools", then select "Nuget Package Manager" and lastly "Package Manager Console" which will open the terminal that can be used to trigger the following commands for any of the missing dependencies:
                    - Install-Package <PackageName> -Version <Package Version>
                    - Install-Package Microsoft.EntityFrameworkCore.Sqlite -Version 6.0.20
            - Refer to "references" section of this file to find the documentation for An Introduction to NuGet.
        */

/*
9. Run the application to see the result of the code.
    Visual Studio
*/

        /*
        Important Note:
            - To run the application, a "green play button" can be found in visual studio which will execute the code added in the previous step.
            - Alternatively, pressing the "F5" button on the keyboard can be used to run the application.
            - A debug console will appear to print out the "Hello World" message.
            - If a project is opened via the "File > Open > Folder" option, running the application will automatically close the debug console.
            - To open a project, use the "File > Open > Project/Solution" option instead and select the corresponding solution file for the project indicated by the ".sln" extension.
            - A window pop up may appear asking to "Trust the ASP.NET Core SSL Certificate", since this is a locally created file, this should have no risk and we can select "yes" on all windows and trust the certificate.
        */

/*
10. Modify the "Create.cshtml" file to incorporate the created enum list from the "Stack" Model and change the design of the page.
    discussion > Views > Courses > Create.cshtml
*/
        @model discussion.Models.Course

        @{
            ViewData["Title"] = "Add Course";
        }

        <h1 class="text-center">Add Course</h1>

        <div class="row">
            <div class="col-md-4 offset-md-4 fw-bold">
                <form asp-action="Create">
                    <div asp-validation-summary="ModelOnly" class="text-danger"></div>
                    <div class="form-group mb-3">
                        <label asp-for="Title" class="control-label"></label>
                        <input asp-for="Title" class="form-control" />
                        <span asp-validation-for="Title" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Stack" class="control-label"></label>
                        @*<select asp-for="Stack" class="form-control"></select>*@
                        <select asp-for="Stack" asp-items="Html.GetEnumSelectList<Stack>()" class="form-control"></select>
                        <span asp-validation-for="Stack" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Description" class="control-label"></label>
                        <input asp-for="Description" class="form-control" />
                        <span asp-validation-for="Description" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Price" class="control-label"></label>
                        <input asp-for="Price" class="form-control" />
                        <span asp-validation-for="Price" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="StackUrl" class="control-label"></label>
                        <input asp-for="StackUrl" class="form-control" />
                        <span asp-validation-for="StackUrl" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Logo" class="control-label"></label>
                        <input asp-for="Logo" class="form-control" />
                        <span asp-validation-for="Logo" class="text-danger"></span>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" type="submit">
                            Add Course
                        </button>
                        <button class="btn btn-danger" type="button">
                            <a class="text-light text-decoration-none" asp-action="Index">Back to Courses</a>
                        </button>
                    </div>
                </form>
            </div>
        </div>

        @section Scripts {
            @{await Html.RenderPartialAsync("_ValidationScriptsPartial");}
        }

        /*
        Important Note:
            - The "@*" and "*@" are used to comment out lines of code in view files. This is helpful when testing code in views.
            - The previously created select tag may be removed altogether to create a cleaner code.
            - After this step, we can now access the "Create" courses page by clicking the "Create New" link in the browser to create a record in our database. 
        */

/*
11. Modify the "Edit.cshtml" file to incorporate the created enum list from the "Stack" Model and change the design of the page.
    discussion > Views > Courses > Create.cshtml
*/
        @model discussion.Models.Course

        @{
            ViewData["Title"] = "Edit Course";
        }

        <h1 class="text-center">Edit Course</h1>

        <div class="row">
            <div class="col-md-4 offset-md-4 fw-bold">
                <form asp-action="Edit">
                    <div asp-validation-summary="ModelOnly" class="text-danger"></div>
                    <input type="hidden" asp-for="Id" />
                    <div class="form-group mb-3">
                        <label asp-for="Title" class="control-label"></label>
                        <input asp-for="Title" class="form-control"/>
                        <span asp-validation-for="Title" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Stack" class="control-label"></label>
                        @*<select asp-for="Stack" class="form-control"></select>*@
                        <select asp-for="Stack" asp-items="Html.GetEnumSelectList<Stack>()" class="form-control"></select>
                        <span asp-validation-for="Stack" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Description" class="control-label"></label>
                        <input asp-for="Description" class="form-control" />
                        <span asp-validation-for="Description" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Price" class="control-label"></label>
                        <input asp-for="Price" class="form-control" />
                        <span asp-validation-for="Price" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="StackUrl" class="control-label"></label>
                        <input asp-for="StackUrl" class="form-control" />
                        <span asp-validation-for="StackUrl" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Logo" class="control-label"></label>
                        <input asp-for="Logo" class="form-control" />
                        <span asp-validation-for="Logo" class="text-danger"></span>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" type="submit">
                            Save
                        </button>
                        <button class="btn btn-danger" type="button">
                            <a class="text-light text-decoration-none" asp-action="Index">Back to Courses</a>
                        </button>
                    </div>
                </form>
            </div>
        </div>

        // ...


        /*
        Important Note:
            - After this step, we can now access the "Edit" courses page by clicking the "Edit" link in the browser to update a record in our database.
        */

/*
11. Modify the "Course.cs" model file to include validations for form inputs.
    discussion > Models > Course.cs
*/

        using System.Collections;
        using System.ComponentModel.DataAnnotations;

        namespace discussion.Models
        {
            public class Course
            {
                // Sets the Id as the primary key
                [Key]
                public int Id { get; set; }

                // Used for required fields
                [Required]
                // Used to set the minimum and maximum characters of the form input
                [StringLength(50, MinimumLength = 10)]
                public string Title { get; set; } = null!;

                [Required]
                public Stack Stack { get; set; }

                [Required]
                public string Description { get; set; } = null!;

                [Required]
                // Used to validate that the price input should be in currency value
                [DataType(DataType.Currency)]
                public double Price { get; set; }

                [Required]
                // Used to validate that the stackURL is a valid link
                [DataType(DataType.Url)]
                public string StackUrl { get; set; } = null!;

                [Required]
                // Used to validate that the "Logo" field is a valid image url
                [DataType(DataType.ImageUrl)]
                [Display(Name = "Logo")]
                public string Logo { get; set; } = null!;

            }
        }

/*
12. Modify the "Index.cshtml" view file to add some design, re-arrange the order of table columns and add an image to the "Logo" column.
    discussion > Views > Courses > Index.cshtml
*/

        @model IEnumerable<discussion.Models.Course>

        @{
            ViewData["Title"] = "Courses";
        }

        <div class="d-flex justify-content-between my-5">
            <h1>Courses</h1>
            <button class="btn btn-success py-0">
                <a class="text-light text-decoration-none" asp-area="" asp-controller="Courses" asp-action="Create">Add New Course</a>
            </button>
        </div>

        <table class="table">
            <thead>
                <tr class="text-center">
                    <th>
                        @Html.DisplayNameFor(model => model.Logo)
                    </th>
                    <th>
                        @Html.DisplayNameFor(model => model.Title)
                    </th>
                    <th>
                        @Html.DisplayNameFor(model => model.Stack)
                    </th>
                    <th>
                        @Html.DisplayNameFor(model => model.Description)
                    </th>
                    <th>
                        @Html.DisplayNameFor(model => model.Price)
                    </th>
                    <th>
                        @Html.DisplayNameFor(model => model.StackUrl)
                    </th>
                    <th>Actions:</th>
                </tr>
            </thead>
            <tbody>
        @foreach (var item in Model) {
                <tr>
                    <td class="align-middle">
                        @if (item.Logo != null)
                        {
                            <img src="@Url.Content(item.Logo)" alt="Logo" style="height: 75px; width: 75px"/>
                        }
                    </td>
                    <td class="align-middle">
                        @Html.DisplayFor(modelItem => item.Title)
                    </td>
                        <td class="align-middle">
                        @Html.DisplayFor(modelItem => item.Stack)
                    </td>
                    <td class="align-middle">
                        @Html.DisplayFor(modelItem => item.Description)
                    </td>
                    <td class="align-middle">
                        @Html.DisplayFor(modelItem => item.Price)
                    </td>
                    <td class="align-middle">
                        @Html.DisplayFor(modelItem => item.StackUrl)
                    </td>
                    <td class="align-middle">
                        <button class="btn btn-warning fw-bold d-block w-100 mb-2">
                            <a class="text-dark text-decoration-none" asp-action="Edit" asp-route-id="@item.Id">Edit</a>
                        </button>
                            <button class="btn btn-primary fw-bold d-block w-100 mb-2">
                            <a class="text-dark text-decoration-none" asp-action="Details" asp-route-id="@item.Id">Details</a>
                        </button>
                            <button class="btn btn-danger fw-bold d-block w-100 mb-2">
                                <a class="text-dark text-decoration-none" asp-action="Delete" asp-route-id="@item.Id">Delete</a>
                        </button>
                    </td>
                </tr>
        }
            </tbody>
        </table>

        /*
        Important Note:
            - The "@" symbol can be used in a number of ways, in this case, it is used to add a conditional statement for the image tag if the record has a logo.
        */

/*
13. Create an "images" folder inside the "wwwroot" folder to contain images to be used in the application.
    discussion > wwwroot > images
*/

        /*
        Important Note:
            - Any image file may be used for this. You can have the trainees/students download a logo and save it inside of this folder for use.
        */

/*
14. Edit a record to display the newly added image in the folder.
    browser
*/

        /*
        Important Note:
            - Access the edit page via the web application and change the logo url to "/images/file-name" (e.g. images/angular.svg). "file-name" being the name of the file that was used to save the downloaded image in the "images" folder. Make sure that the file extension is a valid file extension and is included in the name.
        */


/*
========
Activity
========
*/

/*
1. Create a "CreateCourse.cshtml" view file.
    discussion > Views > Courses > CreateCourse.cshtml
*/

        @{
            ViewData["Title"] = "Create Course";
        }

        <h1>Create New Course</h1>
        <br />

        <table border="0" cellpadding="5" cellspacing="0" width="600">
            <tr>
                <td>
                    <label for="CourseTitle">Course Title:</label>
                </td>
                <td>
                    <input name="CourseTitle" type="text" maxlength="60" style="width:100%;max-width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Stack">Stack:</label>
                </td>
                <td>
                    <select name="Stack" type="text" maxlength="60" style="width:100%;max-width: 300px;">
                        <option value="empty"></option>
                        <option value="mern">MERN Stack</option>
                        <option value="mean">MEAN Stack</option>
                        <option value="django">Django Stack</option>
                        <option value="rails">Rails</option>
                        <option value="lamp">LAMP Stack</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Description">Description:</label>
                </td>
                <td>
                    <input name="Description" type="text" maxlength="43" style="width: 100%; max-width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Thumbnail">Thumbnail:</label>
                </td>
                <td>
                    <input name="Thumbnail" type="text" maxlength="43" style="width: 100%; max-width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Price">Price:</label>
                </td>
                <td>
                    <input name="Price" type="text" maxlength="43" style="width: 100%; max-width: 300px;" />
                </td>

        </table>
        <br />
        <button name="Create" type="submit" maxlength="43" style="width: 100%; max-width: 300px; margin-left:210px; background-color:gray">
            Create
        </button>

/*
2. Add a "CreateCourse" controller method.
    discussion > Controllers > CoursesController.cs
*/

        // ...

        namespace discussion.Controllers
        {
            public class CoursesController : Controller
            {
                public IActionResult Index()
                {
                    // ...
                }

                public IActionResult CreateCourse()
                {
                    return View();
                }

            }
        }

/*
3. Modify the "Courses" page to be able to properly navigate to the "CreateCourse" page.
    discussion > Views > Courses > Index.cshtml
*/

        // ...
        <body>

            // ...

            <div>
                <h2 style="display:inline-block">Courses available</h2>
                <button style="float:right; background-color:gray">
                    <a class="nav-link text-dark" asp-area="" asp-controller="Home" asp-action="Index">
                        Create New
                    </a>
                </button>


            </div>

            // ...

        </body>
        // ...

/*
10. Modify the "Create.cshtml" file to incorporate the created enum list from the "Stack" Model and change the design of the page.
    discussion > Views > Courses > Create.cshtml
*/
        @model discussion.Models.Course

        @{
            ViewData["Title"] = "Add Course";
        }

        <h1 class="text-center">Add Course</h1>

        <div class="row">
            <div class="col-md-4 offset-md-4 fw-bold">
                <form asp-action="Create">
                    <div asp-validation-summary="ModelOnly" class="text-danger"></div>
                    <div class="form-group mb-3">
                        <label asp-for="Title" class="control-label"></label>
                        <input asp-for="Title" class="form-control" />
                        <span asp-validation-for="Title" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Stack" class="control-label"></label>
                        @*<select asp-for="Stack" class="form-control"></select>*@
                        <select asp-for="Stack" asp-items="Html.GetEnumSelectList<Stack>()" class="form-control"></select>
                        <span asp-validation-for="Stack" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Description" class="control-label"></label>
                        <input asp-for="Description" class="form-control" />
                        <span asp-validation-for="Description" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Price" class="control-label"></label>
                        <input asp-for="Price" class="form-control" />
                        <span asp-validation-for="Price" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="StackUrl" class="control-label"></label>
                        <input asp-for="StackUrl" class="form-control" />
                        <span asp-validation-for="StackUrl" class="text-danger"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label asp-for="Logo" class="control-label"></label>
                        <input asp-for="Logo" class="form-control" />
                        <span asp-validation-for="Logo" class="text-danger"></span>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" type="submit">
                            Add Course
                        </button>
                        <button class="btn btn-danger" type="button">
                            <a class="text-light text-decoration-none" asp-action="Index">Back to Courses</a>
                        </button>
                    </div>
                </form>
            </div>
        </div>

        @section Scripts {
            @{await Html.RenderPartialAsync("_ValidationScriptsPartial");}
        }

        /*
        Important Note:
            - The "@*" and "*@" are used to comment out lines of code in view files. This is helpful when testing code in views.
            - The previously created select tag may be removed altogether to create a cleaner code.
            - After this step, we can now access the "Create" courses page by clicking the "Create New" link in the browser to create a record in our database. 
        */