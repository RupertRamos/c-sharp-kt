﻿using System.Collections;

namespace activity
{
    class Activity
    {
        static void Main(string[] args)
        {
            // Array to store items acting as the database
            ArrayList items = new ArrayList(new string[] { "Computer", "Mouse", "Keyboard" });

            Console.WriteLine("Welcome to Zuitt Asset Management App!");
            Console.WriteLine("Choose an option: [1] Retrieve All Items, [2] Create Item, [3] Delete Item, [4] Edit Item");

            int option = Convert.ToInt32(Console.ReadLine());

            while (option < 1 || option > 5)
            {
                Console.WriteLine("Please choose a valid option");
                option = Convert.ToInt32(Console.ReadLine());
            }

            switch (option)
            {
                case 1:

                    foreach (String item in items)
                    {
                        Console.WriteLine(item);
                    }
                    break;

                case 2:
                    Console.WriteLine("Enter item name:");
                    string addItemName = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(addItemName))
                    {
                        Console.WriteLine("Enter a valid product name:");
                        addItemName = Console.ReadLine();
                    }

                    items.Add(addItemName);
                    Console.WriteLine($"{addItemName} has been successfully added");
                    Console.WriteLine(String.Join(", ", items.ToArray()));
                    break;

                case 3:
                    Console.WriteLine("Enter item name:");
                    string deleteItemName = Console.ReadLine();

                    // Used for checking if input is blank
                    while (string.IsNullOrWhiteSpace(deleteItemName))
                    {
                        Console.WriteLine("Enter a valid product name:");
                        deleteItemName = Console.ReadLine();
                    }

                    for (int x = 0; x < items.Count; x++)
                    {
                        if (string.Equals((string?)items[x], deleteItemName, StringComparison.OrdinalIgnoreCase))
                        {
                            items.Remove(items[x]);
                            Console.WriteLine($"{deleteItemName} has been successfully removed");
                            Console.WriteLine(String.Join(", ", items.ToArray()));
                            break;
                        }

                        if (x == items.Count - 1)
                        {
                            Console.WriteLine("Item not found");
                        }
                    }
                    break;

                case 4:
                    Console.WriteLine("Enter item name:");
                    string editItemName = Console.ReadLine();

                    // Used for checking if input is blank
                    while (string.IsNullOrWhiteSpace(editItemName))
                    {
                        Console.WriteLine("Enter a valid product name:");
                        editItemName = Console.ReadLine();
                    }

                    for (int x = 0; x < items.Count; x++)
                    {
                        if (string.Equals((string?)items[x], editItemName, StringComparison.OrdinalIgnoreCase))
                        {
                            Console.WriteLine("Enter new product name:");
                            string newItemName = Console.ReadLine();

                            items[x] = newItemName;
                            Console.WriteLine($"{editItemName} has been successfully updated to {newItemName}");
                            Console.WriteLine(String.Join(", ", items.ToArray()));
                            break;
                        }

                        if (x == items.Count - 1)
                        {
                            Console.WriteLine("Item not found");
                        }
                    }
                    break;

                default:
                    Console.WriteLine("Invalid Option.");
                    break;
            }
        }
    }
}