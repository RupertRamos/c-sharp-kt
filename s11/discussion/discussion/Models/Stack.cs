﻿using System.ComponentModel.DataAnnotations;

namespace discussion.Models
{
    public enum Stack
    {
        MEAN,
        MERN,
        Django,
        //Change the actual text in displaying information.
        [Display(Name = "Ruby on Rails")] Rails,
        LAMPP
    }
}
