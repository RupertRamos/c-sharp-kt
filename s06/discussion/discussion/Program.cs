﻿namespace discussion
{
    class Discussion
    {
        static void Main(string[] args)
        {

            Crocodile myPet = new Crocodile();
            myPet.Classification = "Reptile";
            myPet.DietType = "Carnivore";
            myPet.Habitat = "Fresh Water";
            myPet.HasScales = true;
            myPet.Name = "Dragon";
            myPet.Age = 7;

            Console.WriteLine("Result of Inheritance:");
            myPet.DescribePet();
            myPet.Swim();
            myPet.Sleep();
            myPet.Eat();

            Crocodile myOtherPet = new Crocodile("Godzilla", 1000, "Reptile", "Carnivore", "Underground", true);
            myOtherPet.DescribePet();
            myOtherPet.Swim();
            myOtherPet.Sleep();
            myOtherPet.Eat();


            Driver myDriver = new Driver("Michael Schumacher", 52);
            Car myCar = new Car("Ferrari", "F430", myDriver);
            Console.WriteLine(myCar.Driver.Name);

            Turtle myFavoritePet = new Turtle("Rhegal", 7, "Reptile", "Herbivore", "Salt Water", false);
            myFavoritePet.Swim("Pacific Ocean");
            myPet.Swim();

            myFavoritePet.Eat();
            myPet.Eat();
        }
    }
}