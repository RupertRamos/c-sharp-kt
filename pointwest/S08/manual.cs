// ===================================
// C# Nested Types and Partial Classes
// ===================================

/*
Other References:
    C# Nested Types (Microsoft)
        https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/nested-types
    C# Nested Types (GeeksForGeeks)
        https://www.geeksforgeeks.org/nested-classes-in-c-sharp/#:~:text=In%20C%23%2C%20a%20user%20is,more%20readable%20and%20maintainable%20code.
    C# Partial Classes
        https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/partial-classes-and-methods


Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "C# Console App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Create a "Car" class to demonstrate the use of nested Types.
    Application > discussion > Car.cs
*/

    	// ...

        namespace discussion
        {
            // [Section] Nested Types
            // A type defined within a class, struct, or interface is called a nested type.
            // This feature enables the user to logically group classes that are only used in one place, thus this increases the use of encapsulation, and create more readable and maintainable code.
            // Nested types of a class can be public, protected, internal, protected internal, private or private protected.
            internal class Car
            {

                private string make;
                private string model;

                public Car() { }
                public Car(string make, string model)
                {
                    this.make = make;
                    this.model = model;
                }

                public string Make { get => make; set => make = value; }
                public string Model { get => model; set => model = value; }

                // By default, a nested type's access specifier is set to private when it is not declared
                public class Engine
                {
                    private string model;
                    private int horsePower;
                    private int cylinders;

                    public Engine() { }
                    public Engine(string model, int horsePower, int cylinders)
                    {
                        this.model = model;
                        this.horsePower = horsePower;
                        this.cylinders = cylinders;
                    }

                    public string Model { get => model; set => model = value; }
                    public int HorsePower { get => horsePower; set => horsePower = value; }
                    public int Cylinders { get => cylinders; set => cylinders = value; }

                    public void StartEngine()
                    {
                        Console.WriteLine("Engine is now running");
                    }

                    public void StopEngine()
                    {
                        Console.WriteLine("Engine has been stopped");
                    }

                }

                public class Tires {

                    private int pressure;
                    private int size;
                    private string brand;

                    public Tires() { }
                    public Tires(int pressure, int size, string brand)
                    {
                        this.pressure = pressure;
                        this.size = size;
                        this.brand = brand;
                    }

                    public int Pressure { get => pressure; set => pressure = value; }
                    public int Size { get => size; set => size = value; }
                    public string Material { get => brand; set => brand = value; }

                    public void CheckPressure()
                    {
                        Console.WriteLine($"Tire pressure is at {pressure}.");
                    }
                }

            }
        }


        /*
        Important Note:
            - Refer to "references" section of this file to find the documentations for C# Nested Types (Microsoft) and C# Nested Types (GeeksForGeeks).
        */

/*
3. Instantiate the "Car" class and invoke it's methods to demonstrate the use of inheriting nested types.
    Application > discussion > Program.cs
*/

        // ...

        namespace Discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // Nested Types
                    Car myCar = new Car("Ferrari", "Ferrari SF90 Stradale");
                    Car.Engine myEngine = new Car.Engine("V-12", 769, 8);
                    Car.Tires myTires = new Car.Tires(32, 9, "Pirelli");

                    Console.WriteLine("Result of Nested Types");
                    myEngine.StartEngine();
                    myEngine.StopEngine();
                    myTires.CheckPressure();

                }

            }
        }

/*
4. Create a "Honda" class that inherits from the "Car" class to demonstrate the use of inheriting nested Types.
    Application > discussion > Honda.cs
*/

        // ...

        namespace discussion
        {
            internal class Honda : Car
            {
                
                private int price;

                public Honda() { }

                public Honda(string make, string model, int price)
                {
                    Make = make;
                    Model = model;
                    this.price = price;
                }

                public int Price { get => price; set => price = value; }
            }
        }

/*
5. Instantiate the "Honda" class and invoke it's methods to demonstrate the use of inheriting nested types.
    Application > discussion > Program.cs
*/

        // ...

        namespace Discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    myTires.CheckPressure();

                    // Inheriting Nested Types
                    Honda myOtherCar = new Honda("Honda", "Honda Civic 2021", 1300000);
                    Honda.Engine myOtherEngine = new Honda.Engine("V-Tec", 158, 4);
                    Honda.Tires myOtherTires = new Honda.Tires(30, 10, "Honda");

                    Console.WriteLine("Result of Inheriting Nested Types");
                    myOtherEngine.StartEngine();
                    myOtherEngine.StopEngine();
                    myOtherTires.CheckPressure();

                }

            }
        }

/*
6. Create a "Laptop.GraphicsCard" class to demonstrate the use of partial types.
    Application > discussion > Laptop.GraphicsCard.cs
*/

        // ...

        namespace discussion
        {
            // [Section] Partial Classes
            // Each source file contains a section of the type or method definition, and all parts are combined when the application is compiled.
            // There are several situations when splitting a class definition is desirable:
            // - When working on large projects, spreading a class over separate files enables multiple programmers to work on it at the same time.
            // - When working with automatically generated source, code can be added to the class without having to recreate the source file.
            // - When using source generators to generate additional functionality in a class.
            internal partial class Laptop
            {
                private string graphicsCard;

                public Laptop() { }
                public Laptop(string graphicsCard)
                {
                    this.graphicsCard = graphicsCard;
                }

                public string GraphicsCard { get => graphicsCard; set => graphicsCard = value; }
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for C# Partial Classes.
        */

/*
7. Create a "Laptop.Memory" class to demonstrate the use of partial types.
    Application > discussion > Laptop.Memory.cs
*/

        // ...

        namespace discussion
        {
            internal partial class Laptop
            {
                private string memory;

                public string Memory { get => memory; set => memory = value; }
            }
        }

/*
8. Update the "Laptop.GraphicsCard" class to include the memory in the constructor.
    Application > discussion > Laptop.Graphics.cs
*/

        // ...

        namespace discussion
        {
            internal partial class Laptop
            {
                // ...
                public Laptop(string graphicsCard, string memory)
                {
                    this.graphicsCard = graphicsCard;
                    this.memory = memory;
                }

                // ...
            }
        }

        /*
        Important Note:
            - Alternatively, this can be done by highlighting the fields of the partial class and selecting the option "Add parameters to" and selecting the appropriate constructor to include the field to.
        */

/*
9. Instantiate the "Laptop" class and invoke it's methods to demonstrate the use of partial classes.
    Application > discussion > Program.cs
*/

        // ...

        namespace Discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    myOtherTires.CheckPressure();

                    // Partial Classes
                    Laptop myLaptop = new Laptop("Up to GeForce RTX™ 3080 Ti Laptop GPU", "16GB or 32GB RAM (DDR5 4800MHz)");
                    Console.WriteLine("Result of Partial Classes");
                    Console.Write(myLaptop.GraphicsCard);
                    Console.Write(myLaptop.Memory);

                }

            }
        }
        

/*
========
Activity
========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

        // 1a. On the menu bar, choose File > New > Project.
        // 1b. Choose "C# Console App" from the templates.
        // 1c. Add the project name as "Discussion" and click on "Next".
        // 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Create a "House" class with nested types.
    Application > discussion > House.cs
*/

        // ...

        namespace activity
        {
            internal class House
            {

                private string type;
                private string address;

                public House() { }
                public House(string type, string address)
                {
                    this.type = type;
                    this.address = address;
                }

                public string Type { get => type; set => type = value; }
                public string Address { get => address; set => address = value; }

                public class Bedroom {
                    private string bed;
                    private ArrayList furnitures = new ArrayList();

                    public Bedroom() { }
                    public Bedroom(string bed, ArrayList furnitures)
                    {
                        this.bed = bed;
                        this.furnitures = furnitures;
                    }

                    public string Bed { get => bed; set => bed = value; }
                    public ArrayList Furnitures { get => furnitures; set => furnitures = value; }

                    public void CheckFurnitures()
                    {
                        foreach (string furniture in furnitures)
                        {
                            Console.WriteLine("The bedroom has the following furnitures:");
                            Console.WriteLine(furniture);
                        }
                    }

                    public void AddFurniture(string furniture)
                    {
                        furnitures.Add(furniture);
                        Console.WriteLine($"{furniture} has been successfully added");
                    }
                }

                public class Bathroom
                {
                    private bool hasShower;
                    private bool hasBathTub;

                    public Bathroom() { }
                    public Bathroom(bool hasShower, bool hasBathTub)
                    {
                        this.hasShower = hasShower;
                        this.hasBathTub = hasBathTub;
                    }

                    public bool HasShower { get => hasShower; set => hasShower = value; }
                    public bool HasBathTub { get => hasBathTub; set => hasBathTub = value; }

                    public void OpenShower()
                    {
                        Console.WriteLine("The shower is currently running.");
                    }
                }
            }
        }


/*
3. Instantiate the "House" class and invoke it's methods.
    Application > discussion > Program.cs
*/

        // ...

        namespace Activity
        {
            class Activity
            {

                static void Main(string[] args)
                {

                    // Nested Types
                    House myHouse = new House("Bungalow", "1746 Jose Rizal St. Manila");
                    House.Bedroom myBedroom = new House.Bedroom("Queen Sized Bed", new ArrayList());

                    Console.WriteLine("Result from Nested Types:");
                    Console.WriteLine(myBedroom.Bed);
                    myBedroom.AddFurniture("Study Table");
                    myBedroom.AddFurniture("Night Stand");

                    myBedroom.CheckFurnitures();

                    House.Bathroom myBathroom = new House.Bathroom(true, true);
                    Console.WriteLine(myBathroom.HasShower);
                    Console.WriteLine(myBathroom.HasBathTub);

                    myBathroom.OpenShower();

                }

            }
        }

/*
4. Create a "TwoStoryHouse" class that inherits from the "House" class.
    Application > discussion > TwoStoryHouse.cs
*/

        // ...

        namespace activity
        {
            internal class TwoStoryHouse : House
            {
                private bool hasGarden;

                public bool HasGarden { get => hasGarden; set => hasGarden = value; }

                public TwoStoryHouse() { }
                public TwoStoryHouse(string type, string address, bool hasGarden)
                {
                    Type = type;
                    Address = address;
                    this.hasGarden = hasGarden;
                }

            }
        }


/*
5. Instantiate the "TwoStoryHouse" class and invoke it's methods.
    Application > discussion > Program.cs
*/

        // ...

        namespace Activity
        {
            class Activity
            {

                static void Main(string[] args)
                {

                    // ...

                    // Inheriting Nested Types
                    TwoStoryHouse myOtherHouse = new TwoStoryHouse("Multi Floored", "573 John Street, California", true);
                    TwoStoryHouse.Bedroom myOtherBedroom = new TwoStoryHouse.Bedroom();
                    TwoStoryHouse.Bathroom myOtherBathroom = new TwoStoryHouse.Bathroom(true, true);

                    Console.WriteLine("Result from Nested Types:");
                    Console.WriteLine(myOtherBedroom.Bed);
                    myOtherBedroom.AddFurniture("Sofa");
                    myOtherBedroom.AddFurniture("TV");

                    myOtherBedroom.CheckFurnitures();
                    Console.WriteLine(myOtherBathroom.HasShower);
                    Console.WriteLine(myOtherBathroom.HasBathTub);

                }

            }
        }

/*
6. Create a "Factory.AssemblyLine" class.
    Application > discussion > Factory.AssemblyLine.cs
*/

        // ...

        namespace activity
        {
            internal partial class Factory
            {

                private string product;
                private int quantityPerPackaging;

                public Factory() { }
                public Factory(string product, int quantityPerPackaging)
                {
                    this.product = product;
                    this.QuantityPerPackaging = quantityPerPackaging;
                }

                public string Product { get => product; set => product = value; }
                public int QuantityPerPackaging { get => quantityPerPackaging; set => quantityPerPackaging = value; }
            }
        }


/*
7. Create a "Factory.Inspection" class.
    Application > discussion > Factory.Inspection.cs
*/

        // ...

        namespace activity
        {
            internal partial class Factory
            {
                private bool isInspected;

                public bool IsInspected { get => isInspected; set => isInspected = value; }
            }
        }

/*
8. Update the "Factory.AssemblyLine" class to include the memory in the constructor.
    Application > discussion > Laptop.Graphics.cs
*/

        // ...

        namespace activity
        {
            internal partial class Factory
            {

                private string product;
                private int quantityPerPackaging;

                public Factory() { }
                public Factory(string product, int quantityPerPackaging, bool isInspected)
                {
                    this.product = product;
                    this.QuantityPerPackaging = quantityPerPackaging;
                    this.isInspected = isInspected;
                }

                public string Product { get => product; set => product = value; }
                public int QuantityPerPackaging { get => quantityPerPackaging; set => quantityPerPackaging = value; }
            }
        }

/*
9. Instantiate the "Factory" class and invoke it's methods to demonstrate the use of partial classes.
    Application > discussion > Program.cs
*/

        // ...

        namespace Activity
        {
            class Activity
            {

                static void Main(string[] args)
                {

                    // ...
                    Console.WriteLine(myOtherBathroom.HasBathTub);

                    // Partial Classes
                    Factory myFactory = new Factory("Fries", 1, true);
                    Console.WriteLine("Result from Partial Classes:");
                    Console.WriteLine(myFactory.Product);
                    Console.WriteLine(myFactory.QuantityPerPackaging);
                    Console.WriteLine(myFactory.IsInspected);

                }

            }
        }