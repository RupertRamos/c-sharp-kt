<table>
    <tr>
        <th>Session Name</th>
        <th>Link</th>
    </tr>
    <tr>
        <td>S27: Introduction to Docker and Docker Desktop</td>
        <td><a href="readme-contents/s27/README.md">Link</a></td>
    </tr>
    <tr>
        <td>S28: App Containerization Basics</td>
        <td><a href="readme-contents/s28/README.md">Link</a></td>
    </tr>
    <tr>
        <td>S29: DB Persistence and Bind Mounts</td>
        <td><a href="readme-contents/s29/README.md">Link</a></td>
    </tr>
    <tr>
        <td>S30: Multi-Container Apps and Docker Compose</td>
        <td><a href="readme-contents/s30/README.md">Link</a></td>
    </tr>
    <tr>
        <td>S31: ASP.NET API and MySQL Containerization</td>
        <td><a href="readme-contents/s31/README.md">Link</a></td>
    </tr>
</table>
