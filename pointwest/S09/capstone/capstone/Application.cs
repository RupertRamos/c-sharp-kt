﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capstone
{
    internal class Application
    {
        private ItemsDB itemsDB = new ItemsDB();
        private UsersDB usersDB = new UsersDB();
        private User authenticatedUser;

        internal ItemsDB ItemsDB { get => itemsDB; set => itemsDB = value; }
        internal UsersDB UsersDB { get => usersDB; set => usersDB = value; }
        internal User AuthenticatedUser { get => authenticatedUser; set => authenticatedUser = value; }

        public Application() { }

        public void OpenLoginMenu()
        {
            Console.WriteLine("Welcome to Zuitt Asset Management App!");
            Console.WriteLine("Choose an option: [1] Login, [2] Register, [3] Exit");

            int option = Convert.ToInt32(Console.ReadLine());

            while (option < 1 || option > 3)
            {
                Console.WriteLine("Please choose a valid option");
                option = Convert.ToInt32(Console.ReadLine());
            }

            switch (option)
            {
                case 1:
                    Login();
                    break;

                case 2:
                    UsersDB.AddUser();
                    break;

                case 3:
                    Console.WriteLine("Thank you for using the Zuitt Asset Management App!");
                    break;

                default:
                    Console.WriteLine("Invalid Option.");
                    break;
            }
        }

        public void Login()
        {
            bool isUserAuthenticated = false;

            while (isUserAuthenticated == false)
            {
                Console.WriteLine("Enter username:");
                string username = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(username))
                {
                    Console.WriteLine("Enter a valid username:");
                    username = Console.ReadLine();
                }

                Console.WriteLine("Enter password:");
                string password = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(password) || password.Length < 8)
                {
                    Console.WriteLine("Enter a valid password:");
                    password = Console.ReadLine();
                }

                foreach (User user in UsersDB.Users)
                {
                    if (user.Username == username && user.Password == password)
                    {

                        this.AuthenticatedUser = user;
                        isUserAuthenticated = true;
                        Console.WriteLine($"Welcome {user.Username}!");
                        OpenHomeMenu(authenticatedUser);
                        break;
                    }
                }

                if (isUserAuthenticated == false)
                {
                    Console.WriteLine("Invalid user credentials, please check your username and password.");
                }
            }

        }

        public void OpenHomeMenu(User authenticatedUser)
        {
            if (authenticatedUser.IsAdmin == true)
            {
                ItemsDB.OpenAdminItemsMenu();
            } else
            {
                ItemsDB.OpenUserItemsMenu();
            }
        }
    }
}
