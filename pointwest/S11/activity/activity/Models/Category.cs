﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace activity.Models
{
    public enum Category
    {
        Food,
        Electronics,
        Clothing,
        Furniture
    }
}