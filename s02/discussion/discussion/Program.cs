﻿using System;

namespace Discussion
{
    class Discussion
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");

            // [SECTION] Variables, Data and Constants
            String myString = "Zuitt Asset Manager";

            //Value Type
            int myInt = 1;
            float myFloat = 5.7F;
            double myDouble = 2.5D;
            decimal myDecimal = 8.4M;
            long myLong = 57390L;
            bool myBoolean = false;

            const double interest = 5.3;

            Console.WriteLine(interest);

            // [SECTION] Concatenation and String Interpolation
            //Concatenation
            Console.WriteLine("Printing Values using Concatenation");
            Console.WriteLine("double: " + myDouble + ", decimal: " + myDecimal);

            //String Interpolation
            Console.WriteLine("Printing Values using String Interpolation");
            Console.WriteLine("double: {0}, decimal: {1}", myDouble, myDecimal);
            Console.WriteLine($"double: {myDouble}, decimal: {myDecimal}");

            //[SECTION] Type Conversion / Type Casting
            //int < float < double
            // Implicit Type Casting
            //Smaller to bigger
            float anotherFloat = myInt;
            double anotherDouble = myFloat;

            //Explicit Type Casting
            //Bigger to smaller
            double convertedDouble = (double)myFloat;
            String convertedString = myInt.ToString();

            //[SECTION] User Inputs via the console.
            // int userInput = Convert.ToInt32(Console.ReadLine());
            // Console.WriteLine($"The number you provided is {userInput}");

            //Checking if the userInput is blank
            String stringInput = Console.ReadLine();
            Console.WriteLine("Result from isNullorWhiteSpace:");
            Console.WriteLine(string.IsNullOrWhiteSpace(stringInput));

            //[SECTION] Operators

            // [Section] Operators
            int numA = 10;
            int numB = 3;

            // Arithmetic Operators
            Console.WriteLine("Result of Arithmetic Operators:");
            Console.WriteLine(numA + numB);
            Console.WriteLine(numA - numB);
            Console.WriteLine(numA * numB);
            Console.WriteLine(numA / numB);
            Console.WriteLine(numA % numB);

            // Increment
            numA++;
            Console.WriteLine(numA);
            // Decrement
            numA--;
            Console.WriteLine(numA);

            // Relational Operators;
            Console.WriteLine("Result of Relational Operators:");
            Console.WriteLine(numA == numB);
            Console.WriteLine(numA != numB);
            Console.WriteLine(numA > numB);
            Console.WriteLine(numA < numB);
            Console.WriteLine(numA >= numB);
            Console.WriteLine(numA <= numB);

            // Logical Operators
            Console.WriteLine("Result of Logical Operators:");
            Console.WriteLine(numA == numB || numA > numB);
            Console.WriteLine(numA == numB && numA > numB);
            Console.WriteLine(!myBoolean);

            // Assignment Operators
            Console.WriteLine("Result of Assignment Operators:");
            Console.WriteLine(numA += numB);
            Console.WriteLine(numA -= numB);
            Console.WriteLine(numA *= numB);
            Console.WriteLine(numA /= numB);
            Console.WriteLine(numA %= numB);

            //printHelloWorld();
            String myMessage = printHelloWorld();
            Console.WriteLine(myMessage);

            Console.WriteLine("The Result from Function Arguments and Parameters:");
            Console.WriteLine(printName("John", "Doe"));


        }

        //[SECTION] Functions

        public static String printHelloWorld()
        {
            Console.WriteLine("Hello World");
            return "Hello World";
        }

        public static String printName(String firstName, String lastName)
        {
            return $"The name provided is: {firstName} {lastName}";
        }


    }
}