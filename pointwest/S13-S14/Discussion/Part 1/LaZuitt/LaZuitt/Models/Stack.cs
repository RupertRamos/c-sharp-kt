﻿using LaZuitt.Models;
using System.ComponentModel.DataAnnotations;

namespace LaZuitt.Models
{
    public enum Stack
    {
        MEAN,
        MERN,
        Django,
        [Display(Name = "Ruby on Rails")]
        Rails,
        LAMP
    }
}
