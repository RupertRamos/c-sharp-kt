﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    internal partial class Laptop
    {
        private string graphicsCard;

        public Laptop() { }

        public Laptop(string graphicsCard, string memory)
        {
            this.graphicsCard = graphicsCard;
            this.memory = memory;
        }

        public string GraphicsCard { get => graphicsCard; set => graphicsCard = value; }
    }
}
