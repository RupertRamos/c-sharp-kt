# Session Objectives

At the end of the session, the students are expected to:

- make app data persistent by using volumes.

# Resources

## Instructional Materials

- [GitLab Repository (App)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/NodeJs-App)
- [GitLab Repository (Dockerized App)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/Dockerized-Apps/NodeJs-App)
- [GitLab Repository (Guide)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1eeth9ODg5wgb9tiHNfY7YFOym8xwO2PgE-mpsUS8j7k/edit)

## Supplemental Materials

- [Part 5: Persist the DB (Docker Docs)](https://docs.docker.com/get-started/05_persisting_data/)
- [Part 6: Use Bind Mounts (Docker Docs)](https://docs.docker.com/get-started/06_bind_mounts/)

# Lesson Proper

The contents of this guide is based from the Getting Started contents of Docker Docs and is built upon it.

## Introduction

In case you did not notice, our todo list is being wiped clean every single time we launch the container. Why is this? Let's dive into how the container is working.

## The Container Filesystem

When a container runs, it uses the various layers from an image for its filesystem. Each container also gets its own "scratch space" to create/update/remove files. Any changes will not be seen in another container, *even if* they are using the same image.

To see this in action, we are going to start two containers and create a file in each. What you will see is that the files created in one container are not available in another.

Start an `ubuntu` container that will create a file named `/data.txt` with a random number between 1 and 10000.

```bash
docker run -d ubuntu bash -c "shuf -i 1-10000 -n 1 -o /data.txt && tail -f /dev/null"
```

Wait for the command to finish.

![Untitled](readme-images/Untitled.png)

In the command above, we are starting a bash shell and invoking two commands (why we have the `&&`). The first portion picks a single random number and writes it to `/data.txt`. The second command is simply watching a file to keep the container running.

Validate that we can see the output by `exec`ing into the container. To do so, open the Docker Desktop and click the first action of the container that is running the `ubuntu` image.

![Untitled](readme-images/Untitled%201.png)

You will see a terminal that is running a shell in the ubuntu container. Run the following command to see the content of the `/data.txt` file. Close this terminal afterwards again.

```bash
cat /data.txt
```

If you prefer the command line you can use the `docker exec` command to do the same. You need to get the container's ID (use `docker ps` to get it) and get the content with the following command.

```bash
docker exec <container-id> cat /data.txt
```

![Untitled](readme-images/Untitled%202.png)

You should see a random number in the commands used above.

Now, let's start another `ubuntu` container (the same image) and we will see we do not have the same file.

```bash
docker run -i -t ubuntu ls /
```

And look! There is no `data.txt` file there! That is because it was written to the scratch space for only the first container.

The `-i` and `-t` flags can be used together as `-it` in order to indicate the use of shell command from within the container. In the code above, after specifying the `ubuntu` image, we used the `ls /` command to view the files and directories of the system.

![Untitled](readme-images/Untitled%203.png)

Go ahead and remove the first container using the `docker rm -f <container-id>` command.

## Container Volumes

With the previous experiment, we saw that each container starts from the image definition each time it starts. While containers can create, update, and delete files, those changes are lost when the container is removed and all changes are isolated to that container. With volumes, we can change all of this.

[Volumes](https://docs.docker.com/storage/volumes/) provide the ability to connect specific filesystem paths of the container back to the host machine. If a directory in the container is mounted, changes in that directory are also seen on the host machine. If we mount that same directory across container restarts, we’d see the same files.

There are two main types of volumes. We will eventually use both, but we will start with **named volumes**.

## Persist the Todo Data

By default, the todo app stores its data in a [SQLite Database](https://www.sqlite.org/index.html) at `/etc/todos/todo.db` in the container’s filesystem. If you are not familiar with SQLite, no worries! It is simply a relational database in which all of the data is stored in a single file. While this is not the best for large-scale applications, it works for small demos. We will talk about switching this to a different database engine later.

With the database being a single file, if we can persist that file on the host and make it available to the next container, it should be able to pick up where the last one left off. By creating a volume and attaching (often called "mounting") it to the directory the data is stored in, we can persist the data. As our container writes to the `todo.db` file, it will be persisted to the host in the volume.

As mentioned, we are going to use a **named volume**. Think of a named volume as simply a bucket of data. Docker maintains the physical location on the disk and you only need to remember the name of the volume. Every time you use the volume, Docker will make sure the correct data is provided.

Create a volume by using the `docker volume create` command.

```bash
docker volume create todo-db
```

Stop and remove the todo app container once again in the Docker Desktop (or with `docker rm -f <id>`), as it is still running without using the persistent volume.

Go to the directory of the app source code, open a terminal in the directory, then start the todo app container. However, add the `v` flag to specify a volume mount. We will use the named volume and mount it to `/etc/todos`, which will capture all files created at the path.

```bash
docker run -dp 3000:3000 -v todo-db:/etc/todos lastname-nodejs-todo
```

Once the container starts up, open the app and add a few items to your todo list.

![Untitled](readme-images/Untitled%204.png)

Stop and remove the container for the todo app. Use the Docker Desktop or `docker ps` to get the ID and then `docker rm -f <id>` to remove it.

Start a new container using the same command from above.

Open the app. You should see your items still in your list!

Go ahead and remove the container when you are done checking out your list.

Hooray! You have now learned how to persist data!

While named volumes and bind mounts (which we will talk about in a minute) are the two main types of volumes supported by a default Docker engine installation, there are many volume driver plugins available to support NFS, SFTP, NetApp, and more! This will be especially important once you start running containers on multiple hosts in a clustered environment with Swarm, Kubernetes, etc.

## Dive Into the Volume

A lot of people frequently ask "Where is Docker *actually* storing my data when I use a named volume?" If you want to know, you can use the `docker volume inspect` command.

```bash
docker volume inspect todo-db
```

![Untitled](readme-images/Untitled%205.png)

The `Mountpoint` is the actual location on the disk where the data is stored. Note that on most machines, you will need to have root access to access this directory from the host. But, that is where it is!

While running in Docker Desktop, the Docker commands are actually running inside a small VM on your machine. If you wanted to look at the actual contents of the Mountpoint directory, you would need to first get inside of the VM.

At this point, we have a functioning application that can survive restarts! We can show it off to our investors and hope they can catch our vision!

However, we saw earlier that rebuilding images for every change takes quite a bit of time. There's got to be a better way to make changes, right? With bind mounts (which we hinted at earlier), there is a better way! Let's take a look at that now!

## Quick Volume Type Comparisons

Earlier, we talked about and used a **named volume** to persist the data in our database. Named volumes are great if we simply want to store data, as we do not have to worry about *where* the data is stored.

With **bind mounts**, we control the exact mountpoint on the host. We can use this to persist data, but it is often used to provide additional data into containers. When working on an application, we can use a bind mount to mount our source code into the container to let it see code changes, respond, and let us see the changes right away.

For Node-based applications, [nodemon](https://npmjs.com/package/nodemon) is a great tool to watch for file changes and then restart the application. There are equivalent tools in most other languages and frameworks.

Bind mounts and named volumes are the two main types of volumes that come with the Docker engine. However, additional volume drivers are available to support other uses cases ([SFTP](https://github.com/vieux/docker-volume-sshfs), [Ceph](https://ceph.com/geen-categorie/getting-started-with-the-docker-rbd-volume-plugin/), [NetApp](https://netappdvp.readthedocs.io/en/stable/), [S3](https://github.com/elementar/docker-s3-volume), and more).

|  | Named Volumes | Bind Mounts |
| --- | --- | --- |
| Host Location | Docker chooses | You control |
| Mount Example (using -v) | volume:/usr/local/data | /path/to/data:/usr/local/data |
| Populates new volume with container contents | Yes | No |
| Supports Volume Drivers | Yes | No |

## Start a Dev-Mode Container

To run our container to support a development workflow, we will do the following:

- Mount our source code into the container.
- Install all dependencies, including the "dev" dependencies.
- Start nodemon to watch for filesystem changes.

So, let's do it!

Make sure you do not have any previous `lastname-nodejs-todo` containers running.

Run the following command. We will explain what is going on afterwards:

```bash
docker run -dp 3000:3000 \
     -w /app -v "$(pwd):/app" \
     node:12-alpine \
     sh -c "yarn install && yarn run dev"
```

If you are using PowerShell then use this command:

```powershell
docker run -dp 3000:3000 `
     -w /app -v "$(pwd):/app" `
     node:12-alpine `
     sh -c "yarn install && yarn run dev"
```

The commands above are different due to how Powershell allows multi-line command execution.

In the command above, we will do the following:

- `dp 3000:3000` - same as before. Run in detached (background) mode and create a port mapping
- `w /app` - sets the "working directory" or the current directory that the command will run from
- `v "$(pwd):/app"` - bind mount the current directory from the host in the container into the `/app` directory
- `node:12-alpine` - the image to use. Note that this is the base image for our app from the Dockerfile
- `sh -c "yarn install && yarn run dev"` - the command. We're starting a shell using `sh` (alpine does not have `bash`) and running `yarn install` to install *all* dependencies and then running `yarn run dev`. If we look in the `package.json`, we’ll see that the `dev` script is starting `nodemon`.

You can watch the logs using `docker logs -f <container-id>` or by going to the Docker Desktop and clicking the created container. You'll know you're ready to go when you see this:

![Untitled](readme-images/Untitled%206.png)

If you used the `docker logs` command, exit out by hitting `Ctrl`+`C`.

Now, let's make a change to the app. In the `src/static/js/app.js` file, let’s change the "Add Item" button to simply say "Add". This change will be on line 109:

```jsx
- {submitting ? 'Adding...' : 'Add Item'}
+ {submitting ? 'Adding...' : 'Add'}
```

Simply refresh the page (or open it) and you should see the change reflected in the browser almost immediately. It might take a few seconds for the Node server to restart, so if you get an error, just try refreshing after a few seconds.

![Untitled](readme-images/Untitled%207.png)

Feel free to make any other changes you’d like to make. When you're done, stop the container and build your new image using `docker build -t getting-started .`.

Using bind mounts is *very* common for local development setups. The advantage is that the dev machine does not need to have all of the build tools and environments installed. With a single `docker run` command, the dev environment is pulled and ready to go. We'll talk about Docker Compose in a future step, as this will help simplify our commands (we're already getting a lot of flags).

At this point, we can persist our database and respond rapidly to the needs and demands of our investors and founders. Hooray! But, guess what? We received great news!

**Your project has been selected for future development!**

In order to prepare for production, we need to migrate our database from working in SQLite to something that can scale a little better. For simplicity, we'll keep with a relational database and switch our application to use MySQL. But, how should we run MySQL? How do we allow the containers to talk to each other? We'll talk about that next!

# Activity

## Quiz

### Instructions

Answer the quiz form provided by your instructor.

For instructors, create a copy of this [quiz form](https://docs.google.com/forms/d/1hIWvKH609L4CuLdmHS0wT9R_sjJBiojgZD3loKTOEJU/edit) to your own batch kit folder.

### Answers

What was the symbol used to execute two or more shell commands in succession.

- &&

Docker command to execute a shell command from within the container.

- docker exec

Provides the ability to connect specific filesystem paths of the container back to the host machine.

- volume, volumes

A relational database in which all of the data is stored in a single file.

- SQLite, sqlite

Docker command to determine where volume data are actually stored.

- docker volume inspect

This property name is the actual location on the disk where the data is stored.

- Mountpoint

Docker command to view or review the logs emitted by a container.

- docker logs