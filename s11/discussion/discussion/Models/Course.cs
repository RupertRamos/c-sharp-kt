﻿using System.ComponentModel.DataAnnotations;

namespace discussion.Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 10)]
        public string Title { get; set; } = null!;

        [Required]
        public Stack Stack { get; set; }

        [Required]
        public string Description { get; set; } = null!;

        [Required]
        // Used to validate that the price input should be in currency value
        [DataType(DataType.Currency)]
        public double Price { get; set; }

        [Required]
        // Used to validate that the stackURL is a valid link
        [DataType(DataType.Url)]
        [Display(Name = "Stack URL")]
        public string StackUrl { get; set;} = null!;

        [Required]
        // Used to validate that the "Logo" field is a valid image url
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Logo")]
        public string Logo { get; set; } = null!;

    }
}
