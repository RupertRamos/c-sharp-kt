﻿using System;
using System.Runtime.ConstrainedExecution;

namespace activity
{
    class Activity
    {

        static void Main(string[] args)
        {

            Admin adminAccount = new Admin("Jane Smith", "05-01-83", true, "Lead Developer");
            adminAccount.AddProduct();

            Premium myAccount = new Premium("John Smith", "10-24-21", false);
            Console.WriteLine("Result from a Premium user:");
            myAccount.Login();
            myAccount.Checkout();

            Regular unknownUser = new Regular("Joe Garcia", "02-24-90", false);
            Console.WriteLine("Result from a Regular user:");
            unknownUser.Login();
            unknownUser.Checkout();

        }

    }
}