﻿using LaZuitt.Models;

using System.ComponentModel.DataAnnotations;

namespace LaZuitt.Models
{
    public class Course
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Title { get; set; } = null!;

        [Required]
        public Stack Stack { get; set; }

        [Required]
        public string Description { get; set; } = null!;

        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }

        [Required]
        [DataType(DataType.Url)]
        [Display(Name = "Stack URL")]
        public string StackUrl { get; set; } = null!;

        [Required]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Logo")]
        public string Logo { get; set; } = null!;
    }
}

