# Session Objectives

At the end of the session, the students are expected to:

- learn to create routes with controller
- Implement a Restful Web API
- Utilize the use the HTTP Methods

# Resources

## Instructional Materials

- [GiLab Repository](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/tree/main/05_06_ASPNET_RESTful_EF_Core/Discussion/Part%201/LaZuitt/LaZuitt)
- [Google Slides Presentation](https://docs.google.com/presentation/d/1uGbst2FvqDK85wXBYokjeAaaNBoB3fwilPJ3xoN69hY/edit#slide=id.g15ca2785fc4_0_391)

## Supplemental Materials

- [Restful Web API](https://medium.com/net-core/build-a-restful-web-api-with-asp-net-core-6-30747197e229)
- [Rest APIs](https://www.ibm.com/cloud/learn/rest-apis)
- [Http Method](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)
- [Http Messages](https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages)
- [Web Request Anatomy](https://www.realisable.co.uk/support/documentation/iman-user-guide/DataConcepts/WebRequestAnatomy.htm)
- [Restful API Video](https://www.youtube.com/watch?v=SLwpqD8n3d0)

# Lesson Proper

## Part 1

# What is a REST API?

**REST API (also known as RESTful API)** is an application programming interface. Usually, HTTPS communication protocol accesses the Restful app programming interface.

An API, or application programming interface, is a set of rules that define how applications or devices can connect to and communicate with each other. he developer creates the API on the server and allows the client to talk to it. A REST API is an API that conforms to the design principles of the REST, or representational state transfer architectural style. For this reason, REST APIs are sometimes referred to RESTful APIs.


## What is an REST?

REST determines how the API looks like. It stands for “Representational State Transfer”. It is a set of rules that developers follow when they create their API. One of these rules states that you should be able to get a piece of data (called a resource) when you link to a specific URL.

Each URL is called a **request** while the data sent back to you is called a **response**.

* Request - requests sent by the client to trigger an action on the server
* Response - the answer from the server.

### How REST APIs work

REST APIs communicate via HTTP requests to perform standard database functions like creating, reading, updating, and deleting records (also known as CRUD) within a resource.


* Client - Initiates communication through a request.

* Server - Receives & processes the request and sends a response in return. It is the process of getting or saving the data. It has also a bunch of services that can be accessed thru http protocol.

__A client can access the server services by sending a HTTP request, a server will send its response__

![Client and Server](https://www.mindinventory.com/blog/wp-content/uploads/2021/09/rest-api-model-1.png)

### HTTP REQUEST

A request is made up of several parts: the URL; the HTTP method; any headers; and the body.

### HTTP RESPONSE
After processing the request, the server will send a response.

A response contains a response code (to indicate the success or failure of a request), a set of headers and a body.

### Anatomy of a Request

1.  Endpoint 

    Endpoint is the url you request for.

2.  Method

    The method is the type of request you send to the server. They are used to perform four possible actions: Create, Read, Update and Delete (CRUD).

3.  Headers

    HTTP headers let the client and the server pass additional information with an HTTP request or response. An HTTP header consists of its case-insensitive name followed by a colon (:), then by its value. Whitespace before the value is ignored.



4.  Data or Body

    The data (sometimes called “body” or “message”) contains information you want to be sent to the server. This option is only used with POST, PUT, PATCH or DELETE requests.

    <br/>
    
### HTTP Methods

|HTTP Methods | Crud Operation|
|-------------|---------------|
|1.  GET      |Read           | 
|2.  POST     |Create         |
|3.  DELETE   |Delete         |
|4.  PUT      |Update         |

<br/>

1. GET

    The GET method requests a representation of the specified resource. Requests using GET should only retrieve data.

2.  POST

    The POST method submits an entity to the specified resource, often causing a change in state or side effects on the server.

3.  PUT

    The PUT method replaces all current representations of the target resource with the request payload.

4.  DELETE

    The DELETE method deletes the specified resource.



<br/>

# Creating API Controller and Methods

1.  Let’s add the controller first. Right-click on the Controller folder and select Add -> New File.

    ![Right Click on Controller](./Images/01_Click_Controller.png)

    <br/>

2.  Click **ASP.Net Core** and select **Controller Class**. Then let's name our Controller as **SubjectController** and click **Create**.

    ![Click ASP](./Images/02_Click_ASP.png)

    Now, we have our **Subject Controller**

    ![Click ASP](./Images/03_Subject_Controller.png)

    <br/>

3.  Let's call package **Microsoft.EntityFrameworkCore** to use database context and call our models. And let's addd a Route by adding this code and change the Controller into ControllerBase

    ```
    using LaZuitt.Models;
    using Microsoft.EntityFrameworkCore;
    ```

    and to use Route we should add this code.

    ```
    [Route("api/[controller]")]
    [ApiController]
    ```

4. And to inherit the database context, edit the **public class SubjectController : Controller** to **public class SubjectController : ControllerBase**
    
    ![Add LaZuitt Models](./Images/04_Edit_code.png)


5.  Next, we will inject the database context mentioned in the previous section through the constructor of the controller. Let's edit and add the following code:

        ```
        private readonly CoursesContext _context;

            public SubjectController(CoursesContext context)
            {
                _context = context;
            }
        ```

    ![Inject DB](./Images/05_Inject_DB.png)

<br/>

### Now, we will add CRUD (create, read, update, and delete) action methods to the controller. Let’s start with the GET methods.

<br/>

## GET METHOD

6.  Add the following code to the SubjectController:    

    **SubjectController.cs**
    ```
    // GET: api/Subject
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Course>>> GetSubject()
    {
        return await _context.Course.ToListAsync();
    }
    ```

    ![Get Method](./Images/06_Get_Method.png)

    <br/>

    **GetSubject** method returns all the courses.

    <br/>

    These method implement GET endpoints:

    **GET /api/Subject**

    We can test the app by calling the endpoint from a browser as follows:

    _Note : Start with the template string in the controller’s Route attribute (Route("api/[controller]")). Then replace [controller] with the name of the controller, which by convention is the controller class name minus the Controller suffix. For this sample, the controller class name is SubjectController, so the controller name is subject._
    
    * https://localhost:{port}/api/Subject

    <br/>

    ---

    Let's run the code and copy the url in browser 
    
    <br/>

    ![Get Method](./Images/06_Copy_url.png)


    and edit and run through our browser. In my case is **https://localhost:5001/api/Subject**


    
    ![Get Method](./Images/06_Get_Web_Result.png)

    **Test our code in Postman.** 
    
    Insert the the url and select GET method and click send.

     ![Get Method](./Images/06_Get_PM_Result.png)  

    As we see, we can check the result on the lower part(Highlighted with blue) with our HTTP status code 200 OK. 

    We can also check the content of our database.

    ![Get Method](./Images/06_DB_Content.png)

<br/>

## GET METHOD for a specified ID

7.  Let's add our code to **SubjectController.cs** to check a single course.

    **SubjectController.cs**
    ```
    // GET: api/Subject/id
    [HttpGet("{id}")]
    public async Task<ActionResult<Course>> GetTest(int id)
    {
        var course = await _context.Course.FindAsync(id);

        if (course == null)
        {
            return NotFound();
        }

        return course;
    }
    ```

    ![Get Id Method](./Images/07_Get_Id.png)

    <br/>

    GetSubject(int id) method returns the course having the Id given as input. Let's try to run our code in **Postman**. 

    * https://localhost:{port}/api/Subject/Id

    <br/>

    **Test our code in Postman.** 
    
    Insert the the url and select GET method and click send.

    ![Get Id Method](./Images/07_Get_Id_PM_Result.png)

    The highlighted blue is the result that shows from our GET Method for a specific Id. The result is the details of our stated id which is a single subject and with our HTTP status code 200 OK. 

<br/>

## POST METHOD

8.  Add the following code to the SubjectController:

    ![Post Method](./Images/08_Post_Code.png)


    PostSubject method creates a course record in the database. The preceding code is an HTTP POST method, as indicated by the [HttpPost] attribute. The method gets the value of the course record from the body of the HTTP request.

    The CreatedAtAction method:

    *   Returns an HTTP 201 status code, if successful. HTTP 201 is the standard response for an HTTP POST method that creates a new resource on the server.
    *   Adds a Location header to the response. The Location header specifies the URI of the newly created course record.
    *   References the GetSubject action to create the Location header's URI.

    <br/>

    **Testing the PostSubject Method**

    Insert the URL and select Post Method. Since we are going a pass an input, select Body, and then select raw and choose JSON from the dropdown. And insert the data in JSON format in the Body on the lower part of our Postman.

    ![Post Method in Postman](./Images/08_Post_PM_Result.png)

    We can also check our database by reloading it.

    ![Check DB](./Images/08_DB_post.png)    

<br/>

## PUT METHOD

9.  Add the following code to the SubjectController:

    ```
    // PUT: api/Subject
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubject(int id, Course course)
        {
            if (id != course.Id)
            {
                return BadRequest();
            }

            _context.Entry(course).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CourseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool CourseExists(int id)
        {
            return _context.Course.Any(e => e.Id == id);
        }
    ```

    ![Put Method Code](./Images/09_Put_Code.png)
    
    <br/>

    PutSubject method updates the course record with the given Id in the database. The preceding code is an HTTP PUT method, as indicated by the [HttpPut] attribute. The method gets the value of the course record from the body of the HTTP request. **You need to supply the Id both in the request URL and the body and they have to match.** According to the HTTP specification, **a PUT request requires the client to send the entire updated entity, not just the changes.**

    The response is 204 (No Content) if the operation is successful.

    </br>

    **Testing the PutSubject Method**

    Now, let's try to change the last Course we input which is React JS.

    ![Check DB](./Images/08_DB_post.png)

    Insert the URL and select Put Method. Just like post method, we are going a pass an input, select Body, and then select raw and choose JSON from the dropdown. And insert the data in JSON format in the Body. 
    
    ![Check DB](./Images/09_Put_PM_Result.png)

    Let's check and reload our database.

    ![Check DB](./Images/09_DB_put.png)

<br/>

## DELETE METHOD

10. Add the following code to the SubjectController:

    ```
    [HttpDelete("{id}")]
        public async Task<ActionResult<Course>> DeleteSubject(int id)
        {
            var course = await _context.Course.FindAsync(id);
            if (course == null)
            {
                return NotFound();
            }

            _context.Course.Remove(course);
            await _context.SaveChangesAsync();

            return course;
        }
    ```

    <br/>

    Let's insert it in between of Put method and CourseExists.

    ![Check DB](./Images/10_Delete_Code.png)
    
    <br/>

    DeleteSubject method deletes the course record with the given Id in the database. The preceding code is an HTTP DELETE method, as indicated by the [HttpDelete] attribute. This method expects Id in the URL to identify the course record we want to delete.

    **Testing the DeleteSubject Method**

    Now, let's try to delete the course we added and edited which is React JS Advance. Insert the URL with Id and select Delete Method. Then click send.

    ![Check DB](./Images/10_Delete_PM_Result.png)

    <br/>

    Let's check the database.

    ![Check DB](./Images/10_DB_delete.png)





    

    