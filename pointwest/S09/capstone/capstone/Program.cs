﻿using capstone;
using System;
using System.Collections;

namespace Capstone
{
    class capstone
    {

        static void Main(string[] args)
        {

            Application application = new Application();

            // Add test accounts to avoid user input on every test
            User testAdmin = new User("admin", "admin@mail.com", "admin123", "123 Washington St.", "09123456789", true);
            User testUser = new User("johndoe", "john@mail.com", "john1234", "123 Washington St.", "09123456789", false);
            Item testItem = new Item("Beef", "Manila", 100, 20, 10, 110);

            application.UsersDB.Users.Add(testAdmin);
            application.UsersDB.Users.Add(testUser);
            application.ItemsDB.Items.Add(testItem);

            application.OpenLoginMenu();

        }

    }
}