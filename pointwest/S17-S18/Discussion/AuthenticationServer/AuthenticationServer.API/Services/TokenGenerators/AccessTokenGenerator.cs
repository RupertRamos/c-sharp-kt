﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AuthenticationServer.API.Models;
using Microsoft.IdentityModel.Tokens;

namespace AuthenticationServer.API.Services.TokenGenerators
{
    public class AccessTokenGenerator
    {
        private readonly AuthenticationConfiguration _configuration;
        private readonly TokenGenerator _tokenGenerator;

        public AccessTokenGenerator(AuthenticationConfiguration configuration, TokenGenerator tokenGenerator)
        {
            _configuration = configuration;
            _tokenGenerator = tokenGenerator;
        }



        // This weill generate our token.
        public string GenerateToken(User user)
        {

            // Claims - will contain the user information that is going to login.
            // In here we are creating a list of claims
            List<Claim> claims = new List<Claim>()
            {
                new Claim("id", user.Id.ToString()),

                // Claimtypes is given in Security.Claims library
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Name, user.UserName)

            };

            return _tokenGenerator.GenerateToken(
                _configuration.AccessTokenSecret,
                _configuration.Issuer,
                _configuration.Audience,
                _configuration.AccessTokenExpirationMinutes,
                claims
                );
        }
    }
}

