﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    //[SECTION] Classes
    internal class Item
    {
        //Fields
        private string name;
        private readonly string branch;
        private int beginningInventory;
        private int stockIn;
        private int stockOut;
        private int totalBalance;


        // Getters and Setters
        public string Name { get => name; set => name = value; }
        public string Branch { get => branch; }
        public int BeginningInventory { get => beginningInventory; set => beginningInventory = value; }
        public int StockIn { get => stockIn; set => stockIn = value; }
        public int StockOut { get => stockOut; set => stockOut = value; }
        public int TotalBalance { get => totalBalance; set => totalBalance = value; }

        

        //Constructors

        //Empty Constructors

        public Item () 
        {

        }


        //Parametarized Constructor

        public Item(string name, string branch, int beginningInventory, int stockIn, int stockOut, int totalBalance)
        {
            this.name = name;
            this.branch = branch;
            this.beginningInventory = beginningInventory;
            this.stockIn = stockIn;
            this.stockOut = stockOut;
            this.totalBalance = totalBalance;
        }

        //Methods
        public override string ToString()
        {
            return $"{{Name: {name}, Branch: {branch}, Beginning Inventory: {beginningInventory}, Stock In: {stockIn}, Stock Out: {stockOut}, Total Balance: {totalBalance}}}";
        }



    }
}
