﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace activity
{
    internal class Admin : User
    {
        private string role;

        public Admin() { }
        public Admin(string name, string joinDate, bool isAdmin, string role)
        {
            Name = name;
            JoinDate = joinDate;
            IsAdmin = isAdmin;
            this.role = role;
        }

        public string Role { get => role; set => role = value; }

        public void AddProduct()
        {
            Console.WriteLine($"{Name} just added another product");
        }
    }
}
