﻿namespace activity
{
    class Activity
    {

        static void Main(string[] args)
        {

            Nokia myPhone = new Nokia("3310", 100, "Zuitt Electronics");
            Console.WriteLine("Result from Nokia Phone:");
            myPhone.SendText("Hello from the 90's", "John");
            myPhone.PlayMusic("Backstreet Boys - Shape of My Heart");

            Samsung myOtherPhone = new Samsung("Galaxy S5", 10000, "Market Suppliers");
            Console.WriteLine("Result from Samsung Phone:");
            myOtherPhone.SendText("Hw r u doing today?", "Jane");
            myOtherPhone.PlayMusic("Taylor Swift - Love Story");
            myOtherPhone.TakePicture();
            myOtherPhone.TakeVideo();
        }

    }
}