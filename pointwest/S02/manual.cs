// ======================================
// Data Types, Expressions and Statements
// ======================================

/*
Other References:
	C# Data Types
		https://www.tutorialspoint.com/csharp/csharp_data_types.htm
	C# Constants
		https://www.tutorialspoint.com/csharp/csharp_constants.htm
    C# String Interpolation
        https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/interpolated
	C# Type Conversion
		https://www.tutorialspoint.com/csharp/csharp_type_conversion.htm
	C# Operators
		https://www.tutorialspoint.com/csharp/csharp_operators.htm

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
========== 
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "C# Console App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Add code to discuss the initial code when creating a C# program.
    Application > discussion > Program.cs
*/

    	// The "using" keyword is used to include namespaces in a program
    	// A namespace is a collection of classes or code that can be imported within a project
        // The System namespace contains classes or code that can be used to perform specific tasks such as printing values in the console
        // In the .NET 6.0 framework, the importing the "System" namespace is no longer required but in previous .NET frameworks it is required to be able to access the "Console" class
    	using System;

    	// The "namespace" keyword is used to declare a namespace
    	namespace Discussion {
    	    class Discussion
    	    {
    	        // Serves as the entry point of the application
    	        static void Main(string[] args)
    	        {
    	            // Allows to printout values and messages in the console
    	            Console.WriteLine("Hello World");
    	        }
    	    }
    	}

/*
3. Run the application to see the result of the code.
    Visual Studio
*/

    	/*
    	Important Note:
    		- To run the application, a "green play button" can be found in visual studio which will execute the code added in the previous step.
    		- Alternatively, pressing the "F5" button on the keyboard can be used to run the application.
    		- A debug console will appear to print out the "Hello World" message.
    		- If a project is opened via the "File > Open > Folder" option, running the application will automatically close the debug console.
    		- To open a project, use the "File > Open > Project/Solution" option instead and select the corresponding solution file for the project indicated by the ".sln" extension.
    	*/

/*
4. Add more code to demonstrate the use of variables, data types and constants.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            Console.WriteLine("Hello World");

    	            // [Section] Variables, Data Types and Constants

                    // Referencing Data Types
                    String myString = "Zuitt Asset Manager";

                    // Value Types
                    int myInt = 1;
                    float myFloat = 5.7F;
                    double myDouble = 2.5D;
                    decimal myDecimal = 8.4M;
                    long myLong = 57390L;
                    bool myBoolean = false;

                    const double interest = 5.3;

                    Console.WriteLine("Result of Data Types:");
                    Console.WriteLine(myString);
                    Console.WriteLine(myInt);
                    Console.WriteLine(myFloat);
                    Console.WriteLine(myDouble);
                    Console.WriteLine(myDecimal);
                    Console.WriteLine(myLong);
                    Console.WriteLine(myBoolean);
                    Console.WriteLine(interest);
    	        }
    	    }
    	}

    	/*
    	Important Note:
    		- Refer to "references" section of this file to find the documentations for C# Data Types and C# Constants.
    	*/

/*
5. Add more code to demonstrate the use of Concatenation and String Interpolation.
    Application > discussion > Program.cs
*/

        // ...

        namespace Discussion {
            class Discussion
            {
                static void Main(string[] args)
                {
                    // ...
                    Console.WriteLine(interest);

                    // [Section] Concatenation and String Interpolation
                    // Concatenating string and variable values
                    Console.WriteLine("Printing values using concatenation:");
                    Console.WriteLine("double: " + myDouble + ", decimal: " + myDecimal + ", boolean: " + myBoolean);
                    // Using double quotes (") in conjunction with curly braces ({}) allows makes it easier to create human readable string outputs
                    Console.WriteLine("Printing values using double quotes and curly braces:");
                    Console.WriteLine("double: {0}, decimal: {1}, boolean: {2}", myDouble, myDecimal, myBoolean);
                    // String Interpolation using the dollar ($) symbol can also be used for an even cleaner code
                    Console.WriteLine("Printing values using String Interpolation:");
                    Console.WriteLine($"double: {myDouble}, decimal: {myDecimal}, boolean: {myBoolean}");
                }
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentations for C# Data Types and C# Constants.
        */

/*
6. Add more code to demonstrate the use of type conversion/type casting.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            // ...
                    Console.WriteLine($"double: {myDouble}, decimal: {myDecimal}, boolean: {myBoolean}");

                    // [Section] Type Conversion / TypeCasting
                    // Implicit Type Conversion
                    // Conversions are done by C# without having to manually define the conversion from one type to the next
                    // This works on values converted from smaller to larger integral/number types (e.g. int < float < double)
                    float anotherFloat = myInt;
                    double anotherDouble = myFloat;

                    // Explicit Type Conversion
                    // Conversions are done by manually/explicitly defining the data type that a value is to be converted into
                    double convertedDouble = (double)myFloat;
                    String convertedString = myInt.ToString();
    	        }
    	    }
    	}

    	/*
    	Important Note:
    		- Refer to "references" section of this file to find the documentations for C# Type Conversion.
    	*/

/*
7. Add more code to demonstrate the use of user inputs via the console.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            // ...
                    String convertedString = myInt.ToString();

                    // [Section] User inputs via the console
                    // The "ToInt" method has 3 different bit integer values
                    // "ToInt16" converts a number to a short data type
                    // "ToInt32" converts a number to an int data type
                    // "ToInt64" converts a number to a long data type
                    Console.WriteLine("Please enter a number:");

                    int userInput = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("The number you provided is {0}", userInput);

                    // Checking if userInput is blank
                    String stringInput = Console.ReadLine();
                    Console.WriteLine("Result from isNullorWhiteSpace");
                    Console.WriteLine(string.IsNullOrWhiteSpace(stringInput));
    	        }
    	    }
    	}

/*
8. Add more code to demonstrate the use of operators.
    Application > discussion > Program.cs
*/

    	// ...

    	namespace Discussion {
    	    class Discussion
    	    {
    	        static void Main(string[] args)
    	        {
    	            // ...
                    Console.WriteLine(string.IsNullOrWhiteSpace(stringInput));

                    // [Section] Operators
                    int numA = 10;
                    int numB = 3;

                    // Arithmetic Operators
                    Console.WriteLine("Result of Arithmetic Operators:");
                    Console.WriteLine(numA + numB);
                    Console.WriteLine(numA - numB);
                    Console.WriteLine(numA * numB);
                    Console.WriteLine(numA / numB);
                    Console.WriteLine(numA % numB);

                    // Increment
                    numA++;
                    Console.WriteLine(numA);
                    // Decrement
                    numA--;
                    Console.WriteLine(numA);

                    // Relational Operators;
                    Console.WriteLine("Result of Relational Operators:");
                    Console.WriteLine(numA == numB);
                    Console.WriteLine(numA != numB);
                    Console.WriteLine(numA > numB);
                    Console.WriteLine(numA < numB);
                    Console.WriteLine(numA >= numB);
                    Console.WriteLine(numA <= numB);

                    // Logical Operators
                    Console.WriteLine("Result of Logical Operators:");
                    Console.WriteLine(numA == numB || numA > numB);
                    Console.WriteLine(numA == numB && numA > numB);
                    Console.WriteLine(!myBoolean);

                    // Assignment Operators
                    Console.WriteLine("Result of Assignment Operators:");
                    Console.WriteLine(numA += numB);
                    Console.WriteLine(numA -= numB);
                    Console.WriteLine(numA *= numB);
                    Console.WriteLine(numA /= numB);
                    Console.WriteLine(numA %= numB);
    	        }
    	    }
    	}

    	/*
    	Important Note:
    		- Refer to "references" section of this file to find the documentations for C# Operators.
    	*/

/*
9. Add more code to demonstrate the use of functions.
    Application > discussion > Program.cs
*/

        /*
        9a. Create a function named "printHelloWorld" and invoke it.
        */

            // ...

            namespace Discussion
            {
                class Discussion
                {
                    // Serves as the entry point of the application
                    static void Main(string[] args)
                    {
                        // ...
                        Console.WriteLine(numA %= numB);

                        // Invoking functions
                        printHelloWorld();
                    }

                    // [Section] Functions
                    // Functions are reusable pieces of code that is used to execute a specific task
                    public static String printHelloWorld()
                    {
                        Console.WriteLine("Hello World");
                    }
                }
            }

        /*
        9b. Add a "return" statement to the printHelloWorld function, invoke it and store it's data in a variable.
        */

            // ...

            namespace Discussion
            {
                class Discussion
                {
                    // Serves as the entry point of the application
                    static void Main(string[] args)
                    {
                        // ...
                        Console.WriteLine(numA %= numB);

                        // Invoking functions
                        Console.WriteLine("Result from return printHelloWorld function:");
                        String myMessage = printHelloWorld();
                        Console.WriteLine("Result from return statement:");
                        Console.WriteLine(myMessage);
                    }

                    // [Section] Functions
                    // Functions are reusable pieces of code that is used to execute a specific task
                    public static String printHelloWorld()
                    {
                        Console.WriteLine("Hello World");
                        // The return statement is used to pass along a value to it's caller, this can be a variable or another function
                        // It also stops the execution of the function and ignores all other statements/blocks of code when reached
                        return "Hello World";

                        // The visual studio text editor will provide a warning that this statement will never be reached since it's included after the "return" statement
                        Console.WriteLine("This will never be printed");
                    }
                }
            }

        /*
        9c. Create a function named "printName" to demonstrate the use of arguments and parameters.
        */

            // ...

            namespace Discussion
            {
                class Discussion
                {
                    // Serves as the entry point of the application
                    static void Main(string[] args)
                    {
                        // ...
                        Console.WriteLine(myMessage);

                        Console.WriteLine("Result from arguments and parameters:");
                        printName("John", "Doe");
                    }

                    public static String printHelloWorld()
                    {
                        // ...
                    }

                    // Function parameters and arguments
                    // Parameters are variables that store data that functions will use to perform tasks and are used for function declarations
                    // Arguments are the actual data passed along to the function and is commonly used for function invocation/calling
                    // Being able to distinguish parameters from functions helps create a better understanding of code especially when reading documentation
                    public static String printName(String firstName, String lastName)
                    {
                        Console.WriteLine($"The name provided is: {firstName} {lastName}");
                        return $"The name provided is: {firstName} {lastName}";
                    }
                }
            }

/*
========
Activity
========
*/

/*
1. Create a simple console application that will take in the user's input to store variables and display them.
    Application > activity > Program.cs
*/

    	namespace Activity
        {
            class Activity
            {
                static void Main(string[] args)
                {
                    Console.WriteLine("Enter product name:");
                    string name = Console.ReadLine();

                    Console.WriteLine("Enter beginning inventory amount:");
                    int beginningInventory = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Enter stock in amount:");
                    int stockIn = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Enter stock out amount:");
                    int stockOut = Convert.ToInt32(Console.ReadLine());

                    int totalBalance = beginningInventory + stockIn - stockOut;
                    Console.WriteLine($"The EOD total balance of {name} is:");
                    Console.WriteLine(totalBalance);

                    Console.WriteLine("Enter price per unit:");
                    int pricePerUnit = Convert.ToInt32(Console.ReadLine());

                    const double markupValue = 0.15;
                    double incomePerUnit = pricePerUnit * markupValue;
                    Console.WriteLine($"The total sales for today is {totalBalance * incomePerUnit}.");
                }
            }
        }