﻿namespace discussion
{
    class Discussion
    {
        static void Main(string[] args)
        {
            Car myCar = new Car("Ferrari", "Ferrari SF90 Stradale");
            Car.Engine myEngine = new Car.Engine("V-12", 769, 8);
            Car.Tires myTires = new Car.Tires(32, 9, "Pirelli");

            myEngine.StartEngine();
            myEngine.StopEngine();
            myTires.CheckPressure();

            Honda myOtherCar = new Honda("Honda", "Honda Civic 2021", 130);
            Honda.Engine myOtherEngine = new Honda.Engine("V-Tec", 158, 4);
            Honda.Tires myOtherTires = new Honda.Tires(30, 10, "Honda");

            myOtherEngine.StartEngine();
            myOtherEngine.StopEngine();
            myOtherTires.CheckPressure();

            Laptop myLaptop = new Laptop("Up to GeForce RTX TI Laptop GPU", "16GB or 32 GB RAM");
            Console.WriteLine(myLaptop.GraphicsCard);
            Console.WriteLine(myLaptop.Memory);


        }
    }
}