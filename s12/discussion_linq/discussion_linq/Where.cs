﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion_linq
{
    public class Where
    {
        public void WhereOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            //Method Based
            //var data = courses.Where(c => c.Price < 15000);

            //Query Based
            var data = from c in courses where c.Price < 15000 select c;

            foreach (var item in data)
            {
                Console.WriteLine($"{item.Name} - P{item.Price}");
            }

        }
    }
}
