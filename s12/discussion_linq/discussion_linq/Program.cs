﻿namespace discussion_linq
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creates an instance of the Select class.
            discussion_linq.Select select = new discussion_linq.Select();

            //select.SelectOperator();
            //select.SelectNameAndPrice();

            discussion_linq.Where where = new discussion_linq.Where();
            //where.WhereOperator();

            discussion_linq.OrderByThenBy orderByThenBy = new discussion_linq.OrderByThenBy();
            //orderByThenBy.OrderByOperator();
            //orderByThenBy.ThenByOperator();

            discussion_linq.GroupBy groupBy = new discussion_linq.GroupBy();
            //groupBy.GroupByOperator();
            //groupBy.NewListGroupByOperator();
            groupBy.MoreGroupBySample();



        }
    }
}