﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    internal class MyException : Exception
    {
        //Constructor
        public MyException(string message): base(message) 
        {

        }
    }
}
