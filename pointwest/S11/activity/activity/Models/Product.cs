﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace activity.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 10)]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; } = null!;

        [Required]
        public Category Category { get; set; }

        [Required]
        public string Description { get; set; } = null!;

        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }

        [Required]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Product Image")]
        public string ProductImg { get; set; } = null!;
    }
}