﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capstone
{
    internal class UsersDB
    {
        private ArrayList users = new ArrayList();

        public UsersDB() { }
        public UsersDB(ArrayList users)
        {
            this.users = users;
        }

        public ArrayList Users { get => users; set => users = value; }

        public void GetAllUsers()
        {
            if (users.Count == 0)
            {
                Console.WriteLine("No users to display");
            }
            else
            {
                Console.WriteLine("Users List:");
                foreach (User user in users)
                {
                    Console.WriteLine(user.ToString());
                }
            }
        }

        public bool ValidateUsername(string username)
        {
            foreach (User user in users)
            {
                if (user.Username == username)
                {
                    return true;
                }

            }

            return false;
        }

        public bool ValidateEmail(string email)
        {
            foreach (User user in users)
            {
                if (user.Email == email)
                {
                    return true;
                }

            }

            return false;
        }

        public void AddUser()
        {
            Console.WriteLine("Enter username:");
            string username = Console.ReadLine();
            bool duplicateUsername = ValidateUsername(username);

            while (string.IsNullOrWhiteSpace(username) || duplicateUsername == true)
            {
                if (duplicateUsername == true)
                {
                    Console.WriteLine("Username already exists. Enter a valid username:");
                } else
                {
                    Console.WriteLine("Please enter a valid username:");
                }

                username = Console.ReadLine();
                duplicateUsername = ValidateUsername(username);
            }

            Console.WriteLine("Enter email:");
            string email = Console.ReadLine();
            bool duplicateEmail = ValidateEmail(email);

            while (string.IsNullOrWhiteSpace(email) || duplicateEmail == true)
            {
                if (duplicateEmail == true)
                {
                    Console.WriteLine("Email already exists. Enter a valid email:");
                }
                else
                {
                    Console.WriteLine("Please enter a valid email:");
                }


                email = Console.ReadLine();
                duplicateEmail = ValidateEmail(email);
            }

            Console.WriteLine("Enter password:");
            string password = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(password) || password.Length < 8)
            {
                Console.WriteLine("Enter a valid password:");
                password = Console.ReadLine();
            }

            Console.WriteLine("Confirm password:");
            string confirmPassword = Console.ReadLine();

            while ((string.IsNullOrWhiteSpace(confirmPassword) || confirmPassword.Length < 8) && password != confirmPassword)
            {
                Console.WriteLine("Passwords should match. Please enter a valid password:");
                password = Console.ReadLine();
                Console.WriteLine("Confirm password:");
                confirmPassword = Console.ReadLine();
            }

            Console.WriteLine("Enter address:");
            string address = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(address))
            {
                Console.WriteLine("Enter a valid address:");
                address = Console.ReadLine();
            }

            Console.WriteLine("Enter contact number: (Contact Number should be at least 11 digits)");
            string contactNumber = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(contactNumber) || contactNumber.Length < 11)
            {
                Console.WriteLine("Enter a valid contact number:");
                contactNumber = Console.ReadLine();
            }

            User newUser = new User(username, email, password, address, contactNumber, false);

            users.Add(newUser);
            Console.WriteLine("User successfully registered!");
            Console.WriteLine(newUser.ToString());
        }

        public void EditUser()
        {
            if (users.Count == 0)
            {
                Console.WriteLine("There are no users to edit. Please add users.");
            }
            else
            {
                Console.WriteLine("Provide the username of the user to edit:");
                string userToEdit = Console.ReadLine();

                int indexUserToEdit = -1;

                foreach (User user in users)
                {
                    if (userToEdit.ToLower() == user.Username.ToLower())
                    {
                        indexUserToEdit = users.IndexOf(user);
                    }
                }

                if (indexUserToEdit >= 0)
                {
                    Console.WriteLine("Enter username:");
                    string username = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(username))
                    {
                        Console.WriteLine("Enter a valid username:");
                        username = Console.ReadLine();
                    }

                    foreach (User user in users)
                    {
                        if (user.Username == username)
                        {
                            Console.WriteLine("Username already exists");
                        }
                        else
                        {
                            username = Console.ReadLine();
                        }
                    }

                    Console.WriteLine("Enter email:");
                    string email = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(email))
                    {
                        Console.WriteLine("Enter a valid branch:");
                        email = Console.ReadLine();
                    }

                    foreach (User user in users)
                    {
                        if (user.Email == email)
                        {
                            Console.WriteLine("Email already exists");
                        }
                        else
                        {
                            email = Console.ReadLine();
                        }
                    }

                    Console.WriteLine("Enter password:");
                    string password = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(password) || password.Length < 8)
                    {
                        Console.WriteLine("Enter a valid password:");
                        password = Console.ReadLine();
                    }

                    Console.WriteLine("Confirm password:");
                    string confirmPassword = Console.ReadLine();

                    while ((string.IsNullOrWhiteSpace(confirmPassword) || confirmPassword.Length < 8) && password != confirmPassword)
                    {
                        Console.WriteLine("Passwords should match. Please enter a valid password:");
                        password = Console.ReadLine();
                        Console.WriteLine("Confirm password:");
                        confirmPassword = Console.ReadLine();
                    }

                    Console.WriteLine("Enter address:");
                    string address = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(address))
                    {
                        Console.WriteLine("Enter a valid address:");
                        address = Console.ReadLine();
                    }

                    Console.WriteLine("Enter contact number: (Contact Number should be at least 11 digits)");
                    string contactNumber = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(contactNumber) || contactNumber.Length < 11)
                    {
                        Console.WriteLine("Enter a valid contact number:");
                        contactNumber = Console.ReadLine();
                    }

                    User updatedUser = new User(username, email, password, address, contactNumber, false);

                    users[indexUserToEdit] = updatedUser;

                    Console.WriteLine("User successfully updated");
                    Console.WriteLine(updatedUser.ToString());
                }

            }
        }

        public void DeleteItem()
        {
            if (users.Count == 0)
            {
                Console.WriteLine("There are no users to delete. Please add users.");
            }
            else
            {
                Console.WriteLine("Provide the username of the user to delete:");
                string userToDelete = Console.ReadLine();

                int indexUserToDelete = -1;

                foreach (User user in users)
                {
                    if (userToDelete.ToLower() == user.Username.ToLower())
                    {
                        indexUserToDelete = users.IndexOf(user);
                    }
                }

                Console.WriteLine("User successfully Deleted");
                users[indexUserToDelete].ToString();
                users.RemoveAt(indexUserToDelete);

            }

        }
    }
}
