﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion_linq
{
    public class Course
    {
        public int ID { get; set; }
        public string Name { get; set; } = null!;
        public decimal Price { get; set; }
        public string Author { get; set; } = null!;


        public Course(int iD, string name, decimal price, string author)
        {
            ID = iD;
            Name = name;
            Price = price;
            Author = author;
        }


    }
}
