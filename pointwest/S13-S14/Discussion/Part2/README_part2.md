# Session Objectives

At the end of the session, the students are expected to:

* Understand how routing works in ASP.NET MVC
* Understand URL patterns
* How controllers work, specifically:
	- Action method of the controller class
	- Action result types
	- Action selectors

# Resources

## Instructional Materials

* [GitLab Repository](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/tree/main/05_06_ASPNET_RESTful_EF_Core/Discussion/Part%201/LaZuitt/LaZuitt)
* [Google Slide Presentation](https://docs.google.com/presentation/d/1uGbst2FvqDK85wXBYokjeAaaNBoB3fwilPJ3xoN69hY/edit?usp=share_link)

# Supplemental Materials
* [Resource 1](https://www.tutorialsteacher.com/mvc/routing-in-mvc)
* [Resource 2](https://learn.microsoft.com/en-us/aspnet/core/mvc/controllers/routing?view=aspnetcore-6.0)


# Lesson Proper

# Routing in MVC

ASP.NET introduced Routing to eliminate the needs of mapping each URL with a physical file. Routing enables us to define a URL pattern that maps to the request handler. This request handler can be a file or class. In ASP.NET Webform application, request handler is .aspx file, and in MVC, it is the Controller class and Action method. For example, http://domain/students can be mapped to http://domain/studentsinfo.aspx in ASP.NET Webforms, and the same URL can be mapped to Student Controller and Index action method in MVC.

## Route
Route defines the URL pattern and handler information. All the configured routes of an application stored in RouteTable and will be used by the Routing engine to determine appropriate handler class or file for an incoming request.

The following figure illustrates the Routing process.

![data type](https://www.tutorialsteacher.com/Content/images/mvc/routing-process.png)

## Configure a Route

ASP.NET Core controllers use the **Routing** middleware to match the URLs of incoming requests and map them to **actions**. 

Actions are either **conventionally-routed** or **attribute-routed**. Placing a route on the controller or action makes it attribute-routed.

___


### Conventionally-routed Action

The ASP.NET Core MVC template generates conventional routing code similar to the following:

```csharp
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllersWithViews();

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

//======================================================
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
//======================================================

app.Run();
```

**MapControllerRoute** is used to create a single route. The single route is named default route. Most apps with controllers and views use a route template similar to the default route. REST APIs should use **attribute routing**.

```csharp
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
```


#### URL Pattern

The URL pattern is considered only after the domain name part in the URL. For example, the URL pattern *"{controller}/{action}/{id}"* would look like localhost:1234/{controller}/{action}/{id}. Anything after "localhost:1234/" would be considered as a controller name. The same way, anything after the controller name would be considered as action name and then the value of id parameter.

![data type](https://www.tutorialsteacher.com/Content/images/mvc/url-routing.png)

If the URL doesn't contain anything after the domain name, then the default controller and action method will handle the request. For example, **http://localhost:1234** would be handled by the **HomeController** and the **Index()** method as configured in the default parameter.

The following table shows which Controller, Action method, and Id parameter would handle different URLs considering the above default route.

![1](./Images/part2/1.png)

#### Multiple conventional routes

Multiple conventional routes can be configured by adding more calls to **MapControllerRoute** and **MapAreaControllerRoute**. Doing so allows defining multiple conventions, or to adding conventional routes that are dedicated to a specific action, such as:

```csharp
app.MapControllerRoute(name: "Student",
                pattern: "students/{id}",
                defaults: new { controller = "Student", action = "Index"});
app.MapControllerRoute(name: "default",
               pattern: "{controller=Home}/{action=Index}/{id?}");
```

As shown in the above code, the URL pattern for the **Student** route is *students/{id}*, which specifies that any URL that starts with **domainName/students**, must be handled by the **StudentController**. Notice that we haven't specified **{action}** in the URL pattern because we want every URL that starts with students should always use the **Index()** action of the **StudentController** class. We have specified the default controller and action to handle any URL request, which starts from **domainname/students**.

MVC framework evaluates each route in sequence. It starts with the first configured route, and if incoming URL doesn't satisfy the URL pattern of the route, then it will evaluate the second route and so on. In the above example, routing engine will evaluate the **Student** route first and if incoming URL doesn't start with **/students** then only it will consider the second route which is the default route.

The following table shows how different URLs will be mapped to the **Student** route:

![data type](./Images/part2/2.png)

#### Route Constraints

You can also apply restrictions on the value of the parameter by configuring route constraints. For example, the following route applies a limitation on the id parameter that the id's value must be numeric.

```csharp
app.MapControllerRoute(name: "Student",
                pattern: "students/{id}",
                defaults: new { controller = "Student", action = "Index"},
                constraints: new { id = @"\d+" });
```

So if you give non-numeric value for id parameter, then that request will be handled by another route or, if there are no matching routes, then **"The resource could not be found"** error will be thrown.

#### Route Names

The route names give the route a logical name. The named route can be used for URL generation. Using a named route simplifies URL creation when the ordering of routes could make URL generation complicated. Route names must be unique application wide.

Route names:

- Have no impact on URL matching or handling of requests.
- Are used only for URL generation.

___

### Attribute-Routed Action for REST APIs

REST APIs should use attribute routing to model the app's functionality as a set of resources where operations are represented by HTTP verbs.

Attribute routing uses a set of attributes to map actions directly to route templates. The following code is typical for a REST API


```csharp
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

var app = builder.Build();

app.UseHttpsRedirection();

app.UseAuthorization();

//=============================
app.MapControllers();
//=============================

app.Run();
```

In the preceding code, **MapControllers** is called to map attribute routed controllers.

In the following example:

**HomeController** matches a set of URLs similar to what the default conventional route **{controller=Home}/{action=Index}/{id?}** matches.

```csharp
public class HomeController : Controller
{
    [Route("")]
    [Route("Home")]
    [Route("Home/Index")]
    [Route("Home/Index/{id?}")]
    public IActionResult Index(int? id)
    {
        return ControllerContext.MyDisplayRouteInfo(id);
    }

    [Route("Home/About")]
    [Route("Home/About/{id?}")]
    public IActionResult About(int? id)
    {
        return ControllerContext.MyDisplayRouteInfo(id);
    }
}
```

The **HomeController.Index** action is run for any of the URL paths  

* /  
* /Home  
* /Home/Index  
* /Home/Index/3

This example highlights a key programming difference between attribute routing and conventional routing. Attribute routing requires more input to specify a route. The conventional default route handles routes more succinctly. However, attribute routing allows and requires precise control of which route templates apply to each action.

With attribute routing, the controller and action names play no part in which action is matched, unless token replacement is used. The following example matches the same URLs as the previous example:


```csharp
public class MyDemoController : Controller
{
    [Route("")]
    [Route("Home")]
    [Route("Home/Index")]
    [Route("Home/Index/{id?}")]
    public IActionResult MyIndex(int? id)
    {
        return ControllerContext.MyDisplayRouteInfo(id);
    }

    [Route("Home/About")]
    [Route("Home/About/{id?}")]
    public IActionResult MyAbout(int? id)
    {
        return ControllerContext.MyDisplayRouteInfo(id);
    }
}
```

The following code uses token replacement for action and controller:

```csharp
public class HomeController : Controller
{
    [Route("")]
    [Route("Home")]
    [Route("[controller]/[action]")]
    public IActionResult Index()
    {
        return ControllerContext.MyDisplayRouteInfo();
    }

    [Route("[controller]/[action]")]
    public IActionResult About()
    {
        return ControllerContext.MyDisplayRouteInfo();
    }
}
```

The following code applies **`[Route("[controller]/[action]")]`** to the controller:

```csharp
[Route("[controller]/[action]")]
public class HomeController : Controller
{
    [Route("~/")]
    [Route("/Home")]
    [Route("~/Home/Index")]
    public IActionResult Index()
    {
        return ControllerContext.MyDisplayRouteInfo();
    }

    public IActionResult About()
    {
        return ControllerContext.MyDisplayRouteInfo();
    }
}
```

#### Reserved routing names

The following keywords are reserved route parameter names when using Controllers or Razor Pages:

* action  
* area  
* controller  
* handler  
* page

Using **page** as a route parameter with attribute routing is a common error. Doing that results in inconsistent and confusing behavior with URL generation.

```csharp
public class MyDemo2Controller : Controller
{
    [Route("/articles/{page}")]
    public IActionResult ListArticles(int page)
    {
        return ControllerContext.MyDisplayRouteInfo(page);
    }
}
```

The special parameter names are used by the URL generation to determine if a URL generation operation refers to a Razor Page or to a Controller.

The following keywords are reserved in the context of a Razor view or a Razor Page. These keywords shouldn't be used for link generations, model bound parameters, or top level properties:

* page  
* using  
* namespace  
* inject  
* section  
* inherits  
* model  
* addTagHelper  
* removeTagHelper


___


# Controllers in ASP.NET MVC

The Controller in MVC architecture handles any incoming URL request. The **Controller** is a class, derived from the base class **System.Web.Mvc.Controller**. Controller class contains public methods called Action methods. Controller and its action method handles incoming browser requests, retrieves necessary model data and returns appropriate responses.

In ASP.NET MVC, every controller class name must end with a word "Controller". For example, the home page controller name must be **HomeController**, and for the student page, it must be the **StudentController**. Also, every controller class must be located in the **Controller** folder of the MVC folder structure.


## Adding a New Controller

In the Visual Studio, right click on the Controller folder -> select Add -> click on New Class

![data type](./Images/part2/3.png)

Then ASP.NET Core -> Controller Class -> Type in the controller name -> click Create. Remember, the controller name must end with Controller

![data type](./Images/part2/4.png)


This will create the **StudentController** class with the **Index()** method in **StudentController.cs** file under the **Controllers folder**, as shown below.

![data type](./Images/part2/5.png)


```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DemoProject.Controllers
{
    public class StudentController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
    }
}
```

As you can see above, the **StudentController class** is derived from the **Controller class**. Every controller in MVC must be derived from this abstract Controller class. This base Controller class contains helper methods that can be used for various purposes.

Now, we will return a dummy string from the Index action method of above the **StudentController**. Changing the return type of Index method from **ActionResult** to string and returning dummy string is shown below. You will learn about the ActionResult in the next section.

```csharp
public class StudentController : Controller
    {
        // GET: /<controller>/
        public string Index()
        {
            return "This is Index action method of StudentController";
        }
    }
```

We have already seen in the routing section that the URL request **http://localhost/student** or **http://localhost/student/index** is handled by the **Index()** method of the **StudentController class**, as shown above. So let's invoke it from the browser and you will see the following page in the browser.

![data type](./Images/part2/6.png)

## Action Method

In this section, you will learn about the action method of the controller class.

All the public methods of the **Controller** class are called **Action** methods. They are like any other normal methods with the following restrictions:

1. Action method must be public. It cannot be private or protected  
2. Action method cannot be overloaded  
3. Action method cannot be a static method.  

The following illustrates the **Index()** action method in the **StudentController** class.

![data type](./Images/part2/7.png)

As you can see in the above figure, the **Index()** method is public, and it returns the **ActionResult** using the **View()** method. The **View()** method is defined in the **Controller** base class, which returns the appropriate **ActionResult**.

## Action Result

MVC framework includes various **Result** classes, which can be returned from an action method. The result classes represent different types of responses, such as HTML, file, string, JSON, javascript, etc. The following table lists all the result classes available in ASP.NET MVC.

![data type](./Images/part2/8.png)

The **ActionResult** class is a base class of all the above result classes, so it can be the return type of action method that returns any result listed above. However, you can specify the appropriate result class as a return type of action method.

The **Index()** method of the **StudentController** in the above figure uses the **View()** method to return a **ViewResult** (which is derived from the **ActionResult** class). The base Controller class includes the **View()** method along with other methods that return a particular type of result, as shown in the below table.

![data type](./Images/part2/9.png)

As you can see in the above table, the **View()** method returns the **ViewResult**, the **Content()** method returns a string, the **File()** method returns the content of a file, and so on. Use different methods mentioned in the above table to return a different type of result from an action method.

## Action Method Parameters

Every action methods can have input parameters as normal methods. It can be primitive data type or complex type parameters, as shown below.

```csharp
[HttpPost]
public ActionResult Edit(Student std)
{
    // update student to the database
    
    return RedirectToAction("Index");
}

[HttpDelete]
public ActionResult Delete(int id)
{
    // delete student from the database whose id matches with specified id

    return RedirectToAction("Index");
}
```

Note that action method parameter can be Nullable Type. 

Model binding in ASP.NET MVC automatically maps the URL query string or form data collection to the action method parameters if both names match. 


## Action Selectors

Action selector is the attribute that can be applied to the action methods. It helps the routing engine to select the correct action method to handle a particular request. MVC 5 includes the following action selector attributes:

1. ActionName  
2. NonAction  
3. ActionVerbs

### ActionName

The **ActionName** attribute allows us to specify a different action name than the method name, as shown below.

```csharp
public class StudentController : Controller
{
    public StudentController()
    {
    }
       
    [ActionName("Find")]
    public ActionResult GetById(int id)
    {
        // get student from the database 
        return View();
    }
}
```

In the above example, we have applied **ActioName("find")** attribute to the **GetById()** action method. So now, the action method name is **Find** instead of the **GetById**. So now, it will be invoked on http://localhost/student/find/1 request instead of http://localhost/student/getbyid/1 request.

### NonAction

Use the **NonAction** attribute when you want public method in a controller but do not want to treat it as an action method.

In the following example, the **Index()** method is an action method, but the **GetStudent()** is not an action method.

```csharp
public class StudentController : Controller
{
    public string Index()
    {
            return "This is Index action method of StudentController";
    }
   
    [NonAction]
    public Student GetStudent(int id)
    {
        return studentList.Where(s => s.StudentId == id).FirstOrDefault();
    }
}
```

### ActionVerbs: HttpGet, HttpPost, HttpPut

The **ActionVerbs** selector is to handle different type of Http requests. The MVC framework includes HttpGet, HttpPost, HttpPut, HttpDelete, HttpOptions, and HttpPatch action verbs. You can apply one or more action verbs to an action method to handle different HTTP requests. If you don't apply any action verbs to an action method, then it will handle HttpGet request by default.

The following table lists the usage of HTTP methods:

![data type](./Images/part2/10.png)

The following example shows how to handle different types of HTTP requests in the **Controller** using ActionVerbs:

```csharp
public class StudentController : Controller
{
    public ActionResult Index() // handles GET requests by default
    {
        return View();
    }

    [HttpPost]
    public ActionResult PostAction() // handles POST requests by default
    {
        return View("Index");
    }


    [HttpPut]
    public ActionResult PutAction() // handles PUT requests by default
    {
        return View("Index");
    }

    [HttpDelete]
    public ActionResult DeleteAction() // handles DELETE requests by default
    {
        return View("Index");
    }

    [HttpHead]
    public ActionResult HeadAction() // handles HEAD requests by default
    {
        return View("Index");
    }
       
    [HttpOptions]
    public ActionResult OptionsAction() // handles OPTION requests by default
    {
        return View("Index");
    }
       
    [HttpPatch]
    public ActionResult PatchAction() // handles PATCH requests by default
    {
        return View("Index");
    }
}
```

You can also apply multiple action verbs using the **AcceptVerbs** attribute, as shown below.

```csharp
[AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
public ActionResult GetAndPostAction()
{
    return RedirectToAction("Index");
}
```


Learn more about Controller action return types in ASP.NET Core web API: https://learn.microsoft.com/en-us/aspnet/core/web-api/action-return-types?view=aspnetcore-3.1

<br/>

# Quiz
------
## Question
1. It defines the URL pattern and handler information.
    
    a.  View
    
    b.  Model
    
    c.  Controller
    
    d.  Routes

## Solution

    d. Routes

## Where discussed

[Routes defines the URL pattern and handler information](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L32)

<br/>

## Question

2.  It is used to create a single route. 
    
    a.  Map
    
    b.  MapControllerRoute
    
    c.  RouteCreate
    
    d.  default

## Solution

    b.  MapControllerRoute

## Where discussed

[MapControllerRoute is used to create a single route](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L80)

<br/>

## Question
3. Controllers use the Routing middleware to match the URLs of incoming requests and map them to _________.

    a.  Actions

    b.  URL
    
    c.  View
    
    d.  Model

## Solution

    a.  Actions

## Where discussed

[Request will be mapped to Actions](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L40)

<br/>

## Question
4. 	It is a class, derived from the base class System.Web.Mvc.Controller.

    a.  Methods
    
    b.  Web
    
    c.  Controller
    
    d.  Actions

## Solution

    c.  Controller

## Where discussed

[The **Controller** is a class, derived from the base class **System.Web.Mvc.Controller**](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L315)

<br/>

## Question
5. ______ selector handles different type of Http requests.

    a.  ActionName

    b.  ActionResult

    c.  Handler

    d.  ActionVerbs

## Solution

    c.  ActionVerbs

## Where discussed

[ActionVerbs is to handle different type of Http request](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L488)

<br/>

## True or False

##  Question

6. If action verb is not applied in an action method, then it will not handle any http request by default.

## Solution

**False**. HttpGet request is the default.

## Where discussed

[The default is HttpGet request](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L488)

<br/>

## Question

7.  We can apply multiple action verbs using the AcceptMethods attribute.

## Solution

**False**. It should be AcceptVerbs attribute.

## Where discussed
[AcceptVerbs attribute is used for apllying multiple action verbs](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L543)

<br/>

## Question

8.  http://localhost/product/edit/2 - in this URL, 2 is the ID(parameter).

## Solution

**True**, because url pattern is controller/action/id(parameter)

## Where discussed

[URL Pattern](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L99)

<br/>

## Question
9.  Give atleast 3 Result classes of Action Result

## Solution

- ViewResult
- EmptyResult
- ContentResult
- FileContentResult/ FilePathResult/ FileStreamResult
- javascriptResult
- JsonResult
- RedirectResult
- RedirectToRouteResult
- PartialViewResult
- HttpUnauthorizedResult

## Where discussed

[Result Classes](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L397)

<br/>

## Question

10. Give atleast 4 Http Method

## Solution

- GET
- POST
- PUT
- HEAD
- OPTIONS
- DELETE
- PATCH

## Where discussed

[Http Methods](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/blob/02_ASPNET_Core_MVC/02_ASPNET_Core_MVC/Instructors%20guide/README_part2.md?plain=1#L492)