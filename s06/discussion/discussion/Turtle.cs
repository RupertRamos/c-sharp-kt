﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    internal class Turtle : Reptile
    {
        private string name;
        private int age;

        public Turtle() { }

        public Turtle(string name, int age, string classification, string dietType, string habitat, bool hasScales)
        {
            this.name = name;
            this.age = age;
            Classification = classification;
            DietType = dietType;
            Habitat = habitat;
            HasScales = hasScales;
        }

        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }

        //Overloading - change the implementation by adding more parameters.
        public void Swim(string location)
        {
            Console.WriteLine($"This turtle is swimming in {location} using it's limbs and webbed feet.");
        }


        //Overriding - changes the implementation but not the amount of parameters/arguments.
        public void Eat()
        {
            Console.WriteLine("This turtle is eating seaweed.");
        }

    }
}
