﻿namespace activity
{
    class Activity
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter product name:");
            string name = Console.ReadLine();

            Console.WriteLine("Enter beginning inventory amount:");
            int beginningInventory = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter stock in amount:");
            int stockIn = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter stock out amount:");
            int stockOut = Convert.ToInt32(Console.ReadLine());

            int totalBalance = beginningInventory + stockIn - stockOut;
            Console.WriteLine($"The EOD total balance of {name} is:");
            Console.WriteLine(totalBalance);

            Console.WriteLine("Enter price per unit:");
            int pricePerUnit = Convert.ToInt32(Console.ReadLine());

            const double markupValue = 0.15;
            double incomePerUnit = pricePerUnit * markupValue;
            Console.WriteLine($"The total sales for today is {totalBalance * incomePerUnit}.");
        }
    }
}