// ======================================
// Simplifying Database Queries With LINQ
// ======================================

/*
Other References:
    Tuples in C#:
        https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/value-tuples

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

        // 1a. On the menu bar, choose File > New > Project.
        // 1b. Choose "C# Console App" from the templates.
        // 1c. Add the project name as "Discussion" and click on "Next".
        // 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Create a "Course.cs" file.
    discussion > Course.cs
*/
     
        using System;
        using System.Collections.Generic;
        using System.Linq;
        using System.Text;
        using System.Threading.Tasks;

        namespace discussion_linq
        {
            public class Course
            {
                public int Id { get; set; }
                public string Name { get; set; } = null!;
                public decimal Price { get; set; }
                public string Author { get; set; } = null!;

                public Course(int id, string name, decimal price, string author)
                {
                    Id = id;
                    Name = name;
                    Price = price;
                    Author = author;
                }

            }
        }

        /*
        Important Note:
            - To create a class in visual studio, do the following:
                - Right click on "discussion" in the "solution explorer".
                - Select "add" and then select "Class".
                - Select "Class" again from the C# items then finall on "Add".
            - Creating files is disabled while the application is running. You can stop the application by pressing the Red "Stop" button which only appears if the application is running. This will also close the browser where the application is shown for ease of use.
        */

/*
3. Create a "CourseDatabase.cs" file.
    discussion > CourseDatabase.cs
*/

        namespace discussion_linq
        {
            public class CourseDatabase
            {
                public static IEnumerable<Course> GetCoursesData()
                {
                    // Creates a list of courses for use with LINQ syntax to act as our database 
                    return new List<Course>
                    {
                        new Course(102, "HTML & CSS", 10000, "Asami Sato"),
                        new Course(117, "Javascript with Express.js", 12000, "Asami Sato"),
                        new Course(764, "Backend API with Node.js", 15000, "Mako Imawatsu"),
                        new Course(789, "Data Analysis with Python", 17000, "Lin Beifong"),
                        new Course(521, "PHP with Laravel Framework", 15000, "Mako Imawatsu"),
                        new Course(309, "Ruby and Rails Development", 12000, "Iknik Varrick"),
                        new Course(471, "Django with React.js", 20000, "Bolin Imawatsu")
                    };
                }
            }
        }

/*
4. Create a "Select.cs" file to demonstrate the use of "Select Operators".
    discussion > Select.cs
*/

        using System;
        using System.Collections.Generic;
        using System.Linq;
        using System.Text;
        using System.Threading.Tasks;

        namespace discussion_linq
        {
            public class Select
            {
                public void SelectOperator()
                {
                    var courses = CourseDatabase.GetCoursesData();

                    // The Select operator is used for selecting a particular property out of an object
                    var data = courses.Select(c => c.Author);

                    foreach (var item in data)
                    {
                        Console.WriteLine(item);
                    }

                }

            }
        }

/*
5. Invoke the "SelectOperator" method in the "Program.cs" file.
    discussion > Program.cs
*/

        namespace discussion_linq
        {
            class Program
            {
                static void Main(string[] args)
                {

                    // Creates an instance of the Select class
                    discussion_linq.Select select = new discussion_linq.Select();

                    select.SelectOperator();

                }
            }
        }

/*
6. Run the application to see the result of the code.
    Visual Studio
*/

        /*
        Important Note:
            - To run the application, a "green play button" can be found in visual studio which will execute the code added in the previous step.
            - Alternatively, pressing the "F5" button on the keyboard can be used to run the application.
            - A debug console will appear to print out the "Hello World" message.
            - If a project is opened via the "File > Open > Folder" option, running the application will automatically close the debug console.
            - To open a project, use the "File > Open > Project/Solution" option instead and select the corresponding solution file for the project indicated by the ".sln" extension.
            - A window pop up may appear asking to "Trust the ASP.NET Core SSL Certificate", since this is a locally created file, this should have no risk and we can select "yes" on all windows and trust the certificate.
            - The output in the console should be a list of courses. The console will not complete it's execution due to querying of data. In order to stop the terminal, press ctrl + c.
        */

/*
7. Modify the "Select.cs" file to use the Query Based Syntax.
    discussion > Select.cs
*/

        // ...

        namespace discussion_linq
        {
            public class Select
            {
                public void SelectOperator()
                {
                    // ...

                    // [Section] Method Based Syntax
                    // The Select operator is used for selecting a particular property out of an object
                    // var data = courses.Select(c => c.Author);

                    // [Section] Query Based Syntax
                    var data = from c in courses select c.Author;

                    foreach (var item in data)
                    {
                        // ...
                    }

                    // ...
                }

            }
        }


/*
8. Modify the "Select.cs" file again to create a method named "SelectNameAndPrice" and use the Method Based Syntax to retrieve the name and the price of the record.
    discussion > Select.cs
*/

        // ...

        namespace discussion_linq
        {
            public class Select
            {
                public void SelectOperator()
                {
                    // ...
                }

                public void SelectNameAndPrice()
                {
                    var courses = CourseDatabase.GetCoursesData();

                    var data = courses.Select(c => new
                    {
                        CourseName = c.Name,
                        CoursePrice = c.Price
                    });

                    foreach (var item in data)
                    {
                        Console.WriteLine($"{item.CourseName} - P{item.CoursePrice}");
                    }

                }

            }
        }

/*
9. Invoke the "SelectNameAndPrice" method in the "Program.cs" file.
    discussion > Program.cs
*/

        namespace discussion_linq
        {
            class Program
            {
                static void Main(string[] args)
                {
                    // ...

                    // select.SelectOperator();
                    select.SelectNameAndPrice();

                }
            }
        }

        /*
        Important Note:
            - Make sure to comment out the invocation of the "SelectOperator" method to view the output in the console.
        */

/*
10. Modify the "Select.cs" file to use the Query Based Syntax for the "SelectNameAndPrice" method.
    discussion > Select.cs
*/

        // ...

        namespace discussion_linq
        {
            public class Select
            {
                public void SelectOperator()
                {
                    // ...
                }

                public void SelectNameAndPrice()
                {
                    // ...

                    // Method Based Syntax
                    /*
                    var data = courses.Select(c => new
                    {
                        CourseName = c.Name,
                        CoursePrice = c.Price
                    });
                    */

                    // Query Based Syntax
                    var data = from c in courses
                    select new
                    {
                        CourseName = c.Name,
                        CoursePrice = c.Price
                    };

                    foreach (var item in data)
                    {
                        // ...
                    }

                    // ...
                }


            }
        }

/*
11. Modify the "Select.cs" file again to use the tuples for the "SelectNameAndPrice" method.
    discussion > Select.cs
*/

        // ...

        namespace discussion_linq
        {
            public class Select
            {
                public void SelectOperator()
                {
                    // ...
                }

                public void SelectNameAndPrice()
                {
                    // ...

                    // Method Based Syntax
                    /*
                    var data = courses.Select(c => new
                    {
                        CourseName = c.Name,
                        CoursePrice = c.Price
                    });
                    */

                    // Query Based Syntax
                    /*
                    var data = from c in courses
                    select new
                    {
                        CourseName = c.Name,
                        CoursePrice = c.Price
                    };
                    */

                    // Using tuples
                    // Tuples provide a concise syntax to group multiple data elements in a lightweight data structure
                    // We can work with the returned tuple instance directly or deconstruct it in separate variables as shown in the example below
                    var data = courses.Select(c => (CourseName: c.Name, CoursePrice: c.Price)).ToList();

                    foreach (var item in data)
                    {
                        // ...
                    }

                    // ...
                }


            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for Tuples in C#
        */

/*
12. Create a "Where.cs" file to demonstrate the use of "Where Operators".
    discussion > Where.cs
*/

        using System;
        using System.Collections.Generic;
        using System.Linq;
        using System.Text;
        using System.Threading.Tasks;

        namespace discussion_linq
        {
            internal class Where
            {
                public void WhereOperator()
                {
                    var courses = CourseDatabase.GetCoursesData();

                    // Method Based Syntax
                    // The where operator is used to filter records that meet a specific condition
                    var data = courses.Where(c => c.Price < 15000);

                    // Query Based Syntax
                    var data = from c in courses where c.Price < 14000 select c;

                    foreach (var item in data)
                    {
                        Console.WriteLine($"{item.Name} - P{item.Price}");
                    }

                }

            }
        }

        /*
        Important Note:
            - Since the students and trainees have been provided few examples of how to convert Method Based Syntax to Query Based Syntax, we can add both lines of code at the same time and then comment and uncomment them to be able to show the same output using the different approaches.
        */

/*
13. Invoke the "WhereOperator" method in the "Program.cs" file.
discussion > Program.cs
*/

    namespace discussion_linq
    {
        class Program
        {
            static void Main(string[] args)
            {
                // ...

                // select.SelectOperator();
                select.SelectNameAndPrice();

            }
        }
    }

/*
14. Create an "OrderByThenBy.cs" file to demonstrate the use of "OrderBy" and "ThenBy" operators.
    discussion > OrderByThenBy.cs
*/

        using System;
        using System.Collections.Generic;
        using System.Linq;
        using System.Text;
        using System.Threading.Tasks;

        namespace discussion_linq
        {
            public class OrderByThenBy
            {
                public void OrderByOperator()
                {
                    var courses = CourseDatabase.GetCoursesData();

                    // Method Based Syntax
                    var data = courses.OrderBy(c => c.Price);

                    // Query Based Syntax
                    // var data = from c in courses orderby c.Price select c;

                    foreach (var item in data)
                    {
                        Console.WriteLine($"{item.Name} - {item.Price} - {item.Author}");
                    }

                }

                public void ThenByOperator()
                {
                    var courses = CourseDatabase.GetCoursesData();

                    // Method Based Syntax
                    var data = courses.OrderBy(c => c.Price).ThenByDescending(a => a.Author);

                    // Query Based Syntax
                    // var data = from c in courses orderby c.Price, c.Author select c;

                    foreach (var item in data)
                    {
                        Console.WriteLine($"{item.Name} - {item.Price} - {item.Author}");
                    }

                }

            }
        }

/*
15. Invoke the "ThenBy" method in the "Program.cs" file.
discussion > Program.cs
*/

    namespace discussion_linq
    {
        class Program
        {
            static void Main(string[] args)
            {
                // ...
                discussion_linq.Where where = new discussion_linq.Where();
                // where.WhereOperator();

                // Creates an instance of the OrderByThenBy class
                discussion_linq.OrderByThenBy orderByThenBy = new discussion_linq.OrderByThenBy();
                orderByThenBy.ThenByOperator();

            }
        }
    }

/*
16. Create an "GroupBy.cs" file to demonstrate the use of "GroupBy Operators".
    discussion > GroupBy.cs
*/

        using System;
        using System.Collections.Generic;
        using System.Linq;
        using System.Text;
        using System.Threading.Tasks;

        namespace discussion_linq
        {
            public class GroupBy
            {
                public void GroupByOperator()
                {
                    var courses = CourseDatabase.GetCoursesData();

                    var data = courses.GroupBy(c => c.Author);

                    foreach (var item in data)
                    {
                        Console.WriteLine(item.Key);
                    }

                }

            }
        }

/*
17. Invoke the "GroupBy" method in the "Program.cs" file.
discussion > Program.cs
*/

    namespace discussion_linq
    {
        class Program
        {
            static void Main(string[] args)
            {
                // ...

                discussion_linq.OrderByThenBy orderByThenBy = new discussion_linq.OrderByThenBy();
                //orderByThenBy.ThenByOperator();

                // Creates an instance of the GroupBy class
                discussion_linq.GroupBy groupBy = new discussion_linq.GroupBy();
                groupBy.GroupByOperator();

            }
        }
    }

/*
18. Add a "NewListGroupByOperator" method file to demonstrate the use of "GroupBy Operators".
    discussion > GroupBy.cs
*/

        // ...

        namespace discussion_linq
        {
            public class GroupBy
            {
                public void GroupByOperator()
                {
                    // ...
                }

                public void NewListGroupByOperator()
                {
                    var courses = CourseDatabase.GetCoursesData();

                    // Method Based Syntax
                    var data = courses.GroupBy(c => c.Author).Select(g => new
                    {
                        Author = g.Key,
                        CourseCount = g.Count()
                    });

                    // Group Based Syntax
                    /*
                    var data = from c in courses group c by c.Author into g select new
                    {
                        Author = g.Key,
                        CourseCount = g.Count()
                    };
                    */

                    foreach (var item in data)
                    {
                        Console.WriteLine($"{item.Author} - {item.CourseCount}");
                    }

                }


            }
        }

/*
19. Invoke the "NewListGroupByOperator" method in the "Program.cs" file.
discussion > Program.cs
*/

    namespace discussion_linq
    {
        class Program
        {
            static void Main(string[] args)
            {
                // ...
                // groupBy.GroupByOperator();
                groupBy.NewListGroupByOperator();

            }
        }
    }

/*
20. Add a "MoreGroupBySample" method file to demonstrate the use of "GroupBy Operators".
    discussion > GroupBy.cs
*/

        // ...

        namespace discussion_linq
        {
            public class GroupBy
            {
                // ...

                public void NewListGroupByOperator()
                {
                    // ...
                }

                public void MoreGroupBySample()
                {
                    var courses = CourseDatabase.GetCoursesData();

                    var data = courses.Where(c => c.Price >= 15000).GroupBy(c => c.Author).Select(g => new
                    {
                        Author = g.Key,
                        Prices = g.Select(price => price.Price),
                        Courses = g.Select(course => course.Name)
                    }).OrderByDescending(a => a.Author);

                    foreach (var item in data)
                    {
                        Console.WriteLine(item.Author);

                        foreach (var course in item.Courses)
                        {
                            foreach (var price in item.Prices)
                            {
                                Console.WriteLine($"    {course} - {price}");
                                break;
                            }
                        }
                    }
                }

            }
        }

/*
21. Invoke the "MoreGroupBySample" method in the "Program.cs" file.
discussion > Program.cs
*/

    namespace discussion_linq
    {
        class Program
        {
            static void Main(string[] args)
            {
                // ...
                // groupBy.NewListGroupByOperator();
                groupBy.MoreGroupBySample();

            }
        }
    }

/*
========
Activity
========
*/

/*
1. Create a "CreateCourse.cshtml" view file.
    discussion > Views > Courses > CreateCourse.cshtml
*/

        @{
            ViewData["Title"] = "Create Course";
        }

        <h1>Create New Course</h1>
        <br />

        <table border="0" cellpadding="5" cellspacing="0" width="600">
            <tr>
                <td>
                    <label for="CourseTitle">Course Title:</label>
                </td>
                <td>
                    <input name="CourseTitle" type="text" maxlength="60" style="width:100%;max-width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Stack">Stack:</label>
                </td>
                <td>
                    <select name="Stack" type="text" maxlength="60" style="width:100%;max-width: 300px;">
                        <option value="empty"></option>
                        <option value="mern">MERN Stack</option>
                        <option value="mean">MEAN Stack</option>
                        <option value="django">Django Stack</option>
                        <option value="rails">Rails</option>
                        <option value="lamp">LAMP Stack</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Description">Description:</label>
                </td>
                <td>
                    <input name="Description" type="text" maxlength="43" style="width: 100%; max-width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Thumbnail">Thumbnail:</label>
                </td>
                <td>
                    <input name="Thumbnail" type="text" maxlength="43" style="width: 100%; max-width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Price">Price:</label>
                </td>
                <td>
                    <input name="Price" type="text" maxlength="43" style="width: 100%; max-width: 300px;" />
                </td>

        </table>
        <br />
        <button name="Create" type="submit" maxlength="43" style="width: 100%; max-width: 300px; margin-left:210px; background-color:gray">
            Create
        </button>

/*
2. Add a "CreateCourse" controller method.
    discussion > Controllers > CoursesController.cs
*/

        // ...

        namespace discussion.Controllers
        {
            public class CoursesController : Controller
            {
                public IActionResult Index()
                {
                    // ...
                }

                public IActionResult CreateCourse()
                {
                    return View();
                }

            }
        }

/*
3. Modify the "Courses" page to be able to properly navigate to the "CreateCourse" page.
    discussion > Views > Courses > Index.cshtml
*/

        // ...
        <body>

            // ...

            <div>
                <h2 style="display:inline-block">Courses available</h2>
                <button style="float:right; background-color:gray">
                    <a class="nav-link text-dark" asp-area="" asp-controller="Home" asp-action="Index">
                        Create New
                    </a>
                </button>


            </div>

            // ...

        </body>
        // ...