﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace discussion.Models
{
    public enum Stack
    {
        MEAN,
        MERN,
        Django,
        [Display(Name = "Ruby on Rails")] Rails,
        LAMP
    }

}
