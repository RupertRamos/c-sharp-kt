﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    internal class Pet
    {
        private string name;
        private string gender;
        private string classification;
        private int age;
        private string address;
        private string sound;

        public Pet() {}
        public Pet(string name, string gender, string classification, int age, string address, string sound)
        {
            this.name = name;
            this.gender = gender;
            this.classification = classification;
            this.age = age;
            this.address = address;
            this.sound = sound;
        }

        // Destructors and Finalizers
        ~Pet() 
        {
            Console.WriteLine($"{name} is being deleted");
        }

        public string Name { get => name; set => name = value; }
        public string Gender { get => gender; set => gender = value; }
        public string Classification { get => classification; set => classification = value; }
        public int Age { get => age; set => age = value; }
        public string Address { get => address; set => address = value; }
        public string Sound { get => sound; set => sound = value; }

        public void DescribePet()
        {
            Console.WriteLine($"{name} is a {gender} {classification} who is a {age} years of age and lives in {address}. It makes the sound {sound}");
        }

        public void MakeSound()
        {
            Console.WriteLine($"{name} says {sound}");
        }


    }
}
