﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    internal class Honda : Car
    {
        private int price;

        public Honda() { }

        public Honda(string make, string model, int price )
        {
            Make = make;
            Model = model;
            this.price = price;
        }

        public int Price { get => price; set => price = value; }



    }
}
