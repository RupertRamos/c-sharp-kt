# Session Objectives

At the end of the session, the students are expected to:

- Learn about JWT authentication and Entity Framework Core Identity.
- Apply JWT and Identity to our projects.
- Able to create endpoints using authentication and authorization in postman.

# Resources

## Instructional Materials 

- [GitLab Repository](https://gitlab.com/tuitt-tech/zuitt-curriculum-sandbox/vertere-aspnet-curriculum/-/tree/main/13_14_Authentication_Authorization/Discussion/AuthenticationServer/AuthenticationServer.API)
- [Google Slide Presentation (Part I)](https://docs.google.com/presentation/d/1-sAF3hJpDE6vLVKpYe3Xhj0qUKtw9z_e8iPAlVf2HXM/edit)
- [Google Slide Presentation (Part II](https://docs.google.com/presentation/d/1b1TWGJfYXSB8pKjNtH9vviOJl9JdL6e47dvPAxHBMss/edit)

## Supplemental Materials

- [JWT Auth and Identity](https://www.youtube.com/watch?v=vNaDR2fPLKU&list=PLA8ZIAm2I03hG7cAQC6xytRanKLbS7fTK&index=1)
- [Identity](https://stackoverflow.com/questions/55361533/addidentity-vs-addidentitycore)


# Session Proper

## What is JWT in .NET Core

JWT stands for JSON Web Tokens. It is an open standard method for securely transmitting information between parties as a JSON object (commonly known as Token). 

A JWT Web Token consists of 3 parts separated with 2 dots:

1. Header  
2. Payload  
3. Signature  

![data type](https://research.securitum.com/wp-content/uploads/sites/2/2019/10/jwt_ng1_en.png)

**Header** - the part of the token before the first dot. The Header consists of two parts, the first part contains the type of the token which is JWT. The second part contains the signing algorithm being used, such as HMAC SHA256 or RSA.

**Payload** - the part between the first and the second dot. The payload usually contains the claims (user attributes) and additional data like issuer, expiration time, and audience. 

**Signature** - the part after the 2nd dot. The signature is created by taking the encoded header, the encoded payload, a secret, the algorithm specified in the header, and then signing them. The signature is used to verify that the message is not changed along the path and the token is signed with a private key. Signature can also verify the identity of the sender.


## How is JWT implemented in .NET Core?

![data type](https://blog.miniorange.com/wp-content/uploads/sites/19/2021/12/jwt-workflow.webp)

1. Client authenticates itself with Username and Password to the Server.
2. On successful authentication, the Server creates JWT token and sends it to the client.
3. Client signs the Token with Private key, and sends it to the server in HTTP Authorization Header whenever it makes a request to secured resources.
4. Server receives a request, validates the JWT, and sends secured data to the client.

___


## JWT Authentication Server

   First thing, add these packages in your project all at once so that you will not add them during the session.
   ![packages](./Images/packages.png)

<br>

## User Registration

1. Create a new project empty project and name your solution **AuthenticationServer** and name your project **AuthenticationServer.API**

2. Create a Controllers folder inside it create a class and name it **AuthenticationController** and inherit from Controller.

```csharp
    using System;
    using Microsoft.AspNetCore.Mvc;

    namespace AuthenticationServer.API.Controllers
    {
        public class AuthenticationController : Controller
        {
            
        }
    }
```

3. Add to our program.cs file these line of codes. Adding our controllers to our services and mapping our controllers.

```csharp
    var builder = WebApplication.CreateBuilder(args);

    builder.Services.AddControllers();

    var app = builder.Build();

    app.UseRouting();

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });
    app.Run();
```

4. Now, create your Models folder. Inside it, create a new folder and name it **Request**. This will hold all the model of our different request later on. Create your first model request for **Register endpoint** and name it **RegisterRequest**. Below is the code.

```csharp
    using System;
    using System.ComponentModel.DataAnnotations;

    namespace AuthenticationServer.API.Models.Requests
    {
        public class RegisterRequest
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Required]
            public string Username { get; set; }

            [Required]
            public string Password { get; set; }

            [Required]
            public string ConfirmPassword { get; set; }
        }
    }
```

5. Once the user send a request, we will get the data from the body of that request with our Register endpoint.

   After we get the data from the body, we will validate those data annotations we set in the model request.

   **Additional info:** **ModelState** is a collection of name and value pairs that are submitted to the server during a POST. It also contains error messages about each name-value pair, if any are found. ModelState is a property of a Controller instance, and can be accessed from any class that inherits from Microsoft.

```csharp
    [HttpPost("register")]
    public async Task<IActionResult> Register([FromBody] RegisterRequest registerRequest)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest();
        }

        if (registerRequest.Password != registerRequest.ConfirmPassword)
        {
            return BadRequest();
        }
    }
```

6. Now we have to create our User Object. Under our Model folder, create a class and name it **User**. Below is the code.

```csharp
    using System;
    namespace AuthenticationServer.API.Models
    {
        public class User
        {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        }
    }
```

7. In our Register method under **AuthenticationController**, we also want to check if the email and username is not yet taken by another user so we could do that by creating a **UserRepository** file. This file  will contain our user data. 

   Create a folder and name it **Services** and inside that folder, create another folder and name it **UserRepositories**. Create an Interface file under that and name it **IUserRepository**.

   **Additional info:** *Interface* in C# is a blueprint of a class. It is like abstract class because all the methods which are declared inside the interface are abstract methods. It cannot have method body and cannot be instantiated. It is used to achieve multiple inheritance which can't be achieved by class.

```csharp
    public interface IUserRepository
        {
            // Both of these will return a user object from our User model.

            Task<User> GetByEmail(string email);

            Task<User> GetByUsername(string username);
        }
```

8. Back to our controller, we can now get our user repositories by calling them and put them inside our constructor method to use them. Below is the code.

```csharp
    private readonly IUserRepository _userRepository;

    public AuthenticationController(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }
```

9. Now, we can validate our email and username inside our Register method. Below is the code.

```csharp
    User existingUserByEmail = await _userRepository.GetByEmail(registerRequest.Email);
    if (existingUserByEmail != null)
    {
        return Conflict();
    }

    User existingUserByUsername = await _userRepository.GetByUsername(registerRequest.Username);
    if (existingUserByUsername != null)
    {
        return Conflict();
    }
```
10. To our **IUserRepository** we will add **Create** to create our user to be use in our Register method. below is the code.

```csharp
    Task<User> GetByEmail(string email);

    Task<User> GetByUsername(string username);

    // Added
    Task<User> Create(User user);
```
11. Let's create our password hasher first before we create our user. 

    Create these files Services > PasswordHashers > IPasswordHasher.cs. Below is the code.

```csharp
    using System;
    namespace AuthenticationServer.API.Services.PasswordHashers
    {
        public interface IPasswordHasher
        {
            string HashPassword(string password);
        }
    }
```

12. Add the newly created interface file inside our authenticationController constructor. Below is the updated code.

```csharp
    private readonly IUserRepository _userRepository;
    private readonly IPasswordHasher _passwordHasher;

    public AuthenticationController(IUserRepository userRepository, IPasswordHasher passwordHasher)
    {
        _userRepository = userRepository;
        _passwordHasher = passwordHasher;
    }
```

13. Inside our Register method, after all of our conditions, start creating user. Below is the code.

```csharp
    string passwordHash = _passwordHasher.HashPassword(registerRequest.Password);

    User registrationUser = new User()
    {
        Email = registerRequest.Email,
        Username = registerRequest.Username,
        PasswordHash = passwordHash
    };

    await _userRepository.Create(registrationUser);

    return Ok();
```

14. After these steps, we have to implement each interfaces, register their services on our **program.cs** and import a package that will hash our password.

15. First, import the latest version of **BCrypt.Net-Next** to your NuGet packages. This will do the hashing to our password. Then on your PasswordHasher folder, create a class and name it **BcryptPasswordHasher**. Then, implement your interface inside it. Below is the code.

```csharp
    using System;
    namespace AuthenticationServer.API.Services.PasswordHashers
    {
        public class BcryptPasswordHasher : IPasswordHasher
        {
            
            public string HashPassword(string password)
            {
                return BCrypt.Net.BCrypt.HashPassword(password);
            }
        }
    }
```

16. In our program.cs add our service for our password hasher as a singleton. Below is the code.

    **Additional info:** **Singleton** is a creational design pattern, which ensures that only one object of its kind exists and provides a single point of access to it for any other code. Singleton has almost the same pros and cons as global variables. Although they're super-handy, they break the modularity of your code.

```csharp
    builder.Services.AddSingleton<IPasswordHasher, BcryptPasswordHasher>();
```

17. For now, we have to put our user data in a file. Later on the session we will use Entity Framework so we can use database. Create a class inside your **UserRepositories** folder and name it **InMemoryUserRepositories.cs**. Implement your interface inside it. Below is the code.

    **Additional info:** **FirstOrDefault**. Returns the first element of a collection, or the first element that satisfies a condition. Returns a default value if index is out of range. First and FirstOrDefault has two overload methods. The first overload method doesn't take any input parameter and returns the first element in the collection.

```csharp
    using System;
    using AuthenticationServer.API.Models;

    namespace AuthenticationServer.API.Services.UserRepositories
    {
        public class InMemoryUserRepository : IUserRepository
        {
            // Store our user in a list
            private readonly List<User> _users = new List<User>();

            public Task<User> Create(User user)
            {
                // when adding a user, this method gives them a unique Id.
                user.Id = Guid.NewGuid();

                // add a new user to our list.
                _users.Add(user);

                // Return user from the result.
                return Task.FromResult(user);
            }

            public Task<User> GetByEmail(string email)
            {
                // This checks whether we have that email in our user's list and when not found it will return null.
                return Task.FromResult(_users.FirstOrDefault(u => u.Email == email));
            }

            public Task<User> GetByUsername(string username)
            {
                return Task.FromResult(_users.FirstOrDefault(u => u.Username == username ));
            }
        }
    }
```

18. Add the newly created service on our program.cs.

```csharp
    builder.Services.AddSingleton<IUserRepository, InMemoryUserRepository>();
```

19. Run your server and try the endpoint in our postman. You should get **200 OK** result. Once you register again, you will get **409 Conflict** result, meaning our validation works.

    ![register](./Images/register.png)

20. Let's create a **Response Folder** inside our Models folders so that we can add responses for each result later.

    Below is the code.

```csharp
    using System;
    namespace AuthenticationServer.API.Models.Responses
    {
        public class ErrorResponse
        {
            public IEnumerable<string> ErrorMessages { get; set; }

            // Create two constructor
            // This will hold a single error message.
            public ErrorResponse(string errorMessage) : this(new List<string>() { errorMessage }) { }

            // This will take a list of errormessages.
            public ErrorResponse(IEnumerable<string> errorMessages)
            {
                ErrorMessages = errorMessages;
            }
        }
    }
```

21. Apply those errorMessages insisde your Register method under your validations. Below is the code.
 
    Try running your code again and test it on postman. You should see error messages when a validation is hit.


```csharp
    if (!ModelState.IsValid)
        {
            // In here we will map through error messages in modelstate values.
            IEnumerable<string> errorMessages = ModelState.Values.SelectMany(v => v.Errors.Select(e => e.ErrorMessage));
            return BadRequest(errorMessages);
        }

        if (registerRequest.Password != registerRequest.ConfirmPassword)
        {
            return BadRequest(new ErrorResponse("Password doesn't match confirm password."));
        }

        User existingUserByEmail = await _userRepository.GetByEmail(registerRequest.Email);
        if (existingUserByEmail != null)
        {
            return Conflict(new ErrorResponse("Email already exist."));
        }

        User existingUserByUsername = await _userRepository.GetByUsername(registerRequest.Username);
        if (existingUserByUsername != null)
        {
            return Conflict(new ErrorResponse("Username already exist."));
        }
```


## User Login

1. Create a class and name it **LoginRequest** inside our Requests folder. Below is the code.

```csharp
    using System;
    using System.ComponentModel.DataAnnotations;

    namespace AuthenticationServer.API.Models.Requests
    {
        public class LoginRequest
        {
            [Required]
            public string Username { get; set; }

            [Required]
            public string Password { get; set; }
        }
    }
```

2. Add a Login method/route/endpoint inside our AuthenticationController. Below is the code.

```csharp
    [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
    {

    }
```

3. Inside our IPasswordHasher interface, create a method and name it VerifyPassword to verify our password and implement it in our BcryptPasswordHasher class. Below is the code.

```csharp
    using System;
    namespace AuthenticationServer.API.Services.PasswordHashers
    {
        public interface IPasswordHasher
        {
            string HashPassword(string password);

            bool VerifyPassword(string password, string passwordHash);
        }
    }
```

```csharp
    using System;
    namespace AuthenticationServer.API.Services.PasswordHashers
    {
        public class BcryptPasswordHasher : IPasswordHasher
        {
            
            public string HashPassword(string password)
            {
                return BCrypt.Net.BCrypt.HashPassword(password);
            }

            public bool VerifyPassword(string password, string passwordHash)
            {
                return BCrypt.Net.BCrypt.Verify(password, passwordHash);
            }
        }
    }
```

4. Since we are going to use the similar validation for our login endpoint, we can extract the ModelState validation into a method and name it **BadRequestModelState**. Then, add these validations inside our Login method.

```csharp
    if (!ModelState.IsValid)
        {
            return BadRequestModelState();
        }

        // This will get the user data and check if the user exist in our list.
        User user = await _userRepository.GetByUsername(loginRequest.Username);
        if (user == null)
        {
            return Unauthorized();
        }

        // This will verify the user password from the login request to the user data hashed password.
        bool isCorrectPassword = _passwordHasher.VerifyPassword(loginRequest.Password, user.PasswordHash);
        if (!isCorrectPassword)
        {
            return Unauthorized(); 
        }
```

5. Then, we have to generate our token for the user with the use of JWT. First, add another service folder and name it, **TokenGenerators**. Inside it, create a class and name it **AccessTokenGenerator**

   **Additonal info:** **Claims** in a JWT are encoded as a JSON object that is used as the payload of a JSON Web Signature (JWS) structure or as the plain-text of a JSON Web Encryption (JWE) structure. JWT claims can be digitally signed or integrity protected with a Message Authentication Code (MAC) and they can also be encrypted.

```csharp
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using AuthenticationServer.API.Models;
    using Microsoft.IdentityModel.Tokens;

    namespace AuthenticationServer.API.Services.TokenGenerators
    {
        public class AccessTokenGenerator
        {
            // This weill generate our token.
            public string GenerateToken(User user)
            {
                // Inside our jwt security constructor it will need paramaters.
                // 1. Issuer - the one that is issuing the jwt token
                // 2. Audience - is the one that will accept the jwt token.
                // In this case it's the same port. 
                // Later we will put them into a variable that can be access throughout our project.
                // 3. Claims - will contain the user information that is going to login.
                // 4. DateTime - this will tell 'til when our jwt is valid.
                // 5. Signin credentials - this will have the user credentials info.

                // Symmetric means that the key that we use to sign our 
                // jwt is the same key to be use once we verify our jwt.
                // We will generate our key using this site https://mkjwk.org/
                SecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("-ByJ2qPHfWbMjQa_YvhuyDnPNV-mYv-kA76lzt7X-ZP87INL7jBlQz0rNxj5AUyOwy2JOzusM_do26Bu4Zgy5nxCCTtO25ENrjVbHzJvuQxnix7Th3_MJbjvhXkkkFYhh3ePL1GCFKUiqlFkJ3Bhlzj8ojijZa-RE31f9HffYgE"));
                SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256); 

                // In here we are creating a list of claims
                List<Claim> claims = new List<Claim>()
                {
                    new Claim("id", user.Id.ToString()),

                    // Claimtypes is given in Security.Claims library
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, user.Username)

                };

                JwtSecurityToken token = new JwtSecurityToken(
                    "https://localhost:7012",
                    "https://localhost:7012",
                    claims,
                    DateTime.UtcNow,
                    DateTime.UtcNow.AddMinutes(30),
                    credentials
                    );
                // Generate our token
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
        }
    }
```


Generate our key:
![key](./Images/key.png)

<br>

6. After that, let's now add it on our AuthenticationController and add it also to our services inside program.cs file. Below is the code.

```csharp
    private readonly IUserRepository _userRepository;
    private readonly IPasswordHasher _passwordHasher;
    private readonly AccessTokenGenerator _accessTokenGenerator;

    public AuthenticationController(IUserRepository userRepository, IPasswordHasher passwordHasher,
        AccessTokenGenerator accessTokenGenerator)
    {
        _userRepository = userRepository;
        _passwordHasher = passwordHasher;
        _accessTokenGenerator = accessTokenGenerator;
    }
```

```csharp
    builder.Services.AddSingleton<AccessTokenGenerator>();
```

7.  Later on the session we will return and object containing our accessToken and refreshToken so we can put them in an object as a response. So inside your Response folder create a class and name it **AuthenticatedUserResponse**. Below is the code.

```csharp
    using System;
    namespace AuthenticationServer.API.Models.Responses
    {
        public class AuthenticatedUserResponse
        {
            public string AccessToken { get; set; }
        }
    }
```

8.  Let's now finish our Login method. We can now add our token.

```csharp
    // Adding our token
    string accessToken = _accessTokenGenerator.GenerateToken(user);
    return Ok(new AuthenticatedUserResponse()
    {
        AccessToken = accessToken
    });
```

9. Run your project and try login endpoint in postman.

   ![login](./Images/login.png)

<br>

10. Check your accessToken in this site https://jwt.io/ and you should see your claims info. Copy and paste your generated token in the site also.

    ![paylaod](./Images/payload.png)

<br>

11. After that, we will now minimize our code by transfering some codes inside our **appsettings.json**.
    
    Go to your appsettings.json and add this code below.

```csharp
    "AllowedHosts": "*",
    "Authentication": {
        "AccessTokenSecret": "-ByJ2qPHfWbMjQa_YvhuyDnPNV-mYv-kA76lzt7X-ZP87INL7jBlQz0rNxj5AUyOwy2JOzusM_do26Bu4Zgy5nxCCTtO25ENrjVbHzJvuQxnix7Th3_MJbjvhXkkkFYhh3ePL1GCFKUiqlFkJ3Bhlzj8ojijZa-RE31f9HffYgE",
        "AccessTokenExpirationMinutes": "30",
        "Issuer": "https://localhost:7012",
        "Audience": "https://localhost:7012"
    }
```

12. So the things we moved to our appsettings.json must be bind in an object and pass that object to our AccessTokenGenerator class.
 
    First, inside Models folder, create a class and name it **AuthenticationConfiguration**. Below is the code.

```csharp
    using System;
    namespace AuthenticationServer.API.Models
    {
        public class AuthenticationConfiguration
        {
            public string AccessTokenSecret { get; set; }
            public int AccessTokenExpirationMinutes { get; set; }
            public string Issuer { get; set; }
            public string Audience { get; set; }
        }
    }
```

13. After that, go to program.cs and we will now bind the values from our appsettings.json to our AuthenticationConfiguration model/class. And then, register it to our services. Below is the code.

```csharp
    AuthenticationConfiguration authenticationConfiguration = new AuthenticationConfiguration();
    // The parameter "Authentication" here should be the same spelling in the appsettings.json
    builder.Configuration.Bind("Authentication", authenticationConfiguration);
    builder.Services.AddSingleton(authenticationConfiguration);
```

14. Now, it's time to inject it the object to our AccessTokenGenerator class. Below is the code.

```csharp
    private readonly AuthenticationConfiguration _configuration;

    public AccessTokenGenerator(AuthenticationConfiguration authenticationConfiguration)
    {
        _configuration = authenticationConfiguration;
    }
```

15. We can now use that in replace of our JwtSecurityToken constructor parameter. Below is the new code.

```csharp
    ...
    SecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.AccessTokenSecret));
    ...
    JwtSecurityToken token = new JwtSecurityToken(
        _configuration.Issuer,
        _configuration.Audience,
        claims,
        DateTime.UtcNow,
        DateTime.UtcNow.AddMinutes(_configuration.AccessTokenExpirationMinutes),
        credentials
        );
```

## Use Access Token

1. Create a new API project inside your solution and name it **DataServer.API**. Remove the weatherforecast files and create a controller and name it **HomeController**. Below is the code inside HomeController.

```csharp
    using System;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    namespace DataServer.API.Controllers
    {
        public class HomeController : Controller
        {
            [Authorize]
            [HttpGet("data")]
            public IActionResult Index()
            {
                return Ok();
            }
        }
    }
```

2. Copy your **Authentication key** from your AuthenticationServer.API's appsettings.json to DataServer.API's appsettings.json. Remove the **AccessTokenExpirationMinutes** attribute from it. Below is the code.

```csharp
    "Authentication": {
        "AccessTokenSecret": "-ByJ2qPHfWbMjQa_YvhuyDnPNV-mYv-kA76lzt7X-ZP87INL7jBlQz0rNxj5AUyOwy2JOzusM_do26Bu4Zgy5nxCCTtO25ENrjVbHzJvuQxnix7Th3_MJbjvhXkkkFYhh3ePL1GCFKUiqlFkJ3Bhlzj8ojijZa-RE31f9HffYgE",
        "Issuer": "https://localhost:7012",
        "Audience": "https://localhost:7012"
    }
```

3. Then, copy the code inside your **AuthenticationConfiguration** file and paste it at the bottom part of your DataServer.API's program.cs, also remove **AccessTokenExpirationMinutes** attribute from it. Below is the code.

```csharp
    public class AuthenticationConfiguration
    {
        public string AccessTokenSecret { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
    }
```
4. Since we want to use our Authentication Server, in this API we will add authentication.
   
   Add **Microsoft.AspNetCore.Authentication.JwtBearer** package. Then, go to program.cs and add the Authentication service. Below is the code.

```csharp
    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(o =>
    {

        o.TokenValidationParameters = new TokenValidationParameters()
        {
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes())
        };
    });
```

5. Since we will be applying authentication in our new API, we will need to bind the values from authentication key again and add it to our program.cs file. Copy and paste the binding code from your AuthenticationServer.API's program.cs to this API project. Below is the code.

```csharp
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
```
```
    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(o =>
    {
        // Added
        AuthenticationConfiguration authenticationConfiguration = new AuthenticationConfiguration();
        builder.Configuration.Bind("Authentication", authenticationConfiguration);

        o.TokenValidationParameters = new TokenValidationParameters()
        {
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes())
        };
    });
```

6. Add the Authentication middleware inside your program.cs between routing and authorization. Below is the code.

```csharp
    app.UseRouting();
    app.UseAuthentication(); 
    app.UseAuthorization();
```

7. After that, place your authentication attributes in the authentication service. Below is the complete code for our service.

```csharp
    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(o =>
    {
        AuthenticationConfiguration authenticationConfiguration = new AuthenticationConfiguration();
        builder.Configuration.Bind("Authentication", authenticationConfiguration);

        o.TokenValidationParameters = new TokenValidationParameters()
        {
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authenticationConfiguration.AccessTokenSecret)),
            ValidIssuer = authenticationConfiguration.Issuer,
            ValidAudience = authenticationConfiguration.Audience,
            ValidateIssuerSigningKey = true,
            ValidateIssuer = true,
            ValidateAudience = true,
            // By default tokens are expires every 5 mins but by setting this line of code it will be based on the time given to the token.

        };
    });
```

8. Go back to your HomeController and put a breaskpoint in the return statement then run your projects. When doing that, since we are running two projects at the same time, Right click on your solution then select Set startup projects. A window will appear and click **Create Run Configuration** then select both project then click **Ok**.

   ![running2](./Images/running2.png)

<br>

   ![running2.1](./Images/running2.1.png)

<br>

9. On your postman, Try to register and login. Then, create a new http get request and put the url address for **"data"** on its header, add Authorization and give it a value of **Bearer** **(accesstoken) from login**. You should be redirected to your return breakpoint.

   ![postman_data](./Images/postman_data.png)

<br>

10. Now, we will get the claims data. To do that, copy and paste the code below.

```csharp
    [Authorize]
    [HttpGet("data")]
    public IActionResult Index()
    {
        string id = HttpContext.User.FindFirstValue("id");
        string email = HttpContext.User.FindFirstValue(ClaimTypes.Email);
        string username = HttpContext.User.FindFirstValue(ClaimTypes.Name);
        return Ok();
    }
```

11. You should be seeing this to your **Locals** that means you are able to get those values and use them.

    ![running2.2](./Images/running2.2.png)

<br>

## User RefreshToken

A refresh token is a special token that is used to obtain additional access tokens. This allows you to have short-lived access tokens without having to collect credentials every time one expires.

1. First is we have to generate a refresh token for our user once they login, so, back to our **AuthenticationServer.API** go to your **AuthenticatedUserResponse** and add a new property for our refresh token. Below is the code.

```csharp
    public class AuthenticatedUserResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
```

2. In our refresh token, we will not be using the generator token since we will not be needing to signin claims and we will be suing another secret key for a more security (if the secret key is stolen we are secure since we have different token) and a much longer expiration time.

3. Create a class under TokenGenerators' folder and name it **TokenGenerator**. Copy the code below for that class. We migrate these block of codes from the AccessTokenGenerator file since we want it inside a class that to be called by its name only.

```csharp
public string GenerateToken(string secretKey, string issuer, string audience, int expirationMinutes,
            IEnumerable<Claim> claims = null)
        {
            // Inside our jwt security constructor it will need paramaters.
            // Issuer - the one that is issuing the jwt token
            // Audience - is the one that will accept the jwt token.
            // In this case it's the same port. Later we will put them into a variable that can be access throughout our project.
            
            // DateTime - this will tell 'til when our jwt is valid.
            // Signin credentials - this will have the user credentials info.

            // Symmetric means that the key that we use to sign our jwt is the same key to be use once we verify our jwt.
            // We will generate our key using this site https://mkjwk.org/
            SecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
            SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);


            JwtSecurityToken token = new JwtSecurityToken(
                issuer,
                audience,
                claims,
                DateTime.UtcNow,
                DateTime.UtcNow.AddMinutes(expirationMinutes),
                credentials
                );
            // Generate our token
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
```

4. In your AccessTokenGenerator file, insert TokenGenerator class so it can be use in there. Add it to the constructor class and apply its use. Below is the full code for the updated AccessTokenGenerator file.

```csharp
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using AuthenticationServer.API.Models;
    using Microsoft.IdentityModel.Tokens;

    namespace AuthenticationServer.API.Services.TokenGenerators
    {
        public class AccessTokenGenerator
        {
            private readonly AuthenticationConfiguration _configuration;
            private readonly TokenGenerator _tokenGenerator;

            public AccessTokenGenerator(AuthenticationConfiguration configuration, TokenGenerator tokenGenerator)
            {
                _configuration = configuration;
                _tokenGenerator = tokenGenerator;
            }



            // This weill generate our token.
            public string GenerateToken(User user)
            {

                // Claims - will contain the user information that is going to login.
                // In here we are creating a list of claims
                List<Claim> claims = new List<Claim>()
                {
                    new Claim("id", user.Id.ToString()),

                    // Claimtypes is given in Security.Claims library
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, user.Username)

                };

                return _tokenGenerator.GenerateToken(
                    _configuration.AccessTokenSecret,
                    _configuration.Issuer,
                    _configuration.Audience,
                    _configuration.AccessTokenExpirationMinutes,
                    claims
                    );
            }
        }
    }
```

5. Create a class under TokenGenerators' folder and name it **RefreshTokenGenerator**.

6. Add our **TokenGenerator** and **RefreshTokenGenerator** to our services inside our program.cs.

```csharp
    builder.Services.AddSingleton<RefreshTokenGenerator>();
    builder.Services.AddSingleton<TokenGenerator>();
```

7. Back to our RefreshTokenGenerator, add the code below. It is similar to our AccessTokenGenerator but it has no claims only.

```csharp
    public class RefreshTokenGenerator
        {
            private readonly AuthenticationConfiguration _configuration;
            private readonly TokenGenerator _tokenGenerator;

        

            public RefreshTokenGenerator(AuthenticationConfiguration configuration, TokenGenerator tokenGenerator)
            {
                _configuration = configuration;
                _tokenGenerator = tokenGenerator;
            }

            public string GenerateToken()
            {
                return _tokenGenerator.GenerateToken(
                _configuration.RefreshTokenSecret,
                _configuration.Issuer,
                _configuration.Audience,
                _configuration.RefreshTokenExpirationMinutes);
            }
        }
```

8. You will see that RefreshTokenSecret and RefreshTokenExpirationMinutes is underlined with red that means we need to add them as property in our AuthenticationConfiguration file.
```csharp
    public string RefreshTokenSecret { get; set; } = null!;
    public int RefreshTokenExpirationMinutes { get; set; }
```

9. After that, go to your appsettings.json and the RefreshTokenSecret and RefreshTokenExpirationMinutes to our Authentication json. Below is the code.

```csharp
    "Authentication": {
        "AccessTokenSecret": "-ByJ2qPHfWbMjQa_YvhuyDnPNV-mYv-kA76lzt7X-ZP87INL7jBlQz0rNxj5AUyOwy2JOzusM_do26Bu4Zgy5nxCCTtO25ENrjVbHzJvuQxnix7Th3_MJbjvhXkkkFYhh3ePL1GCFKUiqlFkJ3Bhlzj8ojijZa-RE31f9HffYgE",
        "RefreshTokenSecret": "-AyJ2qPHfWbMjQa_YvhuyDnPNV-mYv-kA76lzt7X-ZP87INL7jBlQz0rNxj5AUyOwy2JOzusM_do26Bu4Zgy5nxCCTtO25ENrjVbHzJvuQxnix7Th3_MJbjvhXkkkFYhh3ePL1GCFKUiqlFkJ3Bhlzj8ojijZa-RE31f9HffYgE",
        "AccessTokenExpirationMinutes": "30",
        "RefreshTokenExpirationMinutes": "131400",
        "Issuer": "https://localhost:7012",
        "Audience": "https://localhost:7012"
    }
```

10. We set the minutes of our refresh token to  a much longer expiration time and we can also generate a new token for our RefreshToken but for this sample we just changed the second letter from **B** to **A**.

11. After all of these steps, we can now use our RefreshTokenGenerator to our Login method inside our AuthenticationController.

    Inject it to our constructor first. Below is the updated code.

```csharp
    private readonly IUserRepository _userRepository;
    private readonly IPasswordHasher _passwordHasher;
    private readonly AccessTokenGenerator _accessTokenGenerator;
    private readonly RefreshTokenGenerator _refreshTokenGenerator;

    public AuthenticationController(IUserRepository userRepository, IPasswordHasher passwordHasher,
        AccessTokenGenerator accessTokenGenerator, RefreshTokenGenerator refreshTokenGenerator)
    {
        _userRepository = userRepository;
        _passwordHasher = passwordHasher;
        _accessTokenGenerator = accessTokenGenerator;
        _refreshTokenGenerator = refreshTokenGenerator;
    }
```

12. Inside our Login method, copy the updated code below.

```csharp
    // Adding our tokens
    string accessToken = _accessTokenGenerator.GenerateToken(user);
    string refreshToken = _refreshTokenGenerator.GenerateToken();

    return Ok(new AuthenticatedUserResponse()
    {
        AccessToken = accessToken,
        RefreshToken = refreshToken
    });
```

13. Try running your projects and test it on postman. You should be able to get two token in the body.

    ![body](./Images/body.png)

<br>

14. You can try it out to jwt.io and you should be seeing this informations.

    ![login_jwtio](./Images/login_jwtio.png)

<br>

15. Now that we have our refresh token, how are we going to use that to get another token. For that one, we have to create a new route for that. To your AuthenticationController create a new method/route and name it **Refresh**. Below is the initial code.

```csharp
    [HttpPost("refresh")]
    public async Task<IActionResult> Refresh([FromBody] RefreshRequest refreshRequest)
    {

    }
```

16. Now, create that **RefreshRequest** class under Requests' folder. Below is the code.

```csharp
    using System;
    using System.ComponentModel.DataAnnotations;

    namespace AuthenticationServer.API.Models.Requests
    {
        public class RefreshRequest
        {
            [Required]
            public string RefreshToken { get; set; }
        }
    }
```
17. Since we have data annotations in our refresh token request, we should have a validation for our model state. Below is the updated code of our Refresh method.

```csharp
    [HttpPost("refresh")]
        public async Task<IActionResult> Refresh([FromBody] RefreshRequest refreshRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequestModelState();
            }
        }
```

18. After that, we have to make sure that our refresh token is not expired and we have to validate the signing key. To do that, create folder under Services and name it **TokenValidators**. Under that TokenValidators create a class and name it **RefreshTokenValidator**. Below is the full code of that file.

```csharp
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Text;
    using AuthenticationServer.API.Models;
    using Microsoft.IdentityModel.Tokens;

    namespace AuthenticationServer.API.Services.TokenValidators
    {
        public class RefreshTokenValidator
        {
            private readonly AuthenticationConfiguration _configuration;

            public RefreshTokenValidator(AuthenticationConfiguration configuration)
            {
                _configuration = configuration;
            }

            public bool Validate(string refreshToken)
            {
                // TokenValidationParamters allows us to configure our Jwt Authentication. So we will just copy and paste our
                // previous code in DataServer.
                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.RefreshTokenSecret)),
                    ValidIssuer = _configuration.Issuer,
                    ValidAudience = _configuration.Audience,
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ClockSkew = TimeSpan.Zero
                }; ;

                // It is designed for creating and validating Json Web Tokens
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

                try{

                    // This validates our token and will out/output our validated token. We just have to specify it here. 
                    tokenHandler.ValidateToken(refreshToken, validationParameters, out SecurityToken validatedToken);
                    return true;
                }
                catch
                {
                    return false; 
                }
            }
        }
    }
```

19. Add RefreshTokenValidator to our services. Below is the code.

```csharp
    builder.Services.AddSingleton<RefreshTokenValidator>();
```

20. We can now inject it to our AuthenticationController file. Below is the updated code of the constructor.

```csharp
    private readonly IUserRepository _userRepository;
    private readonly IPasswordHasher _passwordHasher;
    private readonly AccessTokenGenerator _accessTokenGenerator;
    private readonly RefreshTokenGenerator _refreshTokenGenerator;
    private readonly RefreshTokenValidator _refreshTokenValidator;

    public AuthenticationController(IUserRepository userRepository, IPasswordHasher passwordHasher,
        AccessTokenGenerator accessTokenGenerator, RefreshTokenGenerator refreshTokenGenerator,
        RefreshTokenValidator refreshTokenValidator)
    {
        _userRepository = userRepository;
        _passwordHasher = passwordHasher;
        _accessTokenGenerator = accessTokenGenerator;
        _refreshTokenGenerator = refreshTokenGenerator;
        _refreshTokenValidator = refreshTokenValidator;
    }
```

21. We now validate our refresh token inside our Refresh method. Below is the added code.

```csharp
    bool isValidRefreshToken = _refreshTokenValidator.Validate(refreshRequest.RefreshToken);
    if (!isValidRefreshToken)
    {
        return BadRequest(new ErrorResponse("Invalid refresh token."));
    }
```

22. Now that we have validated refresh token, the problem here is that we don't know who's the user is and how we can generate a refresh token for them. So what can we do to invalidate our refresh token without changing the refresh token secret key is that, we can store refresh tokens (like database) along  with the ID of the user that owns the token. So we if the refresh token is stolen we can just remove it from our database to invalidate it.

23. To do that, on our services create a new folder and name it **RefreshTokenRepositories** and under that create an interface because we will be implementing it with a database later and name it **IRefreshTokenRepository**. Below is the code.

```csharp
    using System;
    namespace AuthenticationServer.API.Services.RefreshTokenRepositories
    {
        public interface IRefreshTokenRepository
        {
            Task Create(RefreshToken refreshToken);
        }
    }
```

24. Create a class and name it RefreshToken inside our Models' folder. Below is the code.

```csharp
using System;
namespace AuthenticationServer.API.Models
{
    public class RefreshToken
    {
        // As a unique identifier
        public Guid Id { get; set; }

        // This two is what we need from our new refresh token
        public string Token { get; set; }

        public Guid UserId { get; set; }
    }
}
```

25. Update your parameter RefreshToken inside your IRefreshTokenRepository file.
```csharp
using AuthenticationServer.API.Models;
```


26. Create a class under RefreshTokenRepositories and name it **InMemoryRefreshTokenRepository**. Then implement your IRefreshTokenRepository interface in it. Below is the code.

```csharp
    using System;
    using AuthenticationServer.API.Models;

    namespace AuthenticationServer.API.Services.RefreshTokenRepositories
    {
        public class InMemoryRefreshTokenRepository : IRefreshTokenRepository
        {
            // This will hold our refresh token that we create.
            private readonly List<RefreshToken> _refreshTokens = new List<RefreshToken>();

            public Task Create(RefreshToken refreshToken)
            {
                // Giving our refresh token a unique identifier
                refreshToken.Id = Guid.NewGuid();

                // Adding our token to our list.
                _refreshTokens.Add(refreshToken);

                return Task.CompletedTask;

            }
        }
    }
```

27. Add IRefreshTokenRepository to our services inside program.cs. Below is the code.

```csharp
    builder.Services.AddSingleton<IRefreshTokenRepository, InMemoryRefreshTokenRepository>();
```

28. Inject IRefreshTokenRepository to our AuthenticationController's constructor so we can use it to our Refresh and Login method. Below is the updated code.

```csharp
    private readonly IUserRepository _userRepository;
    private readonly IPasswordHasher _passwordHasher;
    private readonly AccessTokenGenerator _accessTokenGenerator;
    private readonly RefreshTokenGenerator _refreshTokenGenerator;
    private readonly RefreshTokenValidator _refreshTokenValidator;
    private readonly IRefreshTokenRepository _refreshTokenRepository;

    public AuthenticationController(IUserRepository userRepository, IPasswordHasher passwordHasher,
        AccessTokenGenerator accessTokenGenerator, RefreshTokenGenerator refreshTokenGenerator,
        RefreshTokenValidator refreshTokenValidator, IRefreshTokenRepository refreshTokenRepository)
    {
        _userRepository = userRepository;
        _passwordHasher = passwordHasher;
        _accessTokenGenerator = accessTokenGenerator;
        _refreshTokenGenerator = refreshTokenGenerator;
        _refreshTokenValidator = refreshTokenValidator;
        _refreshTokenRepository = refreshTokenRepository;
    }
```

29. We can now use it to our Login method. Below is the updated code.

    **Additional info**: A **Data Transfer Object (DTO)** is an object that carries data between processes. You can use this technique to facilitate communication between two systems (like an API and your server) without potentially exposing sensitive information. 

```csharp
    // Adding our tokens
    string accessToken = _accessTokenGenerator.GenerateToken(user);
    string refreshToken = _refreshTokenGenerator.GenerateToken();

    RefreshToken refreshTokenDTO = new RefreshToken()
    {
        Token = refreshToken,
        UserId = user.Id
    };

    await _refreshTokenRepository.Create(refreshTokenDTO);

    return Ok(new AuthenticatedUserResponse()
    {
        AccessToken = accessToken,
        RefreshToken = refreshToken
    });
```

30. Now let's create a way to query that DTO to our refresh method. Go to our IRefreshTokenRepository and add the updated code below.

```csharp
    public interface IRefreshTokenRepository
    {
        Task<RefreshToken> GetByToken(string token);
        Task Create(RefreshToken refreshToken);
    }
```

31. Then implement that to our InMemoryRefreshTokenRepository. Update the method with this code.

```csharp
    public Task<RefreshToken> GetByToken(string token)
        {
            RefreshToken refreshToken = _refreshTokens.FirstOrDefault(r => r.Token == token);

            return Task.FromResult(refreshToken);
        }
```

32. Back to our Refresh method, add this block of codes to generate our refresh token.

```csharp

    RefreshToken refreshTokenDTO = await _refreshTokenRepository.GetByToken(refreshRequest.RefreshToken);
    if (refreshTokenDTO == null)
    {
        return NotFound(new ErrorResponse("Invalid refresh token."));
    }
    User user = await _userRepository.GetById(refreshTokenDTO.UserId);
```

33. Generate GetById method and implement it to our InMemoryUserRepository. Below is the code inside InMemoryUserRepository.

IUserRepository
```csharp
Task<User> GetById(Guid userId);
```

InMemoryUserRepository
```csharp
    public Task<User> GetById(Guid userId)
        {
            return Task.FromResult(_users.FirstOrDefault(u => u.Id == userId));
        }
```

34. Transform the below code to a class to re-use it again to our Refresh method.

    This block of codes inside Login method
```csharp
    // Adding our tokens
    string accessToken = _accessTokenGenerator.GenerateToken(user);
    string refreshToken = _refreshTokenGenerator.GenerateToken();

    RefreshToken refreshTokenDTO = new RefreshToken()
    {
        Token = refreshToken,
        UserId = user.Id
    };

    await _refreshTokenRepository.Create(refreshTokenDTO);

    return (new AuthenticatedUserResponse()
    {
        AccessToken = accessToken,
        RefreshToken = refreshToken
    });
```

35. Create a folder under services and name it **Authenticators**. Inside it, create a class and name it **Authenticator**. Below is the code for that class. 

```csharp
    using System;
    using AuthenticationServer.API.Models;
    using AuthenticationServer.API.Models.Responses;
    using AuthenticationServer.API.Services.RefreshTokenRepositories;
    using AuthenticationServer.API.Services.TokenGenerators;

    namespace AuthenticationServer.API.Services.Authenticators
    {
        public class Authenticator
        {
            private readonly AccessTokenGenerator _accessTokenGenerator;
            private readonly RefreshTokenGenerator _refreshTokenGenerator;
            private readonly IRefreshTokenRepository _refreshTokenRepository;

            public Authenticator(AccessTokenGenerator accessTokenGenerator, RefreshTokenGenerator refreshTokenGenerator,
                IRefreshTokenRepository refreshTokenRepository)
            {
                _accessTokenGenerator = accessTokenGenerator;
                _refreshTokenGenerator = refreshTokenGenerator;
                _refreshTokenRepository = refreshTokenRepository;
            }

            public async Task<AuthenticatedUserResponse> Authenticate(User user)
            {
                // Adding our tokens
                string accessToken = _accessTokenGenerator.GenerateToken(user);
                string refreshToken = _refreshTokenGenerator.GenerateToken();

                RefreshToken refreshTokenDTO = new RefreshToken()
                {
                    Token = refreshToken,
                    UserId = user.Id
                };

                await _refreshTokenRepository.Create(refreshTokenDTO);

                return (new AuthenticatedUserResponse()
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken
                });
            }
        }
    }
```

36. Add it to our services. Below is the code.

```csharp
    builder.Services.AddSingleton<Authenticator>();
```

37. Inject it to our AuthenticationController's constructor and we will remove AccessTokenGenerator and RefreshTokenGenerator. Below is the updated code.

```csharp
    private readonly IUserRepository _userRepository;
    private readonly IPasswordHasher _passwordHasher;
    private readonly RefreshTokenValidator _refreshTokenValidator;
    private readonly IRefreshTokenRepository _refreshTokenRepository;
    private readonly Authenticator _authenticator;

    public AuthenticationController(IUserRepository userRepository, IPasswordHasher passwordHasher,
        RefreshTokenValidator refreshTokenValidator, IRefreshTokenRepository refreshTokenRepository,
        Authenticator authenticator)
    {
        _userRepository = userRepository;
        _passwordHasher = passwordHasher;
        _refreshTokenValidator = refreshTokenValidator;
        _refreshTokenRepository = refreshTokenRepository;
        _authenticator = authenticator;
    }
```

38. To our Login method, add this in replacement of the removed block of codes awhile ago.

```csharp
    AuthenticatedUserResponse response = await _authenticator.Authenticate(user);
    return Ok(response);
```
39. After that, go back to our controller and validate our user with that refresh token and finally generate a new refresh token for them. Below is the added code.

```csharp
    User user = await _userRepository.GetById(refreshTokenDTO.UserId);
    if (user == null)
    {
        return NotFound(new ErrorResponse("User not found."));
    }
    AuthenticatedUserResponse response = await _authenticator.Authenticate(user);
    return Ok(response);
```

40. Try running your projects and test it on postman. From registering to logging in to test data from DataServer. Lastly, create another Http POST request and try the structure shown below.

    ![refresh](./Images/refresh.png)

41. Lastly, we have to delete our refresh token once it is used. Add this code below in your Refresh method before generating your refresh token.

```csharp
    await _refreshTokenRepository.Delete(refreshTokenDTO.Id);
```

42. Then, add the Delete method to your IRefreshTokenRepository class and implement it in our InMemoryRefreshTokenRepository file. Below is the code inside our InMemoryRefreshTokenRepository once implemented.

```csharp
    public Task Delete(Guid id)
        {
            _refreshTokens.RemoveAll(r => r.Id == id);
            return Task.CompletedTask;
        }
```

43. Run your projects and test it again in postman. In refresh endpoint you should be seeing "Invalid Refresh Token" once you resend the request. Meaning our method is working.

    ![resend](./Images/resend.png)

## User Logout

1. Create a new endpoint/route inside your AuthenticationController and copy the initial code below.

```csharp
    [Authorize]
    [HttpDelete("logout")]
    public async Task<IActionResult> Logout()
    {
        
        string rawUserId = HttpContext.User.FindFirstValue("id");

        // Since in here our id is in string we have to parse it to Guid.
        // Then if the parsing fails, it means that there some invalid Id.
        if(!Guid.TryParse(rawUserId, out Guid userId))
        {
            return Unauthorized();
        }
    }
```

2. We should invalidate our refresh tokens. When we take a look on our Refresh method we can see that if a refresh token in our _refreshTokenRepository it will return "Invalid refresh token". So in order for us to invalidate our refresh token, we only have to delete it from our data storage using our IRefreshTokenRepository.

3. Go to our RefreshTokenRepository and add a new method and add the code below.

```csharp
    Task DeleteAll(Guid userId);
```

4. Implement it to our InMemoryRefreshTokenRepository file. Below is the code.

```csharp
    public Task DeleteAll(Guid userId)
        {
            _refreshTokens.RemoveAll(r => r.UserId == userId);

            return Task.CompletedTask;
        }
```

5. Back to our AuthenticationController, inside our Logout method, we now use the newly created DeleteAll method to delete all refresh tokens and return NoContent. Below is the updated code.

```csharp
    [Authorize]
    [HttpDelete("logout")]
    public async Task<IActionResult> Logout()
    {
        
        string rawUserId = HttpContext.User.FindFirstValue("id");

        // Since in here our id is in string we have to parse it to Guid.
        // Then if the parsing fails, it means that there some invalid Id.
        if(!Guid.TryParse(rawUserId, out Guid userId))
        {
            return Unauthorized();
        }

        await _refreshTokenRepository.DeleteAll(userId);
        return NoContent();
    }
```

6. To your program.cs add this validation block of codes. 

```csharp
    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(o =>
    {
        
        o.TokenValidationParameters = new TokenValidationParameters()
        {
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authenticationConfiguration.AccessTokenSecret)),
            ValidIssuer = authenticationConfiguration.Issuer,
            ValidAudience = authenticationConfiguration.Audience,
            ValidateIssuerSigningKey = true,
            ValidateIssuer = true,
            ValidateAudience = true,
            ClockSkew = TimeSpan.Zero
        };
    });
```

7. Add this also at the bottom part after app.UseRouting().

```csharp
app.UseAuthentication();

app.UseAuthorization();
```

8. Run your projects and test it to your postman. Create a new Http Delete request. Add **Authorization** and **Bearer<space>accessToken**. From the refresh endpoint copy the accessToken and paste it to the header of the newly created Http request. And at the body of the Http request put username and password of the user who login. You shouldbe seeing No Content in the result, that means our logout method works. Below is the illustration.

   ![delete_head](./Images/delete_head.png)

   ![delete_body](./Images/delete_body.png)

<br>

9. Once you try to use your refresh token provided when you go to refresh endpoint to refresh endpoint too, it will now invalidate it. 

   ![delete_invalid](./Images/delete_invalid.png)


## Auhtentication with Entity Framework

1. Create a class under Models' folder and name it **AuthenticationDbContext**. Below is the code.

```csharp
    using System;
    using Microsoft.EntityFrameworkCore;

    namespace AuthenticationServer.API.Models
    {
        public class AuthenticationDbContext : DbContext
        {
            public AuthenticationDbContext(DbContextOptions<AuthenticationDbContext> options)
                : base(options)
            {

            }

            // In here we are mapping through our Databases table.

            public DbSet<User> Users { get; set; }

            public DbSet<RefreshToken> RefreshTokens { get; set; }
        }
    }
```

2. Add our connection string in our appsettings.json. Below is the code. In here I have prepared a database already. And we are going to use MySql.

```csharp
    "ConnectionStrings": {

        "DefaultConnection": "Server=localhost;Database=auth_jwt;User=root;Password=;"
    }
```

3. Add our DbContext to our program.cs and connect our database. Below is the code.

```csharp
    // Connection to our database
    string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
    builder.Services.AddDbContext<AuthenticationDbContext>(options =>
    {
        options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
    });
```

4. Then let's migrate here. Type in the terminal the code below.

    Original Command:
    (Not working)

            dotnet ef migrations add InitialCreate


    Alternative Command:

    4.a. Open Xampp and start MySQL and Apache.

    4.b. Type this alternative command:

            Add-Migrations InitialCreate

5. Then it will generate Migrations Folder.

6. Now we want our application when it starts to start with migration so that we will now updated our database anymore. To do that add the code below in our program.cs.

```csharp
    // Since our DbContext is a scoped meaning all our http request to our server will be coming from our DbContext.
    // That's why here we create a scoped service to use our AuthenticationDbContext.
    using (IServiceScope scope = app.Services.CreateScope())
    {
        // This lets us to use migration when the application starts so you don't have to update your database.
        using (AuthenticationDbContext context = scope.ServiceProvider.GetRequiredService<AuthenticationDbContext>())
        {
            context.Database.Migrate();
        }
    }
```

7. We will store our user in our database. To do that, create a class under UserRepositories and name it **DatabaseUserRepository**. Then implement IUserRepository in it. Below is the code.

```csharp
    using System;
    using AuthenticationServer.API.Models;
    using Microsoft.EntityFrameworkCore;

    namespace AuthenticationServer.API.Services.UserRepositories
    {
        public class DatabaseUserRepository : IUserRepository
        {
            // Inject DbContext in here
            private readonly AuthenticationDbContext _context;

            // Generate our constructor
            public DatabaseUserRepository(AuthenticationDbContext context)
            {
                _context = context;
            }

            public async Task<User> Create(User user)
            {
                _context.Users.Add(user);
                await _context.SaveChangesAsync();
                return user;
            }

            public async Task<User> GetByEmail(string email)
            {
                // We are going to find the first data of email here if none it will return null
                return await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
            }

            public async Task<User> GetById(Guid userId)
            {
                // In here we will find the primary key which is the userId that's why we sue FindAsync.
                return await _context.Users.FindAsync(userId);
            }

            public async Task<User> GetByUsername(string username)
            {
                // We are going to find the first data of username here if none it will return null
                return await _context.Users.FirstOrDefaultAsync(u => u.Username == username);
            }
        }
    }
```

8. Then in our program.cs change the **InMemoryUserRepository** into **DatabaseUserRepository** since that is the one we are going to use. Below is the code.

```csharp
    builder.Services.AddSingleton<IUserRepository, DatabaseUserRepository>();
```

9. Create a class under RefreshTokenRepositories and name it **DatabaseRefreshTokenRepository**. Implement the interface and inject the DbContext too. Create a constructor and update its methods with thew code below.

```csharp
    using System;
    using AuthenticationServer.API.Models;
    using Microsoft.EntityFrameworkCore;

    namespace AuthenticationServer.API.Services.RefreshTokenRepositories
    {
        public class DatabaseRefreshTokenRepository : IRefreshTokenRepository
        {
            private readonly AuthenticationDbContext _context;

            public DatabaseRefreshTokenRepository(AuthenticationDbContext context)
            {
                _context = context;
            }

            public async Task Create(RefreshToken refreshToken)
            {
                _context.RefreshTokens.Add(refreshToken);
                await _context.SaveChangesAsync();
                
            }

            public async Task Delete(Guid id)
            {
                // Query first then do remove.
                RefreshToken refreshToken = await _context.RefreshTokens.FindAsync(id);
                if (refreshToken != null)
                {
                    _context.RefreshTokens.Remove(refreshToken);
                    await _context.SaveChangesAsync();
                }
            }

            public async Task DeleteAll(Guid userId)
            {
                IEnumerable<RefreshToken> refreshTokens = await _context.RefreshTokens
                    .Where(t => t.UserId == userId)
                    .ToListAsync();

                _context.RefreshTokens.RemoveRange(refreshTokens);
                await _context.SaveChangesAsync();
            }

            public async Task<RefreshToken> GetByToken(string token)
            {
                return await _context.RefreshTokens.FirstOrDefaultAsync(t => t.Token == token);
            }
        }
    }
```

10. Then in our program.cs change the **InMemoryRefreshTokenRepository** into **DatabaseRefreshTokenRepository** since that is the one we are going to use. Below is the code.

```csharp
    builder.Services.AddSingleton<IRefreshTokenRepository, DatabaseRefreshTokenRepository>();
```
11. Now transform these service with codes.

```csharp
builder.Services.AddScoped<IUserRepository, DatabaseUserRepository>();
builder.Services.AddScoped<IRefreshTokenRepository, DatabaseRefreshTokenRepository>();
builder.Services.AddScoped<Authenticator>();
```

12. Run your porjects in postman and test each endpoints. They should be all working. To test if we succeed in our database implementation. Try re-running your projects and directly login. You should be logging in wihtout registration since we have our database.

## Authentication with Identity

**ASP.NET Core Identity** provides a framework for managing and storing user accounts in ASP.NET Core apps. Identity is added to your project when Individual User Accounts is selected as the authentication mechanism. By default, Identity makes use of an Entity Framework (EF) Core data model. The following are the steps on how we can implement Identity to our server.

1. Add this to our program.cs so that we can use Identity to our projects.

```csharp
    builder.Services.AddIdentityCore<User>(); 
```

2. Now let's clear our projects and apply Identity. Go to our AuthenticationController and apply the code below.

```csharp
    public class AuthenticationController : Controller
        {
            // Changed
            private readonly UserManager<User> _userRepository;

            private readonly RefreshTokenValidator _refreshTokenValidator;
            private readonly IRefreshTokenRepository _refreshTokenRepository;
            private readonly Authenticator _authenticator;

            public AuthenticationController(UserManager<User> userRepository,
                RefreshTokenValidator refreshTokenValidator, IRefreshTokenRepository refreshTokenRepository,
                Authenticator authenticator)
            {
                _userRepository = userRepository;
                _refreshTokenValidator = refreshTokenValidator;
                _refreshTokenRepository = refreshTokenRepository;
                _authenticator = authenticator;
            }

            [HttpPost("register")]
            public async Task<IActionResult> Register([FromBody] RegisterRequest registerRequest)
            {
                if (!ModelState.IsValid)
                {
                    // In here we will map through error messages in modelstate values.
                    return BadRequestModelState();
                }

                if (registerRequest.Password != registerRequest.ConfirmPassword)
                {
                    return BadRequest(new ErrorResponse("Password doesn't match confirm password."));
                }

                //User existingUserByEmail = await _userRepository.GetByEmail(registerRequest.Email);
                //if (existingUserByEmail != null)
                //{
                //    return Conflict(new ErrorResponse("Email already exist."));
                //}

                //User existingUserByUsername = await _userRepository.GetByUsername(registerRequest.Username);
                //if (existingUserByUsername != null)
                //{
                //    return Conflict(new ErrorResponse("Username already exist."));
                //}

                //string passwordHash = _passwordHasher.HashPassword(registerRequest.Password);

                User registrationUser = new User()
                {
                    Email = registerRequest.Email,
                    Username = registerRequest.Username
                    //PasswordHash = passwordHash
                };

                //await _userRepository.Create(registrationUser);

                // Changed
                // Our password is automatically hash.
                // It also do the validation
                IdentityResult result =  await _userRepository.CreateAsync(registrationUser, registerRequest.Password);
                if (!result.Succeeded)
                {
                    IdentityErrorDescriber errorDescriber = new IdentityErrorDescriber();
                    IdentityError primaryError = result.Errors.FirstOrDefault();

                    if (primaryError.Code == nameof(errorDescriber.DuplicateEmail))
                    {
                        return Conflict(new ErrorResponse("Email already exist."));
                    }
                    else if (primaryError.Code == nameof(errorDescriber.DuplicateUserName))
                    {
                        return Conflict(new ErrorResponse("Username already exist."));
                    }
                }
                

                return Ok();
            }


            [HttpPost("login")]
            public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequestModelState();
                }

                // This will get the user data and check if the user exist in our list.
                // User user = await _userRepository.GetByUsername(loginRequest.Username);

                // Changed
                User user = await _userRepository.FindByNameAsync(loginRequest.Username);
                if (user == null)
                {
                    return Unauthorized();
                }

                // This will verify the user password from the login request to the user data hashed password.
                //bool isCorrectPassword = _passwordHasher.VerifyPassword(loginRequest.Password, user.PasswordHash);

                // Changed
                bool isCorrectPassword = await _userRepository.CheckPasswordAsync(user, loginRequest.Password);
                if (!isCorrectPassword)
                {
                    return Unauthorized(); 
                }

                AuthenticatedUserResponse response = await _authenticator.Authenticate(user);
                return Ok(response);
            }

            [HttpPost("refresh")]
            public async Task<IActionResult> Refresh([FromBody] RefreshRequest refreshRequest)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequestModelState();
                }

                bool isValidRefreshToken = _refreshTokenValidator.Validate(refreshRequest.RefreshToken);
                if (!isValidRefreshToken)
                {
                    return BadRequest(new ErrorResponse("Invalid refresh token."));
                }

                RefreshToken refreshTokenDTO = await _refreshTokenRepository.GetByToken(refreshRequest.RefreshToken);
                if (refreshTokenDTO == null)
                {
                    return NotFound(new ErrorResponse("Invalid refresh token."));
                }

                await _refreshTokenRepository.Delete(refreshTokenDTO.Id);

                // User user = await _userRepository.GetById(refreshTokenDTO.UserId);

                // Changed
                User user = await _userRepository.FindByIdAsync(refreshTokenDTO.UserId.ToString());
                if (user == null)
                {
                    return NotFound(new ErrorResponse("User not found."));
                }
                AuthenticatedUserResponse response = await _authenticator.Authenticate(user);
                return Ok(response);
            }

            [Authorize]
            [HttpDelete("logout")]
            public async Task<IActionResult> Logout()
            {
                
                string rawUserId = HttpContext.User.FindFirstValue("id");

                // Since in here our id is in string we have to parse it to Guid.
                // Then if the parsing fails, it means that there some invalid Id.
                if(!Guid.TryParse(rawUserId, out Guid userId))
                {
                    return Unauthorized();
                }

                await _refreshTokenRepository.DeleteAll(userId);
                return NoContent();
            }

            private IActionResult BadRequestModelState()
            {
                IEnumerable<string> errorMessages = ModelState.Values.SelectMany(v => v.Errors.Select(e => e.ErrorMessage));
                return BadRequest(errorMessages);
            }
        }
```

3. Now our UserManager needs to have access to our database and to do that we have to update our code in program.cs.

```csharp
    builder.Services.AddIdentityCore<User>(o =>
{
    o.User.RequireUniqueEmail = true;
    o.Password.RequireDigit = false;
    o.Password.RequireNonAlphanumeric = false;
    o.Password.RequireUppercase = false;
    o.Password.RequiredLength = 0;
}).AddEntityFrameworkStores<AuthenticationDbContext>();
```

4. And implement IdentityUser to our User model. Below is the updated code where the properties were removed. Then change the **Username** to **UserName** since IdentityUser uses different format.

```csharp
    using System;
    using Microsoft.AspNetCore.Identity;

    namespace AuthenticationServer.API.Models
    {
        public class User : IdentityUser<Guid>
        {
        }
    }
```

5. Migrate once again for our IdentityUser. Run and test your projects. All endpoints should be running smoothly.