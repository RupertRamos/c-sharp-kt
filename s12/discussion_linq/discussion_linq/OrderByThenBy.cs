﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion_linq
{
    public class OrderByThenBy
    {
        public void OrderByOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            //Method Based
            var data = courses.OrderBy(c => c.Price);

            // Query Based
            // var data = from c in courses orderby c.Price select c;

            foreach(var item in data)
            {
                Console.WriteLine($"{item.Name} - P{item.Price} - {item.Author}");
            }
        }

        public void ThenByOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            //Method Based
            var data = courses.OrderBy(c => c.Price).ThenByDescending(a => a.Author);

            foreach (var item in data)
            {
                Console.WriteLine($"{item.Name} - P{item.Price} - {item.Author}");
            }
        }

    }
}
