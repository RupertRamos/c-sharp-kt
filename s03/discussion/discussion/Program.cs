﻿using System.Collections;

namespace Discussion
{
    class Discussion
    {
        static void Main(String[] args)
        {
            //[SECTION] Control Structures
            int numA = 10;
            int numB = 3;

            if(numA > numB)
            {
                Console.WriteLine("numA is greater than numB");
            } else if (numA == numB) 
            {
                Console.WriteLine("numA is equal to numB");
            } else
            {
                Console.WriteLine("numB is greater than numA");
            }


          /*  Console.WriteLine("What day of the week is today?");
            String day = Console.ReadLine().ToLower();

            Console.WriteLine("Result of switch statements: ");

            switch (day)
            {
                case "monday":
                    Console.WriteLine("Red");
                    break;
                default:
                    Console.WriteLine("Black");
                    break;
            }*/

            Console.WriteLine("Result of Conditional Operators:");
            Console.WriteLine(numA > numB ? true : false);
            Console.WriteLine(numA > numB ? printHelloWorld() : printName("Jonh", "Doe"));


            //[SECTION] Loops
            String word = "hello";

            for(int x = 0; x < word.Length; x++)
            {
                Console.WriteLine(word[x]);
            }

            foreach(char letter in word)
            {
                Console.WriteLine(letter);
            }

            // [SECTION] Arrays
            int[] sales = new int[3];

            sales[0] = 10;
            sales[1] = 3;
            sales[2] = 35;

            String stringifiedArray = String.Join(", ", sales);
            Console.WriteLine(stringifiedArray);

           /* Console.WriteLine("Result from arrays:");
            Console.WriteLine(sales[0]);
            Console.WriteLine(sales[1]);
            Console.WriteLine(sales[2]);
            Console.WriteLine(sales);*/

            foreach(int sale in sales)
            {
                Console.WriteLine(sale);
            }



            //[SECTION] ArrayLists
            //Declaring an array list
            ArrayList myArrayList = new ArrayList();

            //Declaring and initializing an array list
            ArrayList customers = new ArrayList(new string[] {"Donald", "Mickey", "Goofy"});

            // Getting the index of an element
            customers.IndexOf("Mickey");

            foreach (string customer in customers)
            {
                Console.WriteLine(customer);
            }

            String stringifiedArrayList = String.Join(", ", customers.ToArray());
            Console.WriteLine(stringifiedArrayList);
            Console.WriteLine(String.Join(", ", customers.ToArray()));

            myArrayList.Add("Harry");
            myArrayList.Add("Ron");
            myArrayList.Add("Hermione");
            Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

            myArrayList.Insert(1, "Tom");
            Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

            customers.Remove("Mickey");
            Console.WriteLine(String.Join(", ", customers.ToArray()));

            customers.RemoveAt(0);
            Console.WriteLine(String.Join(", ", customers.ToArray()));

            Console.WriteLine("Before");
            Console.WriteLine(String.Join(", ", myArrayList.ToArray()));
            myArrayList.RemoveRange(1, 2);
            Console.WriteLine("After");
            Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

            //Update
            myArrayList[0] = "Luna";
            myArrayList[1] = "Draco";
            Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

            myArrayList.Sort();
            Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

            Console.WriteLine(myArrayList.Contains("Luna"));
            Console.WriteLine(myArrayList.Contains("luna"));


            // Comparing ArrayList String Values
            Console.WriteLine("Input a name to check: ");
            String stringComparison = Console.ReadLine();

            for (int x = 0; x < myArrayList.Count; x++)
            {
                if (string.Equals((string?)myArrayList[x], stringComparison, StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine($"{stringComparison} found.");
                    break;
                };

                if(x == myArrayList.Count - 1)
                {
                    Console.WriteLine("Name not Found");
                }
            }

            //Declaring
            Hashtable myHashtable = new Hashtable();

            // No need to declare data types unlike in Hashmaps for Java
            Hashtable address = new Hashtable();
            address.Add("houseNumber", "15");
            address.Add("street", "Apple");
            address.Add("city", "California");
            address.Add("zip", "14731");

            // Go back here.
            ICollection addressKeys = address.Keys;
            //Console.WriteLine(addressKeys.ToString());

            // Accessing Hashtable Keys
            Console.WriteLine("Result from accessing Hashtable keys:");
            Console.WriteLine(address["street"]);

            Console.WriteLine("Result from Hashtables:");

            /* foreach (string key in addressKeys)
             {
                 Console.WriteLine(key);
             }*/

            foreach (string key in addressKeys)
            {
                Console.WriteLine(address[key]);
            }

        }

        public static String printHelloWorld()
        {
            Console.WriteLine("Hello World");
            return "Hello World";
        }

        public static String printName(String firstName, String lastName)
        {
            Console.WriteLine($"The name provided is {firstName} {lastName}");
            return $"The name provided is {firstName} {lastName}";
        }
    }
}