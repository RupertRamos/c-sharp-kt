﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion_linq
{
    public class Select
    {
        public void SelectOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            // [Section] Method Based Syntax
            // The Select operator is used for selecting a particular property out of an object
            // var data = courses.Select(c => c.Author);

            // [Section] Query Based Syntax
            var data = from c in courses select c.Author;

            foreach (var item in data)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        public void SelectNameAndPrice()
        {
            var courses = CourseDatabase.GetCoursesData();

            // Method Based Syntax
            /*
            var data = courses.Select(c => new
            {
                CourseName = c.Name,
                CoursePrice = c.Price
            });
            */

            // Query Based Syntax
            /*
            var data = from c in courses
            select new
            {
                CourseName = c.Name,
                CoursePrice = c.Price
            };
            */

            // Using tuples
            // Tuples provide a concise syntax to group multiple data elements in a lightweight data structure
            // We can work with the returned tuple instance directly or deconstruct it in separate variables as shown in the example below
            var data = courses.Select(c => (CourseName: c.Name, CoursePrice: c.Price)).ToList();


            foreach (var item in data)
            {
                Console.WriteLine($"{item.CourseName} - P{item.CoursePrice}");
            }

            Console.ReadLine();
        }


    }
}
