﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion_linq
{
    public class Select
    {
        public void SelectOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            // Method Based Syntax
            //var data = courses.Select(c => c.Author);

            // Query Based Syntax
            var data = from c in courses select c.Author;

            foreach (var item in data)
            {
                Console.WriteLine(item);
            }
        }


        public void SelectNameAndPrice()
        {
            var courses = CourseDatabase.GetCoursesData();

            // Method Based Syntax
            //Similar to the concept of object destructuring
            /*
             * var data = courses.Select(c => new
            {
                CourseName = c.Name,
                CoursePrice = c.Price,
            });
            */

            // Query Based Syntax
           /* var data = from c in courses select new
            {
                CourseName = c.Name,
                CoursePrice = c.Price,
            };*/

            //Tuples
            var data = courses.Select(c => (CourseName: c.Name, CoursePrice: c.Price)).ToList();

            foreach(var item in data)
            {
                Console.WriteLine($"{item.CourseName} - P{item.CoursePrice}");
            }

        }


    }
}
