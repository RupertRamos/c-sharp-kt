﻿namespace discussion
{
    class Discussion
    {
        static void Main(string[] args)
        {
            // [Section] Exceptions
            //Console.WriteLine("Please input a number:");
            //int numA = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine($"The number you've input is {numA}.");


            //[Section] Try Catch Finally Statements

          /*  int numA = 0;

            try
            {
                Console.WriteLine("Please input a number:");
                numA = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException  e)
            {
                Console.WriteLine("The input you provided is not a number.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (numA == 0)
                {
                    Console.WriteLine("Please input a valid number");
                    numA = Convert.ToInt32(Console.ReadLine());
                }
                else
                {
                    Console.WriteLine($"The number you provided is {numA}");
                }

            }*/

            try
            {
                MultipleExceptions();
            }
            catch (DivideByZeroException e) 
            {
                Console.WriteLine("Cannot divide a number by zero");
                Console.WriteLine(e.Message);
            }
            catch (MyException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception encountered");
                Console.WriteLine(e.Message);
            }




        }

        public static void MultipleExceptions()
        {
            Console.WriteLine("Which error would you like to receive? [1]DivideByZeroException, [2]UserDefinedException");

            int option = Convert.ToInt32 (Console.ReadLine());

            switch (option)
            {
                case 1:
                    Console.WriteLine("Please input a number: ");
                    int number = Convert.ToInt32(Console.ReadLine());
                    int dividedByZero = number / 0;
                    break;
                case 2:
                    throw (new MyException("This is a user defined exeption"));
                    break;
                default:
                    Console.WriteLine("Please input a valid option");
                    break;
            }
        }
    }
}