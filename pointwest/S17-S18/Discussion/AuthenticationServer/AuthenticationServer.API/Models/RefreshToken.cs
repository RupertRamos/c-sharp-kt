﻿using System;
namespace AuthenticationServer.API.Models
{
    public class RefreshToken
    {
        // As a unique identifier
        public Guid Id { get; set; }

        // This two is what we need from our new refresh token
        public string Token { get; set; }

        public Guid UserId { get; set; }
    }
}

