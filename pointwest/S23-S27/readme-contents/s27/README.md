# Session Objectives

At the end of the session, the students are expected to:

- learn about what Docker is, its purpose, and how to use its basic functionalities.

# Resources

## Instructional Materials

- [GitLab Repository (Guide)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1JQibSxJ5K3qGlbdKDp-aRNwBJaHfvBHaTzCNDXnSGT0/edit)

## Supplemental Materials

- [Containers vs. Virtual Machines (VMs): What's the Difference?](https://www.netapp.com/blog/containers-vs-vms/)
- [Docker Introduction (IBM)](https://www.ibm.com/cloud/learn/docker)

# Lesson Proper

## What is a Virtual Machine?

A virtual machine (or VM) allows the simulation of an operating system that runs like it is on a separate computer. For example, an Ubuntu VM (called a guest) can be created within your Windows computer (called a host).

![Illustration of a host machine containing two virtual machines in it.](readme-images/Untitled.png)

Illustration of a host machine containing two virtual machines in it.

To create a virtual machine, you will need sufficient hardware resources from your actual computer. However, creating a virtual machine in your computer is not feasible most of the time.

If a computer has a modest RAM of 8GB and a dual-core processor, it will be difficult for a user to create or maintain a running virtual machine.

This is where containers come in.

Below is the definition of virtual machines in Docker's website:

*Virtual machines (VMs) are an abstraction of physical hardware turning one server into many servers. The hypervisor allows multiple VMs to run on a single machine.* 

*Each VM includes a full copy of an operating system, the application, necessary binaries and libraries - taking up tens of GBs. VMs can also be slow to boot.*

## What is a Container?

A container is a software package that, unlike a virtual machine, only contains everything the software needs to run. This includes the executable program as well as system tools, libraries, and settings.

![App containerization diagram from Docker.](readme-images/Untitled%201.png)

App containerization diagram from Docker.

Below is the definition of container in Docker's website:

*Containers are an abstraction at the app layer that packages code and dependencies together. Multiple containers can run on the same machine and share the OS kernel with other containers, each running as isolated processes in user space.* 

*Containers take up less space than VMs (container images are typically tens of MBs in size), can handle more applications and require fewer VMs and Operating systems.*

## Virtual Machines vs. Containers

There is a fundamental difference between VMs and containers: VMs virtualize hardware while containers virtualize software.

![Untitled](readme-images/Untitled%202.png)

Since containers virtualize only the software, containers have less components to worry about.

Also, since there is no need to set up the virtual hardware, only resources that are actually needed by an app is allocated when using containers.

## What is Docker?

According to AWS' [website](https://aws.amazon.com/docker/), Docker is a software platform that allows you to build, test, and deploy applications quickly. 

Docker packages software into standardized units called containers that have everything the software needs to run including libraries, system tools, code, and runtime. 

Using Docker, you can quickly deploy and scale applications into any environment and know your code will run.

## Why use Docker?

Here are some of the reasons why you should use Docker:

- **It enables more efficient use of system resources**. Instances of containerized apps use far less memory than virtual machines, they start up and stop more quickly, and they can be packed far more densely on their host hardware. All of this amounts to less spending on IT.
- **It enables faster software delivery cycles**. Docker containers make it easy to put new versions of software, with new business features, into production quickly—and to quickly roll back to a previous version if you need to.
- **It enables application portability**. Because Docker containers encapsulate everything an application needs to run (and only those things), they allow applications to be shuttled easily between environments.

## Who uses Docker?

Some of the [companies](https://www.docker.com/customers) who use Docker are the following:

- Adobe
- Paypal
- Netflix
- Verizon

## Docker Terms

These are the basic terms we need to know in relation to Docker.

- **Images**. Images contain sets of instructions to build a container.
- **Volumes**. Volumes are designed to persist data, independent of the container's life cycle.

## Creating the First Docker Container

Before proceeding, execute the `docker --version` command to check if you can use Docker CLI to your terminal.

If the command prints the Docker version, let's create our first Docker-hosted app by executing the command below:

```bash
docker run -d -p 80:80 docker/getting-started
```

Once the command is running, it will download the necessary resources.

![Untitled](readme-images/Untitled%203.png)

![Untitled](readme-images/Untitled%204.png)

Let's break down each part of the command:

- The `run` command creates a new container and starts the created container.
- The `-d` flag starts the container in detached mode (running in the background).
- The `-p` flag specifies which port the container will use (in this case, port 80).
- The `docker/getting-started` specifies the image to be used.

Once the command has finished execution, we can check the program from the container by going to [http://localhost:80](http://localhost:80) in our web browser.

![Untitled](readme-images/Untitled%205.png)

## Docker Commands

For instructors, demonstrate each command using the terminal.

Aside from `docker run`, we can also use the other commands:

- `docker ps` lists the containers that exists in the machine. Adding the `-a` flag lists all the containers, even the ones that are not running.
- `docker stop` stops a container.
- `docker start` starts a container.
- `docker rm [container_id]` deletes a container.

To run a new container and give it a name, add the following code in the command:

```bash
docker run -d -p 81:81 --name getting_started docker/getting-started
```

To rename an existing container, use the command below:

```bash
docker rename getting_started starter_container
```

## Docker Desktop Walkthrough

So far, we have used Docker through the terminal. However, for macOS and Windows, Docker includes a program called Docker Desktop.

The mentioned program can be used to do a variety of actions without using the terminal. 

There are three main tabs in Docker Desktop: **Containers**, **Images** and **Volumes**.

### Containers Tab

![Untitled](readme-images/Untitled%206.png)

The **Containers** tab contain the list of all containers that exist in the system. Once you hover to a given container, it will show five buttons to the right. In the example above, it has the following options:

- Open in Browser
- CLI
- Stop (or Start)
- Restart
- Delete

Additionally, a container name can be clicked.

![Untitled](readme-images/Untitled%207.png)

Once a container name is clicked, it will show the logs generated while running the container. Additionally, it also has the **Inspect** and **Stats** tab shown in the left side of the buttons earlier.

### Images Tab

The **Images** tab lists all the images used to create a container.

![Untitled](readme-images/Untitled%208.png)

In this tab, image-related information can be seen, such as the disk size of a given image. Once an image is downloaded to create a container, it can be reused in cases where the same image is to be used.

Additional actions are shown upon hovering an image.

![Untitled](readme-images/Untitled%209.png)

By clicking on the **Inspect** action, we can see the individual command history executed in the image.

![Untitled](readme-images/Untitled%2010.png)

### Volumes Tab

The **Volumes** tab contain instances of persistent disk storage needed by a given container. For now, now volumes are created by the container created earlier.

![Untitled](readme-images/Untitled%2011.png)

# Activity

## Quiz

### Instructions

Answer the quiz form provided by your instructor.

For instructors, create a copy of this [quiz form](https://docs.google.com/forms/d/1j0Zwacpuq6CJ5aHIWrzX_XnUwUY3O3XB74njKZZaxq0/edit) to your own batch kit folder.

### Answers

Allows the simulation of an operating system that runs like it is on a separate computer.

- virtual machine, virtual machines

To create a virtual machine, you will need sufficient _________ resources from your actual computer.

- hardware

They are an abstraction of physical hardware turning one server into many servers.

- virtual machine, virtual machines

A software package that only contains everything the software needs to run.

- container, containers

They are an abstraction at the app layer that packages code and dependencies together.

- container, containers

A software platform that allows you to build, test, and deploy applications quickly.

- Docker

Contain sets of instructions to build a container.

- image, images

Designed to persist data, independent of the container's life cycle.

- volume, volumes

The docker command that creates a new container and starts the created container.

- docker run

The docker command flag that starts the container in detached mode (running in the background).

- -d

The docker command flag that specifies which port the container will use.

- -p

The docker command that lists the containers that exists in the machine.

- docker ps