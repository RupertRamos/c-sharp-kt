﻿using AuthenticationServer.API.Models;

namespace AuthenticationServer.API.Services.UserRepositories
{
    public class InMemoryUserRepositories : IUserRepository
    {
        // Store our user in a list
        private readonly List<User> _users = new List<User>();

        public Task<User> Create(User user)
        {
            // when addin a user, this method gives them a unique Id.
            user.Id = Guid.NewGuid();

            // add new user to our list.
            _users.Add(user);

            // Return user from the result.
            return Task.FromResult(user);
        }

        public Task<User> GetByEmail(string email)
        {
            // This checks whether we have that email in our user's list and when not found it will return null.
            return Task.FromResult(_users.FirstOrDefault(u => u.Email == email));
        }

        public Task<User> GetByUsername(string username)
        {
            return Task.FromResult(_users.FirstOrDefault(u =>u.UserName == username));
        }

        public Task<User> GetById(Guid userId)
        {
            return Task.FromResult(_users.FirstOrDefault(u => u.Id == userId));
        }
    }
}
