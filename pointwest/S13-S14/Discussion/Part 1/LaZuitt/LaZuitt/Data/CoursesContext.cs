using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using LaZuitt.Models;

public class CoursesContext : DbContext
{
    public CoursesContext (DbContextOptions<CoursesContext> options)
        : base(options)
    {
    }

    public DbSet<LaZuitt.Models.Course> Course { get; set; }
}