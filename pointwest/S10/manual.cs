// ==================================
// Intro to .NET Core and ASP.NET MVC
// ==================================

/*
Other References:

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "ASP.NET Core Web App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

        /*
        Important Note:
            - Upon creating the project, several files and folders will be created that will serve different functionalities as follows:
                - The "Dependencies" folder will contain dependency files for making the framework run properly.
                - The "Properties" folder will contain a "launchSettings.json" file which describes how a project can be launched, the command to run, whether the browser should be opened, which environment variables should be set and other useful information.
                - The "wwwroot folder" will contain static files such as CSS, JS, Bootstrap and jquery libraries. Bootstrap is already included in the default application and can be readily used after creating the project.
                - The "Controller", "Models" and Views folders are support the MVC pattern for building MVC web applications.
                - The "asspettings.json" is used to store information such as connection strings or application specific settings in JSON format.
                - The "Program.cs" is the main entry point of the application is the entry point for the application. This also contains the default route for the MVC application under the "app.MapControllerRoute" method.
                - The "HomeController.cs" returns the view "Index.cshtml" as the initial page.
        */

/*
2. Run the application to see the result of the code.
    Visual Studio
*/

        /*
        Important Note:
            - To run the application, a "green play button" can be found in visual studio which will execute the code added in the previous step.
            - Alternatively, pressing the "F5" button on the keyboard can be used to run the application.
            - A debug console will appear to print out the "Hello World" message.
            - If a project is opened via the "File > Open > Folder" option, running the application will automatically close the debug console.
            - To open a project, use the "File > Open > Project/Solution" option instead and select the corresponding solution file for the project indicated by the ".sln" extension.
            - A window pop up may appear asking to "Trust the ASP.NET Core SSL Certificate", since this is a locally created file, this should have no risk and we can select "yes" on all windows and trust the certificate.
            - By default, the ASP.NET application uses port "7143" and can be accessed at "https://localhost:7143/". Take not that this is only accessible when the application is running and will open a browser window separately from existing opened browsers.
        */

/*
3. Create a "CoursesController.cs" file.
    discussion > Controllers > CoursesController.cs
*/
     
        namespace discussion.Controllers
        {
            public class CoursesController : Controller
            {
                public IActionResult Index()
                {
                    return View();
                }
            }
        }

        /*
        Important Note:
            - To create a controller in visual studio, do the following:
                - Right click on "Controllers" in the "solution explorer".
                - Select "add" and then select "Class". Alternatively, you may select the "Controller" option instead which will redirect the window to the next step in the selection.
                - Select "Web" then "ASP.NET" and finally "C# Class" from the templates.
                - Rename the class by right clicking on the "HomeController1.cs" file in the "solution explorer" and choosing the "Rename" option.
            - Creating files is disabled while the application is running. You can stop the application by pressing the Red "Stop" button which only appears if the application is running. This will also close the browser where the application is shown for ease of use.
            - The method name of the controller file should match the name of the view file in order to properly navigate to the correct page. An "Index.cshtml" will be created in the succeeding steps.
        */

/*
4. Add the CSS reset and change the font of the application.
    discussion > wwwroot > css > site.css
*/

        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        html {
          font-size: 14px;
        }

        @media (min-width: 768px) {
          html {
            font-size: 16px;
          }
        }

        html {
          position: relative;
          min-height: 100%;
        }

        body {
            margin-bottom: 60px;
            font-family: Arial, Helvetica, sans-serif;
        }

/*
5. Modify the "Home" page.
    discussion > Views > Home > Index.cshtml
*/

        @{
            ViewData["Title"] = "Home Page";
        }

        <div class="text-center" style="padding-bottom: 100px">
            <h1 class="display-4">Welcome to LaZuitt</h1>
            <br />
        </div>

        <div class="text-center">
            <button style="float: center; background-color: #e1f5fe; border-radius: 10px">
                <a class="nav-link text-dark" asp-area="" asp-controller="Courses" asp-action="Index">
                    Manage Courses
                </a>
            </button>
        </div>

        /*
        Important Note:
            - In the anchor tag, the "asp-controller" is used to define the controller file to be used when clicking on the link, the "asp-action" refers to the method to be executed in the controller to render the view page.
            - Clicking the "Hot Reload" button after making changes to the file will allow to apply changes to the browser.
            - Alternativelu you may press Alt + F10 to use the keyboard shortcut.
        */

/*
6. Create a "Settings" folder and an "Index.cshtml" file.
    discussion > Views > Settings > Index.cshtml
*/
    
        @{
            ViewData["Title"] = "Settings";
        }

        /*
        Important Note:
            - The code above will set the title of the page to "Settings". At this moment we will leave the page empty and return to update the page in the succeeding discussions.
            - To create a new folder in visual studio, do the following:
                - Right click on "discussion" in the "solution explorer".
                - Select "add" and then select "New Folder".
            - To create a view in visual studio, do the following:
                - Right click on "Views" in the "solution explorer".
                - Select "add" and then select "View".
                - The "Razor View - Empty" option should be selected by default and click on "Add" to create the view file.
        */

/*
7. Create a "Courses" folder and an "Index.cshtml" file.
    discussion > Views > Courses > Index.cshtml
*/
    
        @{
            ViewData["Title"] = "Courses Page";
        }

        <div class="text-center mb-5">
            <h1 class="display-4">
                Welcome to LaZuitt
            </h1>
            <br />
            <h3>Hello, Admin</h3>
        </div>

        <div class="d-flex justify-content-between mb-3">
            <h2 class="d-inline">Courses Available</h2>
            <button class="btn btn-secondary text-light" >
                <a class="nav-link text-dark" asp-area="" asp-controller="Courses" asp-action="CreateCourse">
                    Create Course
                </a>
            </button>
        </div>

        <div class="row d-flex justify-content-between">
        <div class="card bg-white p-3 text-center" style="width: 22.5%; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2)">
                <h3>ASP.NET</h3>
                <img class="img-fluid h-50" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMOH00P2p0cF3CtYyLLUp_7yTJsoqpwIyxeUihYWnOszvwSbFcJbyv3NleB4ackTfap2I&usqp=CAU"/>

                <p>ASP is a development framework for building web pages.</p>

                <div class="d-flex flex-column justify-content-center align-items-center">
                    <button class="mb-2 btn btn-primary w-100">Edit</button>
                    <button class="mb-2 btn btn-info w-100">Details</button>
                    <button class="mb-2 btn btn-danger w-100">Delete</button>
                </div>
                            
            </div>

        <div class="card bg-white p-3 text-center" style="width: 22.5%; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2)">
                <h3>Python</h3>
                <img class="img-fluid h-50" src="https://i.pinimg.com/originals/a8/53/14/a8531424a5fac660e4261f72ca817141.png"/>

                <p>Python can be used on a server to create web applications.</p>

                <div class="d-flex flex-column justify-content-center align-items-center">
                    <button class="mb-2 btn btn-primary w-100">Edit</button>
                    <button class="mb-2 btn btn-info w-100">Details</button>
                    <button class="mb-2 btn btn-danger w-100">Delete</button>
                </div>
                            
            </div>

        <div class="card bg-white p-3 text-center" style="width: 22.5%; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2)">
                <h3>Django</h3>
                <img class="img-fluid h-50" src="https://youteam.io/blog/wp-content/uploads/2022/06/django-icon-0.png"/>

                <p>Django is a Python framework that makes it easier to create websites using Python.</p>

                <div class="d-flex flex-column justify-content-center align-items-center">
                    <button class="mb-2 btn btn-primary w-100">Edit</button>
                    <button class="mb-2 btn btn-info w-100">Details</button>
                    <button class="mb-2 btn btn-danger w-100">Delete</button>
                </div>
                            
            </div>

        <div class="card bg-white p-3 text-center" style="width: 22.5%; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2)">
                <h3>JavaScript</h3>
                <img class="img-fluid h-50" src="https://upload.wikimedia.org/wikipedia/commons/6/6a/JavaScript-logo.png"/>

                <p>JavaScript is the most known programming language of the web</p>

                <div class="d-flex flex-column justify-content-center align-items-center">
                    <button class="mb-2 btn btn-primary w-100">Edit</button>
                    <button class="mb-2 btn btn-info w-100">Details</button>
                    <button class="mb-2 btn btn-danger w-100">Delete</button>
                </div>
                            
            </div>

        </div>

        /*
        Important Note:
            - Naming this file "Index" is important because it allows the application to identify the correct view page to load which was previously added in our "CoursesController.cs" file.
        */

/*
8. Modify the "_Layout.cshtml" file to redirect to the recently created view files.
    discussion > Shared > _Layout.cshtml
*/

        // ...
        <body>
            <header>
                <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3">
                    <div class="container-fluid">
                        // ...
                        <div class="navbar-collapse collapse d-sm-inline-flex justify-content-between">
                            <ul class="navbar-nav flex-grow-1">
                                <li class="nav-item">
                                    <a class="nav-link text-dark" asp-area="" asp-controller="Courses" asp-action="Index">Courses</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark" asp-area="" asp-controller="Settings" asp-action="Index">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div class="container">
                // ...
            </div>

            // ...
        </body>
        </html>

/*
========
Activity
========
*/

/*
1. Create a "CreateCourse.cshtml" view file.
    discussion > Views > Courses > CreateCourse.cshtml
*/

        @{
            ViewData["Title"] = "Create Course";
        }

        <h1>Create New Course</h1>
        <br />

        <table border="0" cellpadding="5" cellspacing="0" width="600">
            <tr>
                <td>
                    <label for="CourseTitle">Course Title:</label>
                </td>
                <td>
                    <input name="CourseTitle" type="text" maxlength="60" style="width:100%;max-width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Stack">Stack:</label>
                </td>
                <td>
                    <select name="Stack" type="text" maxlength="60" style="width:100%;max-width: 300px;">
                        <option value="empty"></option>
                        <option value="mern">MERN Stack</option>
                        <option value="mean">MEAN Stack</option>
                        <option value="django">Django Stack</option>
                        <option value="rails">Rails</option>
                        <option value="lamp">LAMP Stack</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Description">Description:</label>
                </td>
                <td>
                    <input name="Description" type="text" maxlength="43" style="width: 100%; max-width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Thumbnail">Thumbnail:</label>
                </td>
                <td>
                    <input name="Thumbnail" type="text" maxlength="43" style="width: 100%; max-width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Price">Price:</label>
                </td>
                <td>
                    <input name="Price" type="text" maxlength="43" style="width: 100%; max-width: 300px;" />
                </td>

        </table>
        <br />
        <button name="Create" type="submit" maxlength="43" style="width: 100%; max-width: 300px; margin-left:210px; background-color:gray">
            Create
        </button>

/*
2. Add a "CreateCourse" controller method.
    discussion > Controllers > CoursesController.cs
*/

        // ...

        namespace discussion.Controllers
        {
            public class CoursesController : Controller
            {
                public IActionResult Index()
                {
                    // ...
                }

                public IActionResult CreateCourse()
                {
                    return View();
                }

            }
        }

/*
3. Modify the "Courses" page to be able to properly navigate to the "CreateCourse" page.
    discussion > Views > Courses > Index.cshtml
*/

        // ...
        <body>

            // ...

            <div>
                <h2 style="display:inline-block">Courses available</h2>
                <button style="float:right; background-color:gray">
                    <a class="nav-link text-dark" asp-area="" asp-controller="Home" asp-action="Index">
                        Create New
                    </a>
                </button>


            </div>

            // ...

        </body>
        // ...