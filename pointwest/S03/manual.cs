// ==========================================
// Control Structures, Arrays and Collections
// ==========================================

/*
Other References:
    C# Decision Making
        https://www.tutorialspoint.com/csharp/csharp_decision_making.htm
    C# Ternary Conditional Operators
        https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/operators/conditional-operator
    C# Loops
        https://www.tutorialspoint.com/csharp/csharp_loops.htm
    C# Arrays
        https://www.tutorialspoint.com/csharp/csharp_arrays.htm
    C# ArrayLists
        https://learn.microsoft.com/en-us/dotnet/api/system.collections.arraylist?view=net-6.0
    C# HashMaps
        https://learn.microsoft.com/en-us/dotnet/api/system.collections.hashtable?view=net-6.0
    How to check if a string has only whitespace or is empty in C#
        https://www.educative.io/answers/how-to-check-if-a-string-has-only-whitespace-or-is-empty-in-c-sharp

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "C# Console App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Add code to demonstrate the use of conditional statements/selection control structures.
    Application > discussion > Program.cs
*/

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // [Section] Conditional Statements/Selection Control Structures
                    // Conditional statements allow us to control the flow of a program to result in different outputs based on specific conditions

                    // Variables to use with if, else if and else statements
                    int numA = 10;
                    int numB = 3;

                    // If, else if and else statements
                    Console.WriteLine("Result of If, Else If and Else Statements:");
                    if (numA > numB)
                    {
                        Console.WriteLine("numA is greater than numB");
                    }
                    else if (numA == numB)
                    {
                        Console.WriteLine("numA is equal to numB");
                    }
                    else
                    {
                        Console.WriteLine("numA is less than numB");
                    }

                    // Switch statements
                    // The "ToLower" method returns a copy of the string converted to all lowercase letters
                    Console.WriteLine("What day of the week is today:");
                    String day = Console.ReadLine().ToLower();

                    Console.WriteLine("Result of Switch Statements:");
                    switch (day)
                    {
                        case "monday":
                            Console.WriteLine("Red");
                            break;
                        case "tuesday":
                            Console.WriteLine("Blue");
                            break;
                        case "wednesday":
                            Console.WriteLine("Green");
                            break;
                        default:
                            Console.WriteLine("Black");
                            break;
                    }

                    // Conditional Operators
                    // Conditional operators works similarly to an if and else statement which has a shorter syntax and cleaner code
                    Console.WriteLine("Result of Conditional Operators:");
                    Console.WriteLine(numA > numB ? true : false);

                    // Conditional Operators with Functions
                    // Since conditional operators only result to one of two options, functions are commonly used to execute multiple lines of code and to make code more readable
                    Console.WriteLine(numA > numB ? printHelloWorld() : printName("john", "doe"));

                }

                // Functions for use with ternary conditional operators
                public static String printHelloWorld()
                {
                    Console.WriteLine("Hello World");
                    return "Hello World";
                }

                public static String printName(String firstName, String lastName)
                {
                    Console.WriteLine($"The name provided is: {firstName} {lastName}");
                    return $"The name provided is: {firstName} {lastName}";
                }

                
            }
        }

/*
3. Add code to demonstrate the use of ternary conditional operators.
    Application > discussion > Program.cs
*/

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...

                    Console.WriteLine("Result of Switch Statements:");
                    switch (day)
                    {
                        // ...
                    }

                    // [Section] Ternary Conditional Operators
                    // Ternary conditional operators works similarly to an if and else statement which has a shorter syntax and cleaner code
                    Console.WriteLine("Result of Conditional Operators:");
                    Console.WriteLine(numA > numB ? true : false);

                    // Ternary Conditional Operators with Functions
                    // Since ternary conditional operators only result to one of two options, functions are commonly used to execute multiple lines of code and to make code more readable
                    Console.WriteLine(numA > numB ? printHelloWorld() : printName("john", "doe"));

                }

                // Functions for use with ternary conditional operators
                public static String printHelloWorld()
                {
                    Console.WriteLine("Hello World");
                    return "Hello World";
                }

                public static String printName(String firstName, String lastName)
                {
                    Console.WriteLine($"The name provided is: {firstName} {lastName}");
                    return $"The name provided is: {firstName} {lastName}";
                }
                
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentations for C# Ternary Conditional Operators.
        */

/*
4. Add code to demonstrate the use of loops/repetition control structures.
    Application > discussion > Program.cs
*/

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...

                    Console.WriteLine(numA > numB ? printHelloWorld() : printName("john", "doe"));

                    // [Section] Loops/Repetitive Control Structures
                    // Loops allow us to execute repetitive tasks with a shorter syntax and are essential to programming
                    // They're commonly used with arrays in order to output different results

                    String word = "hello";

                    // Code to printout all values of an array without loops
                    Console.WriteLine("Result of manually printing array elements:");
                    Console.WriteLine(word[0]);
                    Console.WriteLine(word[1]);
                    Console.WriteLine(word[2]);
                    Console.WriteLine(word[3]);
                    Console.WriteLine(word[4]);

                    // For Loops
                    // For loops are used to execute a block of code based on a given condition, typically using a number value that increases/decreases with each iteration of the loop
                    Console.WriteLine("Result of using for loops with array elements:");
                    for (int x = 0; x < word.Length; x++)
                    {
                        Console.WriteLine(word[x]);
                    }

                    // ForEach Loops
                    // ForEach Loops are used to execute a block of code for each element in a collection
                    Console.WriteLine("Result from array forEach loop:");
                    foreach (char letter in word)
                    {
                        Console.WriteLine(letter);
                    }

                    // While Loops
                    // While loops are used to execute a block of code depending on a condition so long as that condition is true
                    Console.WriteLine("Result of While Loops:");
                    int numC = 5;
                    while (numC > 0)
                    {
                        Console.WriteLine($"value of a: {numC}");
                        numC--;
                    }

                    // Continue Statements
                    // Continue statements are used to allow loops to continue to the next iteration of the loop when it is reached/executed skipping all other code statements/blocks that come after it
                    Console.WriteLine("Result of Continue Statements:");
                    int numD = 0;
                    while (numD < 20)
                    {
                        if (numD % 5 == 0)
                        {
                            Console.WriteLine("value of numC: {0}", numD);
                        }

                        numD++;

                        continue;
                    }

                    // Break Statements
                    // Break statements are used to allow loops to break/stop loops when it is reached/executed skipping all other code statements/blocks that come after it
                    Console.WriteLine("Result of Break Statements:");
                    int numE = 0;
                    while (numE < 20)
                    {
                        if (numE == 10)
                        {
                            break;
                        }
                        if (numE % 5 == 0)
                        {
                            Console.WriteLine("value of numD: {0}", numE);
                        }

                        numE++;
                    }

                }

                // ...
                
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentations for C# Loops.
        */

/*
5. Add code to demonstrate the use of arrays.
    Application > discussion > Program.cs
*/

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...

                    while (numE < 20)
                    {
                        // ...
                    }

                    // [Section] Arrays
                    // An array stores a fixed-size sequential collection of elements of the same type.
                    // An array is used to store a collection of data.

                    // Declaring Arrays
                    // An array is a reference type, so the "new" keyword is used to create an instance of an array
                    // As a reference type, it references an array class which allows access to different method/functions to make manipulation of arrays easier
                    int[] sales = new int[3];

                    // Assigning values to an array
                    // C# arrays start with the 0 index value as the first element
                    sales[0] = 10;
                    sales[1] = 5;
                    sales[2] = 27;

                    Console.WriteLine("Result from arrays:");
                    Console.WriteLine(sales[0]);
                    Console.WriteLine(sales[1]);
                    Console.WriteLine(sales[2]);
                    Console.WriteLine(sales);
                    Console.WriteLine($"First Element: {sales[0]}, Second Element: {sales[1]}, Third Element: {sales[2]}");

                    // Array declaration and initialization
                    // Declaring arrays only creates the array and defines the number of elements that can be stored in it
                    // Initialization is when an element is provided it's initial value, this is important to understand especially when dealing with documentation
                    string[] managers = new string[3]
                    {
                        "john", "jane", "joe"
                    };

                    // Printing the length of an array
                    Console.WriteLine(sales.Length);

                    // Printing arrays as a string
                    Console.WriteLine("Result of using String.Join with arrays:");
                    String stringifiedArray = String.Join(", ", managers);
                    Console.WriteLine(stringifiedArray);

                    // For Loops with arrays
                    Console.WriteLine("Result of using for loops with array elements:");
                    for (int x = 0; x < managers.Length; x++)
                    {
                        Console.WriteLine(managers[x]);
                    }

                    // ForEach Loops with arrays
                    // ForEach Loops are used to execute a block of code for each element in an array/collection
                    Console.WriteLine("Result from array forEach loop:");
                    foreach (string manager in managers)
                    {
                        Console.WriteLine(manager);
                    }

                }
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for C# Arrays.
        */

/*
6. Add more code to demonstrate the use of Array Lists.
    Application > discussion > Program.cs
*/

    	// ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...

                    foreach (string manager in managers)
                    {
                        // ...
                    }

                    // [Section] Collections
                    // ArrayList
                    // A resizeable list that represents an ordered collection of an object that can be indexed individually

                    // Declaring an array list
                    ArrayList myArrayList = new ArrayList();

                    // Declaring and initializing an array list
                    ArrayList customers = new ArrayList(new string[] { "Donald", "Mickey", "Goofy" });

                    // Getting the index of an array list element
                    customers.IndexOf("Mickey");

                    // ForEach loop with array lists
                    Console.WriteLine("Result from Array List:");
                    foreach (string customer in customers)
                    {
                        Console.WriteLine(customer);
                    }

                    // Getting the length of an array list
                    Console.WriteLine("Result of Count property:");
                    Console.WriteLine(customers.Count);

                    // Printing array lists as a string
                    Console.WriteLine("Result of using String.Join with arrays lists:");
                    String stringifiedArrayList = String.Join(", ", customers.ToArray());
                    Console.WriteLine(stringifiedArrayList);

                    // Adding elements to an array list
                    myArrayList.Add("Harry");
                    myArrayList.Add("Ron");
                    myArrayList.Add("Hermione");

                    Console.WriteLine("Result from adding Array List elements:");
                    Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

                    // Adding elements in between an array list
                    myArrayList.Insert(1, "Tom");

                    Console.WriteLine("Result from adding Array List elements in between:");
                    Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

                    // Removing elements to an array list
                    // Removing single elements
                    customers.Remove("Mickey");
                    customers.RemoveAt(0);

                    Console.WriteLine("Result from removing single Array List elements:");
                    Console.WriteLine(String.Join(", ", customers.ToArray()));

                    // Removing multiple elements
                    // Removes 2 elements from index 1
                    myArrayList.RemoveRange(1, 2);

                    Console.WriteLine("Result from removing multiple Array List elements:");
                    Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

                    // Updating array list elements
                    myArrayList[0] = "Luna";
                    myArrayList[1] = "Draco";

                    Console.WriteLine("Result from updating Array List elements:");
                    Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

                    // Sorting Arrays
                    myArrayList.Sort();

                    Console.WriteLine("Result from sorting Array List elements:");
                    Console.WriteLine(String.Join(", ", myArrayList.ToArray()));

                    // Checking if element exists in an Array List
                    // For strings, comparison using the "Contains" method is case sensitive
                    Console.WriteLine("Result from array list Contains method:");
                    Console.WriteLine(myArrayList.Contains("Luna"));
                    Console.WriteLine(myArrayList.Contains("luna"));

                    // Comparing ArrayList String Values
                    Console.WriteLine("Input a name to check:");
                    String stringComparison = Console.ReadLine();

                    for (int x = 0; x < myArrayList.Count; x++)
                    {
                        // "string?" converts the ArrayList element into a nullable string
                        // "StringComparison.OrdinalIgnoreCase" is used for case insensitive comparison
                        if (string.Equals((string?)myArrayList[x], stringComparison, StringComparison.OrdinalIgnoreCase))
                        {
                            Console.WriteLine($"{stringComparison} found.");
                            break;
                        }

                        if (x == myArrayList.Count - 1)
                        {
                            Console.WriteLine("Name not found");
                        }
                    }

                }
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for C# Array Lists.
        */

/*
7. Add more code to demonstrate the use of Hashtables.
    Application > discussion > Program.cs
*/

        // ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...

                    foreach (string arrayListItem in myArrayList)
                    {
                        // ...
                    }

                    // Hashtables
                    // Hashtables represents a collection of key-value pairs.
                    // It is used when you need to access elements by using keys.

                    // Declaring hashtables
                    Hashtable myHashtable = new Hashtable();

                    // Initializing Hashtables
                    Hashtable address = new Hashtable();

                    address.Add("houseNumber", "15");
                    address.Add("street", "Apple");
                    address.Add("city", "California");
                    address.Add("zip", "19463");

                    // Getting the keys of a hashtable
                    ICollection addressKeys = address.Keys;

                    // Accessing Hashtable Keys
                    Console.WriteLine("Result from accessing Hashtable keys:");
                    Console.WriteLine(address["street"]);

                    Console.WriteLine("Result from Hashtables:");
                    foreach (string key in addressKeys)
                    {
                        Console.WriteLine(address[key]);
                    }

                }
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for C# Hashtables.
        */

/*
========
Activity
========
*/

/*
1. Create a simple console application that functions as an asset management app utilizing Control Structures, Arrays and Collections.
    Application > activity > Program.cs
*/

