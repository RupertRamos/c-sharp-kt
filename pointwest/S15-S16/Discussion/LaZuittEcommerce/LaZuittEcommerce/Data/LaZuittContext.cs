﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using LaZuittEcommerce.Models;

namespace LaZuittEcommerce.Data;

public class LaZuittContext : DbContext
{
    public LaZuittContext(DbContextOptions<LaZuittContext> options) : base(options)
    {
    }

    public DbSet<Course> Course { get; set; }
    public DbSet<Student> Student { get; set; }
    public DbSet<Enroll> Enroll { get; set; }
    public DbSet<EnrollCourses> EnrollCourses { get; set; }
}