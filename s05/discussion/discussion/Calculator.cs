﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    internal interface ICalculator
    {
        void Compute(double numA, double numB, string operation);

        void TurnOff();


    }
}
