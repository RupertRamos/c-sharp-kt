﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace discussion
{
    internal class User
    {

        // Fields
        private string username;
        private readonly string email;
        private string password;
        private string address;
        private string contactNumber;

        // Getters and Setters
        public string Username { get => username; set => username = value; }
        public string Email { get => email; }
        public string Password { get => password; set => password = value; }
        public string Address { get => address; set => address = value; }
        public string ContactNumber { get => contactNumber; set => contactNumber = value; }

        // Empty Constructor
        public User() { }

        // Parameterized Constructor
        public User(string username, string email, string password, string address, string contactNumber)
        {
            this.username = username;
            this.email = email;
            this.password = password;
            this.address = address;
            this.contactNumber = contactNumber;
        }

        public override string ToString()
        {
            // The use of the "$" symbol allows inclusion of variables in a string for code readability
            // Using multiple curly brackets acts as an escaping brace allowing curly braces to be printed along with the string output
            return $"{{Username: {username}, Email: {email}, Password: {password}, Address: {address}, Contact Number: {contactNumber}}}";
        }

    }
}
