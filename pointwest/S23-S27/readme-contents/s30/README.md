# Session Objectives

At the end of the session, the students are expected to:

- set up multiple containers that will contain different components of an application.

# Resources

## Instructional Materials

- [GitLab Repository (App)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/NodeJs-App)
- [GitLab Repository (Dockerized App)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31/Dockerized-Apps/NodeJs-App)
- [GitLab Repository (Guide)](https://gitlab.com/zuitt-coding-bootcamp-curricula/corporate-courses/pointwest/pnt010/-/tree/master/s27-s31)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1gsJ-txK8sxOWjPZJXmafkAtmbQmUjDbhgvN89HLqFBw/edit)

## Supplemental Materials

- [Part 7: Multi-Container Apps (Docker Docs)](https://docs.docker.com/get-started/07_multi_container/)
- [Part 8: Use Docker Compose (Docker Docs)](https://docs.docker.com/get-started/08_using_compose/)

# Lesson Proper

The contents of this guide is based from the Getting Started contents of Docker Docs and is built upon it.

## Introduction

Up to this point, we have been working with single container apps. But, we now want to add MySQL to the application stack. The following question often arises - "Where will MySQL run? Install it in the same container or run it separately?" In general, **each container should do one thing and do it well.** A few reasons:

- There is a good chance you would have to scale APIs and front-ends differently than databases.
- Separate containers let you version and update versions in isolation.
- While you may use a container for the database locally, you may want to use a managed service for the database in production. You do not want to ship your database engine with your app then.
- Running multiple processes will require a process manager (the container only starts one process), which adds complexity to container startup/shutdown.

And there are more reasons. So, we will update our application to work like this:

![Untitled](readme-images/Untitled.png)

## Container Networking

Remember that containers, by default, run in isolation and do not know anything about other processes or containers on the same machine. So, how do we allow one container to talk to another? 

The answer is **networking**. Now, you do not have to be a network engineer. Simply remember this rule: If two containers are on the same network, they can talk to each other. If they are not, they cannot.

## Start MySQL

There are two ways to put a container on a network: 1) Assign it at start or 2) connect an existing container. For now, we will create the network first and attach the MySQL container at startup.

Create the network:

```bash
docker network create todo-app
```

Start a MySQL container and attach it to the network. We are also going to define a few environment variables that the database will use to initialize the database (see the "Environment Variables" section in the [MySQL Docker Hub listing](https://hub.docker.com/_/mysql/)).

```bash
docker run -d \
     --network todo-app --network-alias mysql \
     -v todo-mysql-data:/var/lib/mysql \
     -e MYSQL_ROOT_PASSWORD=secret \
     -e MYSQL_DATABASE=todos \
     mysql:5.7
```

If you are using PowerShell then use this command.

```powershell
docker run -d `
     --network todo-app --network-alias mysql `
     -v todo-mysql-data:/var/lib/mysql `
     -e MYSQL_ROOT_PASSWORD=secret `
     -e MYSQL_DATABASE=todos `
     mysql:5.7
```

You will also see we specified the `--network-alias` flag. We will come back to that in just a moment.

Wait for the command to finish.

![Untitled](readme-images/Untitled%201.png)

You will notice we are using a volume named `todo-mysql-data` here and mounting it at `/var/lib/mysql`, which is where MySQL stores its data. However, we never ran a `docker volume create` command. Docker recognizes we want to use a named volume and creates one automatically for us.

To confirm we have the database up and running, connect to the database and verify it connects.

```bash
docker exec -it <mysql-container-id> mysql -u root -p
```

When the password prompt comes up, type in **secret**. In the MySQL shell, list the databases and verify you see the `todos` database.

```bash
SHOW DATABASES;
```

You should see output that looks like this:

```
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| todos              |
+--------------------+
5 rows in set (0.00 sec)
```

Hooray! We have our `todos` database and it is ready for us to use!

Commands executed above must show the following output.

![Untitled](readme-images/Untitled%202.png)

## Connect to MySQL

Now that we know MySQL is up and running, let's use it! But, the question is... how? If we run another container on the same network, how do we find the container (remember each container has its own IP address)?

To figure it out, we are going to make use of the [nicolaka/netshoot](https://github.com/nicolaka/netshoot) container, which ships with a *lot* of tools that are useful for troubleshooting or debugging networking issues.

Start a new container using the nicolaka/netshoot image. Make sure to connect it to the same network.

```bash
docker run -it --network todo-app nicolaka/netshoot
```

Wait for the command to finish.

![Untitled](readme-images/Untitled%203.png)

After the command is finish, it will automatically open a shell for which the command below will be executed.

In case the trainee accidentally exits the shell, prompt the trainee to execute the `docker run` command again.

We are going to use the `dig` command, which is a useful DNS tool. We are going to look up the IP address for the hostname `mysql`.

```bash
dig mysql
```

And you will get an output like this:

```
; <<>> DiG 9.14.1 <<>> mysql
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 32162
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;mysql.				IN	A

;; ANSWER SECTION:
mysql.			600	IN	A	172.23.0.2

;; Query time: 0 msec
;; SERVER: 127.0.0.11#53(127.0.0.11)
;; WHEN: Tue Oct 01 23:47:24 UTC 2019
;; MSG SIZE  rcvd: 44
```

In the "ANSWER SECTION", you will see an `A` record for `mysql` that resolves to `172.23.0.2` (your IP address will most likely have a different value). While `mysql` is not normally a valid hostname, Docker was able to resolve it to the IP address of the container that had that network alias (remember the `--network-alias` flag we used earlier?).

What this means is our app only simply needs to connect to a host named `mysql` and it will talk to the database! It does not get much simpler than that!

## Run Your App with MySQL

The todo app supports the setting of a few environment variables to specify MySQL connection settings. They are:

- `MYSQL_HOST` - the hostname for the running MySQL server.
- `MYSQL_USER` - the username to use for the connection.
- `MYSQL_PASSWORD` - the password to use for the connection.
- `MYSQL_DB` - the database to use once connected.

**Note**: for MySQL versions 8.0 and higher, make sure to include the following commands in `mysql`.

```sql
ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'secret';
FLUSH PRIVILEGES;
```

Before continuing, open a terminal to the todo app source code directory.

We will specify each of the environment variables above, as well as connect the container to our app network.

```bash
docker run -dp 3000:3000 \
   -w /app -v "$(pwd):/app" \
   --network todo-app \
   -e MYSQL_HOST=mysql \
   -e MYSQL_USER=root \
   -e MYSQL_PASSWORD=secret \
   -e MYSQL_DB=todos \
   node:12-alpine \
   sh -c "yarn install && yarn run dev"
```

If you are using PowerShell then use this command.

```powershell
docker run -dp 3000:3000 `
   -w /app -v "$(pwd):/app" `
   --network todo-app `
   -e MYSQL_HOST=mysql `
   -e MYSQL_USER=root `
   -e MYSQL_PASSWORD=secret `
   -e MYSQL_DB=todos `
   node:12-alpine `
   sh -c "yarn install && yarn run dev"
```

If we look at the logs for the container (`docker logs <container-id>`) or using Docker Desktop, we should see a message indicating it is using the mysql database.

![Untitled](readme-images/Untitled%204.png)

Open the app in your browser and add a few items to your todo list.

![Untitled](readme-images/Untitled%205.png)

Connect to the mysql database and prove that the items are being written to the database. Remember, the password is **secret**.

```bash
docker exec -it <mysql-container-id> mysql -p
```

And in the mysql shell, run the following:

```sql
USE todos;
SELECT * FROM todo_items;
```

![Untitled](readme-images/Untitled%206.png)

Obviously, your table will look different because it has your items. But, you should see them stored there!

If you take a quick look at the Docker Desktop, you will see that we have two app containers running. But, there is no real indication that they are grouped together in a single app. We will see how to make that better shortly!

![Untitled](readme-images/Untitled%207.png)

At this point, we have an application that now stores its data in an external database running in a separate container. We learned a little bit about container networking and saw how service discovery can be performed using DNS.

But, there is a good chance you are starting to feel a little overwhelmed with everything you need to do to start up this application. We have to create a network, start containers, specify all of the environment variables, expose ports, and more! That is a lot to remember and it is certainly making things harder to pass along to someone else.

In the next section, we will talk about Docker Compose. With Docker Compose, we can share our application stacks in a much easier way and let others spin them up with a single (and simple) command!

### **Setting Connection Settings via Env Vars**

While using env vars to set connection settings is generally ok for development, it is **HIGHLY DISCOURAGED** when running applications in production. Diogo Monica, the former lead of security at Docker, [wrote a fantastic blog post](https://diogomonica.com/2017/03/27/why-you-shouldnt-use-env-variables-for-secret-data/) explaining why.

A more secure mechanism is to use the secret support provided by your container orchestration framework. In most cases, these secrets are mounted as files in the running container. You will see many apps (including the MySQL image and the todo app) also support env vars with a `_FILE` suffix to point to a file containing the variable.

As an example, setting the `MYSQL_PASSWORD_FILE` var will cause the app to use the contents of the referenced file as the connection password. Docker does not do anything to support these env vars. Your app will need to know to look for the variable and get the file contents.

## What is Docker Compose?

[Docker Compose](https://docs.docker.com/compose/) is a tool that was developed to help define and share multi-container applications. With Compose, we can create a YAML file to define the services and with a single command, can spin everything up or tear it all down.

The *big* advantage of using Compose is you can define your application stack in a file, keep it at the root of your project repo (it is now version controlled), and easily enable someone else to contribute to your project. Someone would only need to clone your repo and start the compose app. In fact, you might see quite a few projects on GitHub/GitLab doing exactly this now.

So, how do we get started?

## Install Docker Compose

If you installed Docker Desktop/Toolbox for either Windows or Mac, you already have Docker Compose! Play-with-Docker instances already have Docker Compose installed as well. If you are on a Linux machine, you will need to [install Docker Compose](https://docs.docker.com/compose/install/).

After installation, you should be able to run the following and see version information.

```bash
docker-compose version
```

## Create the Compose File

At the root of the app project, create a file named `docker-compose.yml`.

In the compose file, we will start off by defining the schema version. In most cases, it is best to use the latest supported version. You can look at the [Compose file reference](https://docs.docker.com/compose/compose-file/) for the current schema versions and the compatibility matrix.

```yaml
version: "3.7"
```

Next, we will define the list of services (or containers) we want to run as part of our application.

```yaml
version: "3.7"

services: 
```

And now, we will start migrating a service at a time into the compose file.

## Define the App Service

To remember, this was the command we were using to define our app container.

```bash
docker run -dp 3000:3000 \
  -w /app -v "$(pwd):/app" \
  --network todo-app \
  -e MYSQL_HOST=mysql \
  -e MYSQL_USER=root \
  -e MYSQL_PASSWORD=secret \
  -e MYSQL_DB=todos \
  node:12-alpine \
  sh -c "yarn install && yarn run dev"
```

This is the PowerShell version of the command.

```powershell
docker run -dp 3000:3000 `
  -w /app -v "$(pwd):/app" `
  --network todo-app `
  -e MYSQL_HOST=mysql `
  -e MYSQL_USER=root `
  -e MYSQL_PASSWORD=secret `
  -e MYSQL_DB=todos `
  node:12-alpine `
  sh -c "yarn install && yarn run dev"
```

First, let us define the service entry and the image for the container. We can pick any name for the service. The name will automatically become a network alias, which will be useful when defining our MySQL service.

```yaml
version: "3.7"

services:
  app:
    image: node:12-alpine
```

Typically, you will see the `command` close to the `image` definition, although there is no requirement on ordering. So, let us go ahead and move that into our file.

```yaml
version: "3.7"

services:
  app:
    image: node:12-alpine
    command: sh -c "yarn install && yarn run dev"
```

Let us migrate the `p 3000:3000` part of the command by defining the `ports` for the service. We will use the [short syntax](https://docs.docker.com/compose/compose-file/compose-file-v3/#short-syntax-1) here, but there is also a more verbose [long syntax](https://docs.docker.com/compose/compose-file/compose-file-v3/#long-syntax-1) available as well.

```yaml
version: "3.7"

services:
  app:
    image: node:12-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 3000:3000
```

Next, we will migrate both the working directory (`w /app`) and the volume mapping (`v "$(pwd):/app"`) by using the `working_dir` and `volumes` definitions. Volumes also has a [short](https://docs.docker.com/compose/compose-file/compose-file-v3/#short-syntax-3) and [long](https://docs.docker.com/compose/compose-file/compose-file-v3/#long-syntax-3) syntax.

One advantage of Docker Compose volume definitions is we can use relative paths from the current directory.

```yaml
version: "3.7"

services:
  app:
    image: node:12-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 3000:3000
    working_dir: /app
    volumes:
      - ./:/app
```

Finally, we need to migrate the environment variable definitions using the `environment` key.

```yaml
version: "3.7"

services:
  app:
    image: node:12-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 3000:3000
    working_dir: /app
    volumes:
      - ./:/app
    environment:
      MYSQL_HOST: mysql
      MYSQL_USER: root
      MYSQL_PASSWORD: secret
      MYSQL_DB: todos
```

## Define the MySQL Service

Now, it is time to define the MySQL service. The command that we used for that container was the following:

```bash
docker run -d \
  --network todo-app --network-alias mysql \
  -v todo-mysql-data:/var/lib/mysql \
  -e MYSQL_ROOT_PASSWORD=secret \
  -e MYSQL_DATABASE=todos \
  mysql:5.7
```

This is the PowerShell version of the command.

```powershell
docker run -d `
  --network todo-app --network-alias mysql `
  -v todo-mysql-data:/var/lib/mysql `
  -e MYSQL_ROOT_PASSWORD=secret `
  -e MYSQL_DATABASE=todos `
  mysql:5.7
```

We will first define the new service and name it `mysql` so it automatically gets the network alias. We will go ahead and specify the image to use as well.

```yaml
version: "3.7"

services:
  app:
    ...
  mysql:
    image: mysql:5.7
```

Next, we will define the volume mapping. When we ran the container with `docker run`, the named volume was created automatically. However, that does not happen when running with Compose. We need to define the volume in the top-level `volumes:` section and then specify the mountpoint in the service config. By simply providing only the volume name, the default options are used. There are [many more options available](https://docs.docker.com/compose/compose-file/compose-file-v3/#volume-configuration-reference) though.

```yaml
version: "3.7"

services:
  app:
    ...
  mysql:
    image: mysql:5.7
    volumes:
      - todo-mysql-data:/var/lib/mysql

volumes:
  todo-mysql-data:
```

Finally, we only need to specify the environment variables.

```yaml
version: "3.7"

services:
  app:
    ...
  mysql:
    image: mysql:5.7
    volumes:
      - todo-mysql-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: todos

volumes:
  todo-mysql-data:
```

At this point, our complete `docker-compose.yml` should look like this:

```yaml
version: "3.7"

services:
  app:
    image: node:12-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 3000:3000
    working_dir: /app
    volumes:
      - ./:/app
    environment:
      MYSQL_HOST: mysql
      MYSQL_USER: root
      MYSQL_PASSWORD: secret
      MYSQL_DB: todos
  mysql:
    image: mysql:5.7
    volumes:
      - todo-mysql-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: todos

volumes:
  todo-mysql-data:
```

## Run the Application Stack

Now that we have our `docker-compose.yml` file, we can start it up!

Make sure no other copies of the app/db are running first (`docker ps` and `docker rm -f <ids>`).

Start up the application stack using the `docker-compose up` command. We will add the `d` flag to run everything in the background.

```bash
docker-compose up -d
```

When we run this, we should see output like this:

![Untitled](readme-images/Untitled%208.png)

You will notice that the volume was created as well as a network! By default, Docker Compose automatically creates a network specifically for the application stack (which is why we did not define one in the compose file).

Let's look at the logs using the `docker-compose logs -f` command or the Docker Desktop. You will see the logs from each of the services interleaved into a single stream. This is incredibly useful when you want to watch for timing-related issues. The `-f` flag "follows" the log, so will give you live output as it is generated.

If you have run the command already, you will see output that looks like this:

![Untitled](readme-images/Untitled%209.png)

The service name is displayed at the beginning of the line (often colored) to help distinguish messages. If you want to view the logs for a specific service, you can add the service name to the end of the logs command (for example, `docker-compose logs -f app`).

When the app is starting up, it actually sits and waits for MySQL to be up and ready before trying to connect to it. Docker does not have any built-in support to wait for another container to be fully up, running, and ready before starting another container. For Node-based projects, you can use the [wait-port](https://github.com/dwmkerr/wait-port) dependency. Similar projects exist for other languages/frameworks.

At this point, you should be able to open your app and see it running. And hey! We are down to a single command!

## See App Stack in Docker Desktop

If we look at the Docker Desktop, we will see that there is a group named **nodejs-app**. This is the "project name" from Docker Compose and used to group the containers together. By default, the project name is simply the name of the directory that the `docker-compose.yml` was located in.

![Untitled](readme-images/Untitled%2010.png)

If you twirl down the app, you will see the two containers we defined in the compose file. The names are also a little more descriptive, as they follow the pattern of `<project-name>_<service-name>_<replica-number>`. So, it is very easy to quickly see what container is our app and which container is the mysql database.

## Tear It All Down

When you are ready to tear it all down, simply run `docker-compose down` or hit the trash can on the Docker Dashboard for the entire app. The containers will stop and the network will be removed.

By default, named volumes in your compose file are NOT removed when running `docker-compose down`. If you want to remove the volumes, you will need to add the `--volumes` flag.

The Docker Desktop does *not* remove volumes when you delete the app stack.

Once torn down, you can switch to another project, run `docker-compose up` and be ready to contribute to that project! It really does not get much simpler than that!

# Activity

## Quiz

### Instructions

Answer the quiz form provided by your instructor.

For instructors, create a copy of this [quiz form](https://docs.google.com/forms/d/1mKDkd0rdvwXDNzNhuvBEGRjOB7PFcH0jW4QG4SHytPY/edit) to your own batch kit folder.

### Answers

You have to be a network engineer to know how to use Docker networking.

- False

Docker command to create a new network.

- docker network create

Docker command flag used in the "docker run" command to specify the name of the network where the container will be linked to.

- --network-alias

A container image that ships with a lot of tools that are useful for troubleshooting or debugging networking issues.

- nicolaka/netshoot

A tool that was developed to help define and share multi-container applications.

- Docker Compose

Name of the Compose file recognized by Docker.

- docker-compose.yml

It is recommended to put actual environment values inside the Compose file.

- False

Docker command used to execute a Compose file.

- docker-compose up