﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    // Classes
    // Classes act as blueprints for data to be stored in the database as well as methods that define what a class is capable of doing
    internal class Item
    {

        // Fields
        private string name;
        // using the readonly keyword ensures that a field cannot be assigned a value at the class scope or in a constructor
        private readonly string branch;
        private int beginningInventory;
        private int stockIn;
        private int stockOut;
        private int totalBalance;

        // Getters and Setters
        public string Name { get => name; set => name = value; }
        // Getters and setters can be used to set a field to be read only or read and write depending on the use case

        public string Branch { get => branch; }
        public int BeginningInventory { get => beginningInventory; set => beginningInventory = value; }
        public int StockIn { get => stockIn; set => stockIn = value; }
        public int StockOut { get => stockOut; set => stockOut = value; }
        public int TotalBalance { get => totalBalance; set => totalBalance = value; }
        

        // Constructors
        // Empty Constructor
        // It is best to include empty constructors in instances where instantiating an object without any fields is required
        public Item()
        {
        }

        // Parameterized Constructor
        // Parameterized constructors are used to instantiate and initialize class fields
        public Item(string name, string branch, int beginningInventory, int stockIn, int stockOut, int totalBalance)
        {
            this.name = name;
            this.branch = branch;
            this.beginningInventory = beginningInventory;
            this.stockIn = stockIn;
            this.stockOut = stockOut;
            this.totalBalance = totalBalance;
        }

        // Methods
        // Methods are functions that belong to a specific class to perform specific tasks
        // Overriding Methods
        // Overriding methods refers to changing the implementation of an object's method, by default C# classes inherit methods from the Object class containing a ToString method that simply returns the variable's class
        // We can override a method using the "override" keyword
        // Overriding the Object.ToString() Method is a useful way of printing the values of objects for debugging
        public override string ToString()
        {
            // The use of the "$" symbol allows inclusion of variables in a string for code readability
            // Using multiple curly brackets acts as an escaping brace allowing curly braces to be printed along with the string output
            return $"{{Name: {name}, Branch: {branch}, Beginning Inventory: {beginningInventory}, Stock In: {stockIn}, Stock Out: {stockOut}, Total Balance: {totalBalance}}}";
        }

    }
}
