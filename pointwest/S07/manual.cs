// =====================
// C# Exception Handling
// =====================

/*
Other References:
    C# Exception Handling
        https://www.tutorialspoint.com/csharp/csharp_exception_handling.htm

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "C# Console App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Create code to demonstrate the use of exceptions.
    Application > discussion > Program.cs
*/

    	// ...

        namespace Discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // [Section] Exceptions
                    // An exception is a problem that arises during the execution of a program.
                    // A C# exception is a response to an exceptional circumstance that arises while a program is running, such as an attempt to divide by zero.
                    // The "Console.Readline()" method returns a string. When converting the value to a different data type and the user inputs a value that cannot be converted, the application will result in an exception.
                    // When an exception occurs, the application will terminate abnormally making it unresponsive and users will not be able to interact with it.
                    Console.WriteLine("Please input a number");
                    int numA = Convert.ToInt32(Console.ReadLine());
                    
                }

            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for C# Exception Handling.
        */

/*
3. Add more code to demonstrate the use of try-catch-finally statements.
    Application > discussion > Program.cs
*/

        // ...

        namespace Discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    /*
                    Console.WriteLine("Please input a number");
                    int numA = Convert.ToInt32(Console.ReadLine());
                    */
                    
                    // [Section] Try-Catch-Finally Statements and Exception Handling
                    // To ensure that the application will not terminate when an exception is encountered, try-catch-finally statements may be used to handle these exceptions and tell the application what to do.

                    int numA;

                    // What will be the code that the application will try to execute
                    try
                    {
                        Console.WriteLine("Please input a number");
                        numA = Convert.ToInt32(Console.ReadLine());
                    }
                    // What will the application do once it catches an exception
                    // There are several exceptions that can be encountered.
                    // All exceptions inherit from the "Exception" base class allowing us to use the "Exception" class in a catch statement to capture all possible exceptions.
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        Console.WriteLine(e.Message);
                    }
                    // What will the application finally do after trying to execute the code regardless if an exception was caught or nor
                    finally
                    {
                        Console.WriteLine("Please input a valid number");
                        numA = Convert.ToInt32(Console.ReadLine());
                    }

                }

            }
        }

/*
4. Add more code to demonstrate the use of multiple catch statements.
    Application > discussion > Program.cs
*/

        // ...

        namespace Discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...

                    int numA;

                    try
                    {
                        // ...
                    }
                    // Multiple catch statements can be used to capture different exceptions and to perform different tasks.
                    catch (FormatException e)
                    {
                        Console.WriteLine("The input you provided is not a number");
                    }
                    catch (Exception e)
                    {
                        // ...
                    }
                    // ...

                }

            }
        }

/*
5. Create a "multipleExceptions" function to further demonstrate the use of multiple catch statements.
    Application > discussion > Program.cs
*/

        using System;

        namespace Discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...

                    /*
                    int numA;

                    try
                    {
                        Console.WriteLine("Please input a number");
                        numA = Convert.ToInt32(Console.ReadLine());
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("The input you provided is not a number");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        Console.WriteLine("Please input a valid number");
                        numA = Convert.ToInt32(Console.ReadLine());
                    }
                    */

                    try
                    {
                        multipleExceptions();
                    }
                    catch (DivideByZeroException e)
                    {
                        Console.WriteLine("Cannot divide a number by zero");
                        Console.WriteLine(e.Message);
                    }
                    
                    
                }

                public static void MultipleExceptions()
                {
                    Console.WriteLine("Which error would you like to receive? [1]DivideByZeroException, [2]NullReferenceException");
                    int option = Convert.ToInt32(Console.ReadLine());

                    switch (option)
                    {
                        case 1:
                            Console.WriteLine("Please input a number");
                            int number = Convert.ToInt32(Console.ReadLine());
                            int divideByZero = number / 0;
                            break;
                        default:
                            Console.WriteLine("Please input a valid option");
                            break;
                    }
                }

            }
        }

/*
6. Create a "MyException" class to demonstrate the use of User Defined exceptions.
    Application > discussion > MyException.cs
*/

        // ...

        namespace discussion
        {
            // In cases where the pre-built exceptions of C# are inappropriate or whenever a customized exception is need in an application, user defined exceptions can be useful.
            internal class MyException : Exception
            {
                // The ": base(message)" overwrites the parent classes "message" field
                public MyException(string message): base(message) {
                }
            }
        }


/*
7. Update the "multipleExceptions" function to demonstrate the use of the "throw" keyword and User Defined exceptions.
    Application > discussion > Program.cs
*/

        using discussion;
        using System;

        namespace Discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    try
                    {
                        // ...
                    }
                    catch (DivideByZeroException e)
                    {
                        // ...
                    }
                    catch (MyException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    
                }

                public static void MultipleExceptions()
                {
                    Console.WriteLine("Which error would you like to receive? [1]DivideByZeroException, [2]NullReferenceException, [3] User Defined Exception");
                    int option = Convert.ToInt32(Console.ReadLine());

                    switch (option)
                    {
                        // ...
                        case 1:
                            // ...
                        case 2:
                            // The "throw" keyword can be used to force an exception when needed.
                            throw (new MyException("This is a user defined exception."));
                            // The break statement will now be unreachable because of throwing an exception
                            break;
                        default:
                            // ...
                    }
                }

            }
        }

/*
========
Activity
========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

        // 1a. On the menu bar, choose File > New > Project.
        // 1b. Choose "C# Console App" from the templates.
        // 1c. Add the project name as "Discussion" and click on "Next".
        // 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Create a "MyException" class.
    Application > discussion > MyException.cs
*/

        // ...

        namespace activity
        {
            internal class MyException : Exception
            {
                public MyException(string message): base(message) {
                }
            }
        }

/*
3. Create a "multipleExceptions" function and a try-catch-statement to capture the different kinds of exceptions.
    Application > discussion > Program.cs
*/

        // ...

        namespace activity
        {
            class Activity
            {

                static void Main(string[] args)
                {

                    try
                    {
                        multipleExceptions();
                    }
                    catch (DivideByZeroException e)
                    {
                        Console.WriteLine("Cannot divide a number by zero");
                        Console.WriteLine(e.Message);
                    }
                    catch (MyException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (IndexOutOfRangeException e)
                    {
                        Console.WriteLine("The element you are trying to access does not exist.");
                        Console.WriteLine(e.Message);
                    }

                }

                public static void MultipleExceptions()
                {
                    Console.WriteLine("Which error would you like to receive? [1]DivideByZeroException, [2]UserDefinedException, [3]IndexOutOfRangeException");
                    int option = Convert.ToInt32(Console.ReadLine());

                    switch (option)
                    {
                        case 1:
                            Console.WriteLine("Please input a number");
                            int number = Convert.ToInt32(Console.ReadLine());
                            int divideByZero = number / 0;
                            break;
                        case 2:
                            throw (new MyException("This is a user defined exception"));
                        case 3:
                            int[] intArray = new int[3];
                            Console.WriteLine(intArray[3]);
                            break;
                        default:
                            Console.WriteLine("Please input a valid option");
                            break;
                    }
                }

            }
        }