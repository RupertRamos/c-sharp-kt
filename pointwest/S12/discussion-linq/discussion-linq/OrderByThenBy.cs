﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion_linq
{
    public class OrderByThenBy
    {
        public void OrderByOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            // Method Based Syntax
            var data = courses.OrderBy(c => c.Price);

            // Query Based Syntax
            // var data = from c in courses orderby c.Price, c.Author select c;

            foreach (var item in data)
            {
                Console.WriteLine($"{item.Name} - {item.Price} - {item.Author}");
            }

            Console.ReadLine();
        }

        public void ThenByOperator()
        {
            var courses = CourseDatabase.GetCoursesData();

            // Method Based Syntax
            var data = courses.OrderBy(c => c.Price).ThenByDescending(a => a.Author);

            // Query Based Syntax
            // var data = from c in courses orderby c.Price, c.Author select c;

            foreach (var item in data)
            {
                Console.WriteLine($"{item.Name} - {item.Price} - {item.Author}");
            }

        }

    }
}
