﻿using AuthenticationServer.API.Models;
using Microsoft.EntityFrameworkCore;


namespace AuthenticationServer.API.Services.UserRepositories
{
    public class DatabaseUserRepository : IUserRepository
    {
        // Inject DbContext in here
        private readonly AuthenticationDbContext _context;

        //Generate our constructor
        public DatabaseUserRepository(AuthenticationDbContext context)
        {
            _context = context;
        }

        public async Task<User> Create(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<User> GetByEmail(string email)
        {
            // We are going to find the first data of email here if none it will return null
            return await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task<User> GetById(Guid userId)
        {
            // In here we will find the primary key which is the userId that's why we sue FindAsync.
            return await _context.Users.FindAsync(userId);
        }

        public async Task<User> GetByUsername(string username)
        {
            // We are going to find the first data of username here if none it will return null
            return await _context.Users.FirstOrDefaultAsync(u => u.UserName == username);
        }

    }
}
