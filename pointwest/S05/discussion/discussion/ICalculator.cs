﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace discussion
{

    // Interfaces
    // An interface is defined as a syntactical contract that all the classes inheriting the interface should follow.
    // The interface defines the 'what' part of the syntactical contract and the deriving classes define the 'how' part of the syntactical contract.
    // They define properties, methods, and events, which are the members of the interface and contain only the declaration of the members.
    // It is the responsibility of the deriving class to define the members.
    // It often helps in providing a standard structure that the deriving classes would follow.
    // Using the letter "I" in front of an interface is one of the common practices for naming interfaces which helps developers identify interfaces from classes.
    internal interface ICalculator
    {
        void Compute(double numA, double numB, String operation);
        void TurnOff();
    }
}
