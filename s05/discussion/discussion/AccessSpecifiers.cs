﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace discussion
{
    internal class AccessSpecifiers
    {

        //Public Access Specifier
        public string publicAccess = "public";

        //Private Access Specifier
        private string privateAccess = "private";

        //Protected Access Specifier
        protected string protectedAccess = "protected";

        //Internal Access Specifier
        internal string internalAccess = "internal";

        // Protected Internal access specificer
        protected internal string protectedInternalAccess = "protected and internal";


    }
}
