﻿using AuthenticationServer.API.Models;

namespace AuthenticationServer.API.Services.UserRepositories
{
    public interface IUserRepository
    {
        // Both of these will return a user object from our User Model.

        Task<User> GetByEmail(string email);

        Task<User> GetByUsername(string username);

        //Added
        Task<User> Create(User user);

        Task<User> GetById(Guid userId);
    }
}
