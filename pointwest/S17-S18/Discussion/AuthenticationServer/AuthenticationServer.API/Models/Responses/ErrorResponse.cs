﻿using System;
namespace AuthenticationServer.API.Models.Responses
{
    public class ErrorResponse
    {
        public IEnumerable<string> ErrorMessages { get; set; }

        // Create two constructor
        // This will hold a single error message.
        public ErrorResponse(string errorMessage) : this(new List<string>() { errorMessage }) { }

        // This will take a list of errormessages.
        public ErrorResponse(IEnumerable<string> errorMessages)
        {
            ErrorMessages = errorMessages;
        }
    }
}

