// =================================================
// C# Pillars of OOP (Encapsulation and Abstraction)
// =================================================

/*
Other References:
    C# Classes
        https://www.tutorialspoint.com/csharp/csharp_classes.htm
    C# Interfaces
        https://www.tutorialspoint.com/csharp/csharp_interfaces.htm
    C# Encapsulation
        https://www.tutorialspoint.com/csharp/csharp_encapsulation.htm

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

    	// 1a. On the menu bar, choose File > New > Project.
    	// 1b. Choose "C# Console App" from the templates.
    	// 1c. Add the project name as "Discussion" and click on "Next".
    	// 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Add code to demonstrate the use of encapsulation.
    Application > discussion > Program.cs
*/

    	// ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // [Section] Encapsulation
                    // Encapsulation is the process of enclosing one or more items within a physical or logical package.
                    // It prevents access to implementation details
                    // Using the method below to define information to be used in the app is tasking and creates confusion

                    string petAName = "Franky";
                    string petAGender = "Female";
                    string petAClassification = "Dog";
                    int petAAge = 10;
                    string petAAddress = "Manila, Philippines";
                    string petASound = "Bark!";

                    describePet(petAName, petAGender, petAClassification, petAAge, petAAddress);
                    makeSound(petAName, petASound);

                    string petBName = "Simba";
                    string petBGender = "Male";
                    string petBClassification = "Lion";
                    int petBAge = 1;
                    string petBAddress = "Pride Lands";
                    string petBSound = "Rawr!";

                    describePet(petBName, petBGender, petBClassification, petBAge, petBAddress);
                    makeSound(petBName, petBSound);

                }

                public static void describePet(string name, string gender, string classification, int age, string address)
                {
                    Console.WriteLine($"{name} is a {gender} {classification} who is {age} years of age and lives in {address}.");
                }

                public static void makeSound(string name, string sound)
                {
                    Console.WriteLine(name + " says " + sound);
                }
            }
        }

/*
3. Create a Pet Class to demonstrate the use of encapsulation.
    Application > discussion > Pet.cs
*/

    	// ...

        namespace discussion
        {
            internal class Pet
            {
                private string name;
                private string gender;
                private string classification;
                private int age;
                private string address;
                private string sound;

                public Pet() { }
                public Pet(string name, string gender, string classification, int age, string address, string sound)
                {
                    this.name = name;
                    this.gender = gender;
                    this.classification = classification;
                    this.age = age;
                    this.address = address;
                    this.sound = sound;
                }

                public string Name { get => name; set => name = value; }
                public string Gender { get => gender; set => gender = value; }
                public string Classification { get => classification; set => classification = value; }
                public int Age { get => age; set => age = value; }
                public string Address { get => address; set => address = value; }
                public string Sound { get => sound; set => sound = value; }

                public void DescribePet(string name, string gender, string classification, int age, string address)
                {
                    Console.WriteLine($"{name} is a {gender} {classification} who is {age} years of age and lives in {address}.");
                }

                public void MakeSound(string name, string sound)
                {
                    Console.WriteLine(name + " says " + sound);
                }
            }
        }

/*
4. Add more code to demonstrate the use of encapsulation.
    Application > discussion > Program.cs
*/

    	// ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // [Section] Encapsulation
                    // Encapsulation is the process of enclosing one or more items within a physical or logical package.
                    // It prevents access to implementation details
                    // Using the method below to define information to be used in the app is tasking and creates confusion

                    /*
                    string petAName = "Franky";
                    string petAGender = "Female";
                    string petAClassification = "Dog";
                    int petAAge = 10;
                    string petAAddress = "Manila, Philippines";
                    string petASound = "Bark!";

                    DescribePet(petAName, petAGender, petAClassification, petAAge, petAAddress);
                    MakeSound(petAName, petASound);

                    string petBName = "Simba";
                    string petBGender = "Male";
                    string petBClassification = "Lion";
                    int petBAge = 1;
                    string petBAddress = "Pride Lands";
                    string petBSound = "Rawr!";

                    DescribePet(petBName, petBGender, petBClassification, petBAge, petBAddress);
                    MakeSound(petBName, petBSound);
                    */

                    Pet petA = new Pet("Franky", "Female", "Dog", 10, "Manila, Philippines", "Bark!");

                    petA.DescribePet();
                    petA.MakeSound();

                    Pet petB = new Pet("Simba", "Male", "Lion", 1, "Pride Lands", "Rawr!");

                    petB.DescribePet();
                    petB.MakeSound();

                }

                /*
                public static void DescribePet(string name, string gender, string classification, int age, string address)
                {
                    Console.WriteLine($"{name} is a {gender} {classification} who is {age} years of age and lives in {address}.");
                }

                public static void MakeSound(string name, string sound)
                {
                    Console.WriteLine(name + " says " + sound);
                }
                */
            }
        }


/*
5. Add more code to demonstrate the use of destructors/finalizers.
    Application > discussion > Pet.cs
*/

    	// ...

        namespace discussion
        {
            internal class Pet
            {
                // ...
                public Pet(string name, string gender, string classification, int age, string address, string sound)
                {
                    // ...
                }

                // Destructor/Finalizers
                // A destructor is a special member function of a class that is executed whenever an object of its class goes out of scope.
                // It has exactly the same name as that of the class with a prefixed tilde (~) and it can neither return a value nor can it take any parameters.
                // Destructors can be very useful for releasing memory resources before exiting the program.
                // They cannot be inherited or overloaded.
                // It is called when the program exits.
                ~Pet() {
                    Console.WriteLine($"{name} is being deleted");
                }

                public string Name { get => name; set => name = value; }
                // ...
            }
        }


/*
6. Create a Calculator Class code to demonstrate the use of abstraction.
    Application > discussion > Calculator.cs
*/

        // ...

        namespace discussion
        {
            internal class Calculator
            {

                private string brand;
                private string price;

                public Calculator() { }

                public Calculator(string brand, string price)
                {
                    this.brand = brand;
                    this.price = price;
                }

                public string Brand { get => brand; set => brand = value; }
                public string Price { get => price; set => price = value; }

                public void Compute(double numA, double numB, String operation)
                {

                    // The equals method can be used to compare strings.
                    // The addition of the 2nd argument "StringComparison.OrdinalIgnoreCase" allows to compare strings ignoring the uppercase and lowercase differences
                    if (operation.Equals("add", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.WriteLine(numA + numB);
                    }
                    else if (operation.Equals("subtract", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.WriteLine(numA - numB);
                    }
                    else if (operation.Equals("multiply", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.WriteLine(numA * numB);
                    }
                    else if (operation.Equals("divide", StringComparison.OrdinalIgnoreCase) && numA > 0 && numB > 0)
                    {
                        Console.WriteLine(numA / numB);
                    }
                    else
                    {
                        Console.WriteLine("Invalid number or operation provided");
                    }

                }

                public void TurnOff()
                {
                    Console.WriteLine($"Closing {brand} calculator.");
                }
            }
        }

/*
7. Instantiate a "Calculator" class and invoke the "compute" method.
    Application > discussion > Program.cs
*/

        // ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    petB.MakeSound();

                    // [Section] Abstraction
                    // Abstraction is the process where all logic and code complexity is hidden from the users/developers which helps reduce confusion and allows them to focus on the "what" of things rather than the "how"
                    // Objects cannot be instantiated from interfaces

                    Calculator myCalculator = new Calculator("Casio", "500");
                    myCalculator.Compute(23, 45, "divide");
                    myCalculator.TurnOff();
                }
                // ...
            }
        }

/*
8. Rename and refactor the "Calculator" class to "Casio".
    Application > discussion > Calculator.cs
*/

        // Right click on the "Calculator" class and select the option "Rename".
        // Doing so will open a prompt asking for a confirmation to refactor all references to the "Calculator" class to the new class name for ease of use.
        // The references of the Calculator class in the Program.cs file will also change into the Casio Class

/*
9. Create a "Calculator" interface.
    Application > discussion > ICalculator.cs
*/

        // ...

        namespace discussion
        {

            // Interfaces
            // An interface is defined as a syntactical contract that all the classes inheriting the interface should follow.
            // The interface defines the 'what' part of the syntactical contract and the deriving classes define the 'how' part of the syntactical contract.
            // They define properties, methods, and events, which are the members of the interface and contain only the declaration of the members.
            // It is the responsibility of the deriving class to define the members.
            // It often helps in providing a standard structure that the deriving classes would follow.
            // Using the letter "I" in front of an interface is one of the common practices for naming interfaces which helps developers identify interfaces from classes.
            internal interface ICalculator
            {
                void Compute(double numA, double numB, String operation);
                void TurnOff();
            }
        }

        /*
        Important Note:
            - To create an interface in visual studio, do the following:
                - Right click on "discussion" in the "solution explorer".
                - Select "add" and then select "New Item".
                - Select "Interface" from the templates.
                - Rename the class by right clicking on the "Interface1.cs" file in the "solution explorer" and choosing the "Rename" option.
            - Refer to "references" section of this file to find the documentation for C# Interfaces.
        */

/*
10. Implement the "ICalculator" Interface in the "Casio" class.
    Application > discussion > Casio.cs
*/

        // ...

        namespace discussion
        {
            internal class Casio : ICalculator
            {
                // ...
            }
        }

/*
11. Create a Sharp Class code to demonstrate the use of abstraction.
    Application > discussion > Sharp.cs
*/

        // ...

        namespace discussion
        {
            internal class Sharp : ICalculator
            {
                private string brand;
                private string price;

                public Sharp() { }

                public Sharp(string brand, string price)
                {
                    this.brand = brand;
                    this.price = price;
                }

                public string Brand { get => brand; set => brand = value; }
                public string Price { get => price; set => price = value; }

                public void Compute(double numA, double numB, String operation)
                {

                    // The equals method can be used to compare strings.
                    // The addition of the 2nd argument "StringComparison.OrdinalIgnoreCase" allows to compare strings ignoring the uppercase and lowercase differences
                    if (operation.Equals("add", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.WriteLine((int)(numA + numB));
                    }
                    else if (operation.Equals("subtract", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.WriteLine((int)(numA - numB));
                    }
                    else if (operation.Equals("multiply", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.WriteLine((int)(numA * numB));
                    }
                    else if (operation.Equals("divide", StringComparison.OrdinalIgnoreCase) && numA > 0 && numB > 0)
                    {
                        Console.WriteLine((int)(numA / numB));
                    }
                    else
                    {
                        Console.WriteLine("Invalid number or operation provided");
                    }

                }

                public void TurnOff()
                {
                    Console.WriteLine($"Closing {brand} calculator.");
                }
            }
        }


/*
12. Instantiate a "Sharp" class and invoke the "compute" method.
    Application > discussion > Program.cs
*/

        // ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    petB.MakeSound();

                    // [Section] Abstraction
                    // Abstraction is the process where all logic and code complexity is hidden from the users/developers which helps reduce confusion and allows them to focus on the "what" of things rather than the "how"
                    // Objects cannot be instantiated from interfaces

                    Casio myCalculator = new Casio("Casio", "500");
                    myCalculator.Compute(23, 45, "divide");
                    myCalculator.TurnOff();

                    Sharp myOtherCalculator = new Sharp("Sharp", "1000");
                    myOtherCalculator.Compute(23, 45, "divide");
                    myOtherCalculator.TurnOff();

                }
                // ...
            }
        }

/*
13. Create a "useCalculator" function to demonstrate the use of interfaces.
    Application > discussion > Program.cs
*/

        // ...

        namespace discussion
        {
            class Discussion
            {

                static void Main(string[] args)
                {

                    // ...
                    myOtherCalculator.TurnOff();

                    useCalculator(myCalculator);
                    useCalculator(myOtherCalculator);
                }

                /*
                // ...

                public static void MakeSound(string name, string sound)
                {
                    // ...
                }
                */

                public static void useCalculator(ICalculator calculator)
                {
                    calculator.Compute(23, 45, "divide");
                }
            }
        }

/*
14. Create an "AccessSpecifiers" Class to demonstrate the use of access specifiers.
    Application > discussion > AccessSpecifier.cs
*/

        // ...

        namespace discussion
        {
            internal class AccessSpecifiers
            {
                //[Section] Access specifiers
                // Encapsulation is also implemented through the use of access specifiers.
                // An access specifier defines the scope and visibility of a class member.

                // Public access specifier
                // Allows a class to expose its member variables and member functions to other functions and objects.
                // Any public member can be accessed from outside the class.
                public string publicAccess = "public";

                // Private access specifier
                // Allows a class to hide its member variables and member functions from other functions and objects.
                // Only functions of the same class can access its private members.
                // Even an instance of a class cannot access its private members.
                private string privateAccess = "private";

                // Protected access specifier
                // Allows a child class to access the member variables and member functions of its base/parent class.
                // This way it helps in implementing inheritance.
                // Inheritance is one of the four pillars of OOP
                protected string protectedAccess = "protected";

                // Internal access specifier
                // Allows a class to expose its member variables and member functions to other functions and objects in the current assembly.
                // Any member with internal access specifier can be accessed from any class or method defined within the application in which the member is defined.
                internal string internalAccess = "internal";

                // Protected Internal access specificer
                // Allows a class to hide its member variables and member functions from other class objects and functions, except a child class within the same application.
                protected internal string protectedInternalAccess = "protected and internal";
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for C# Encapsulation.
        */

/*
========
Activity
========
*/

/*
1. Create a C# console app project.
    Visual Studio
*/

        // 1a. On the menu bar, choose File > New > Project.
        // 1b. Choose "C# Console App" from the templates.
        // 1c. Add the project name as "Discussion" and click on "Next".
        // 1d. Choose ".NET 6.0" as the framework and click on "Create".

/*
2. Create an "ICellphone" interface.
    Application > discussion > ICellphone.cs
*/

        // ...

        namespace activity
        {
            internal interface ICellphone
            {
                void SendText(string message, string recipient);
                void PlayMusic(string songName);
            }
        }

/*
3. Create a "Nokia" Class that implements the "ICellphone" interface.
    Application > discussion > Nokia.cs
*/

        // ...

        namespace activity
        {
            internal class Nokia : ICellphone
            {

                private string model;
                private int price;
                private string supplier;

                public Nokia() { }
                public Nokia(string model, int price, string supplier)
                {
                    this.model = model;
                    this.price = price;
                    this.supplier = supplier;
                }

                public string Model { get => model; set => model = value; }
                public int Price { get => price; set => price = value; }
                public string Supplier { get => supplier; set => supplier = value; }

                public void SendText(string message, string recipient)
                {
                    Console.WriteLine($"Text message sent to: {recipient}");
                    Console.WriteLine(message);
                }

                public void PlayMusic(string songName)
                {
                    Console.WriteLine($"The song {songName} is playing.");
                }

            }
        }

/*
4. Create a "Samsung" Class that implements the "ICellphone" interface.
    Application > discussion > Samsung.cs
*/

        // ...

        namespace activity
        {
            internal class Samsung : ICellphone
            {

                private string model;
                private int price;
                private string supplier;

                public Samsung() { }

                public Samsung(string model, int price, string supplier)
                {
                    this.model = model;
                    this.price = price;
                    this.supplier = supplier;
                }

                public string Model { get => model; set => model = value; }
                public int Price { get => price; set => price = value; }
                public string Supplier { get => supplier; set => supplier = value; }

                public void SendText(string message, string recipient)
                {
                    Console.WriteLine($"Text message sent to: {recipient} <3 :D");
                    Console.WriteLine(message);
                }

                public void PlayMusic(string songName)
                {
                    Console.WriteLine($"The song {songName} is playing on spotify.");
                }

            }
        }

/*
5. Instantiate and invoke the methods of the "Nokia" and "Samsung" classes.
    Application > discussion > Program.cs
*/

        // ...

        namespace activity
        {
            class Activity
            {

                static void Main(string[] args)
                {

                    Nokia myPhone = new Nokia("3310", 100, "Zuitt Electronics");
                    Console.WriteLine("Result from Nokia Phone:");
                    myPhone.SendText("Hello from the 90's", "John");
                    myPhone.PlayMusic("Backstreet Boys - Shape of My Heart");

                    Samsung myOtherPhone = new Samsung("Galaxy S5", 10000, "Market Suppliers");
                    Console.WriteLine("Result from Samsung Phone:");
                    myOtherPhone.SendText("Hw r u doing today?", "Jane");
                    myOtherPhone.PlayMusic("Taylor Swift - Love Story");

                }

            }
        }

/*
6. Create an "ICamera" interface.
    Application > discussion > ICamera.cs
*/

        // ...

        namespace activity
        {
            internal interface ICamera
            {
                void TakePicture();
                void TakeVideo();
            }
        }

/*
7. Implement the "ICamera" interface in the "Samsung" class.
    Application > discussion > Samsung.cs
*/

        // ...

        namespace activity
        {
            internal class Samsung : ICellphone, ICamera
            {

                // ...

                public void PlayMusic(string songName)
                {
                    // ...
                }

                public void TakePicture()
                {
                    Console.WriteLine("Smile for the camera");
                }

                public void TakeVideo()
                {
                    Console.WriteLine("Capturing video of your best moments.");
                }
            }
        }

/*
8. Invoke the additional methods of the "Samsung" classes.
    Application > discussion > Program.cs
*/

        // ...

        namespace activity
        {
            class Activity
            {

                static void Main(string[] args)
                {

                    // ...
                    myOtherPhone.PlayMusic("Taylor Swift - Love Story");
                    myOtherPhone.TakePicture();
                    myOtherPhone.TakeVideo();

                }

            }
        }
